# Flowcell barcodes

The Brother PT-P750W label printer is used to print the barcodes.
You need to install the `Brother P-touch Editor 6` software to use the printer.



## Instructions

Once you have installed the printer software, you can open the [barcode_design.lbx](./barcode_design.lbx) file. The barcode values are read from the [barcodes.csv](./barcodes.csv) file.

When printing the barcodes, make sure to use the settings as shown in the picture below. In particular, make sure to only check `Half Cut` and `Chain Print` in the Option tab of the Print window.

<img src="./instructions_print.png" width="800"/>

**NOTE**: the barcode standard followed is UPC-E. For this reason, the 6-digit barcode value has an additional zero in the front and a check digit in the back. Hence, only the 6 inner digits of the printed 8 digits correspond to the barcode value. E.g. the barcode value `441002` is printed as `04410029`.
