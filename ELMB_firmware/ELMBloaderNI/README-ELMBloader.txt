ELMBloader, application for programming ELMBs via CAN-bus
----------------------------------------------------------
Henk B, NIKHEF,  7 Nov 2008
Henk B, NIKHEF, 26 Jun 2008
Henk B, NIKHEF,  1 Dec 2007
Henk B, NIKHEF,  2 Aug 2006
Henk B, NIKHEF,  5 Jul 2006
Henk B, NIKHEF, 27 Jun 2005

File qt-mt230.dll contains the Qt GUI framework library,
used to create the ELMBloader application.
File qt-mt230.dll needs to be present in the same directory as ELMBloader.exe
or may be stored in the Windows DLL directory (e.g. C:\windows\system32).

Release notes
--------------
Version 1.3.1:
 - Bug fix: messages to activate Bootloaders bus-wide are buffered by
   the driver for sending, causing subsequent messages to read out
   the processor signature to NodeIDs up to about 10 to time out immediately.
   Fixed by increasing a pause after sending the activation messages
   (from 100 to 400 ms).

Version 1.3.0:
 - Menu item 'Tools' added with menu item 'Options'.
 - One option is available: a multiplication factor for a CAN-bus scan
   time-out can be set, in case there are CAN-nodes on the bus
   that are relatively slow to reply. The default time-out is 10 ms
   (was 20 ms in previous versions), so e.g. with a multiplication factor
   of 5 this becomes 50 ms.
 - Support for ATMEL AT90CANxxx microcontrollers containing 'ELMB-like'
   firmware has been added; different signatures have to be taken into account,
   as well as different Flash and EEPROM memory sizes.

Version 1.2.0:
 - Renamed programming option 'Multiple' to 'All', and added 'Multiple',
   with which any selection of the nodes on the bus can be programmed.
   Before, one could only program one or all nodes on the bus.

Version 1.1.3:
 - Bug fix: multiple NMT Reset messages were sent too quickly one after
   another (every NMT message is received by all nodes and handled
   in software) in case of bus-wide operations, causing some nodes to miss
   the ones addressed to them.
 - Progress bar text now shows 'Stopped' when operation is aborted.
 - Show enabled/disabled 'Advanced' menu items depending on CAN port opened
   or not.

Version 1.1.2:
 - CAN interface port number was limited to a single character, so up to 9;
   now fixed.

Version 1.1.1:
 - Addition of menu item in the 'Advanced' menu (item 'Erase NodeID'),
   for erasing the NodeID value in EEPROM addr location 107h,
   i.e. the ELMB will revert to its DIP-switch setting for its NodeID.
   This only applies to ELMB application firmware that supports
   a remotely configurable NodeID.
 - When the user aborts an operation involving the ELMB Bootloader firmware
   ELMBloader tries to restart the application firmware
   (by sending an NMT Reset message to nodes it knows about).

Version 1.1.0:
 - Addition of ELMB NodeID configuration via CAN-bus (NodeID is stored
   in ELMB onboard EEPROM), in the 'Advanced' menu (item 'Change NodeID').
   This only works for ELMB application firmware that supports this
   feature (via Object index 3300h and 3301h).
 - 'Node Info' operation (bus scan) now able to detect the presence
   of double Node-IDs on the bus (a warning is issued).
 - 'Kvaser port' combobox shows only CAN ports detected at startup.
 - Button 'Node Info' also enabled when 'single node' is selected
   (also then it scans the bus for all nodes present).
 - Command line options added (enabling automatic ELMBloader startup
   by external programs, etc.):
   Usage: ELMBloader -i<interface_no> -n<node_id> <filename>
   with <interface_no>: KVASER CAN-port number (>=1)
        <node_id>     : ELMB node identifier (1<=identifier<=127)
        <filename>    : name of firmware HEX-file to program into ELMB flash

Version 1.0.1:
 - First release.

