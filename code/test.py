
import pyads
from elmb import *
import traceback

plc = pyads.Connection('127.0.0.1.1.1', 851)
plc.open()

logging.basicConfig(level='INFO', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')




def try_n_times(callable, n=2):
    for _ in range(n-1):
        try:
            callable()
            return
        except Exception:
            logging.error(f'Error catched in try_n_times: {traceback.format_exc()}')
            time.sleep(0.2)
    callable()



    
elmb = ElmbElmbio(plc, 3)
print('Initialized ELMB')

try_n_times(lambda: elmb.read_can(0x1009))
try_n_times(lambda: elmb.read_can(0x100A, 0))
try_n_times(lambda: elmb.read_can(0x100A, 1))


try_n_times(lambda: elmb.read_can(0x1017))
try_n_times(lambda: elmb.read_can(0x100D))
try_n_times(lambda: elmb.read_can(0x3200, 3))


try_n_times(lambda: elmb.read_can(0x2100, 1))
try_n_times(lambda: elmb.read_can(0x2100, 2))
try_n_times(lambda: elmb.read_can(0x2100, 3))
try_n_times(lambda: elmb.read_can(0x2100, 4))


# # Valve

# try_n_times(lambda: elmb.read_can(0x2140))
# try_n_times(lambda: elmb.read_can(0x2150))
# try_n_times(lambda: elmb.read_can(0x2300))
# try_n_times(lambda: elmb.read_can(0x6423))
# try_n_times(lambda: elmb.read_can(0x6424, 1))
# try_n_times(lambda: elmb.read_can(0x6425, 1))


# Pressure
try_n_times(lambda: print(elmb.read_can(0x1802, 2)))
try_n_times(lambda: print(elmb.read_can(0x1802, 5)))



# try_n_times(lambda: elmb.write_can(255, 0x1802, 2))
# try_n_times(lambda: elmb.write_can(30, 0x1802, 5))





# try_n_times(lambda: elmb.write_can(0, 0x1017))
# try_n_times(lambda: elmb.write_can(0,   0x100D))
# try_n_times(lambda: elmb.write_can(255, 0x3200, 3))

# ## ADC parameters
# try_n_times(lambda: elmb.write_can(0, 0x2100, 1))
# try_n_times(lambda: elmb.write_adc_rate('30.0 Hz'))
# try_n_times(lambda: elmb.write_adc_range('5 V')) # Needs 2 attempts (0x719)
# try_n_times(lambda: elmb.write_adc_mode('Unipolar'))

# # Valve
# try_n_times(lambda: elmb.write_can(True, 0x2140))
# try_n_times(lambda: elmb.write_can(1, 0x2150))
# try_n_times(lambda: elmb.write_can(True, 0x2300))
# try_n_times(lambda: elmb.write_can(True, 0x6423))
# try_n_times(lambda: elmb.write_can(1.5*1e6, 0x6424, 255))
# try_n_times(lambda: elmb.write_can(0.0*1e6, 0x6425, 255))

# # # Pressure
# try_n_times(lambda: elmb.write_can(255, 0x1802, 2))
# try_n_times(lambda: elmb.write_can(30, 0x1802, 5))

# try_n_times(lambda: elmb.write_can('save', 0x1010, 1))



print('Successfully finished!')
    
    

