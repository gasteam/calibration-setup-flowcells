import time
import os
import subprocess
import numpy as np
from PySide6.QtCore import QRunnable
import logging
from datetime import datetime
from utils import relative_path
from textwrap import dedent
import concurrent.futures
# For example of parallel execution of functions, see
# https://neurostars.org/t/execute-multiple-functions-in-parallel-in-python/27485


def parallel_function_execution(callable1, callable2):
    '''Parallel execution of 2 functions'''
    with concurrent.futures.ThreadPoolExecutor() as executor:
        # Submit functions for execution
        futures = [
            executor.submit(callable1),
            executor.submit(callable2)
        ]
        results = [future.result() for future in futures]
    return results


class Runnable(QRunnable):
    def __init__(self, ui):
        super().__init__()
        self.ui = ui
        self.running = True
    
    def stop(self):
        self.running = False
        logging.debug('Thread stopped')


class RunnableFlowcellCalibration(Runnable):
    def __init__(self, ui, barcodes, flows1, flows2, nb_repeats, mfc1, fluid_number1,
            use_2_mfcs, mfc2, fluid_number2, percentage_mfc1,
            use_backpressure_regulator, backpressure_setpoint, RELATIVE_TOLERANCE_MFC=0.1
        ):
        '''Initialize thread for flowcell calibration'''
        super().__init__(ui)
        self.nb_flowcells = len(barcodes)
        self.barcode_groups = self.group_barcodes(barcodes)
        self.flows1 = flows1
        self.flows2 = flows2
        self.all_flows = self.flows2[:0:-1] + self.flows1[::-1] # Should now be ordered from large to small
        self.is_dual_curve = (len(self.flows2) > 0)
        self.nb_repeats = nb_repeats
        # Calculate the number of times to wait for measurements in self.run_calibration
        nb_flows = len(self.all_flows)
        nb_channels_elmb1 = len(self.barcode_groups) # The first ELMB is the bottleneck
        self.nb_wait_times = nb_flows*self.nb_repeats*nb_channels_elmb1
        self.nb_wait_time = 0
        # Set MFC variables
        self.mfc1 = mfc1
        self.fluid_number1 = fluid_number1
        self.use_2_mfcs = use_2_mfcs
        self.mfc2 = mfc2
        self.fluid_number2 = fluid_number2
        self.percentage_mfc1 = percentage_mfc1
        self.file_location = relative_path('calibration_data', f'calibration_{datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.csv')
        self.RELATIVE_TOLERANCE_MFC = RELATIVE_TOLERANCE_MFC
        self.use_backpressure_regulator = use_backpressure_regulator
        self.backpressure_setpoint = backpressure_setpoint
        self.logger = logging.getLogger('RunnableFlowcellCalibration')
        self.logger.debug('Thread initialized')
    
    def group_barcodes(self, barcodes):
        groups = []
        for i in range( (len(barcodes)+2)//4 ):
            groups.append(barcodes[4*i:4*i+4])
        return groups
    
    def get_progress(self):
        return self.nb_wait_time/self.nb_wait_times
    
    def is_outside_tolerance(self, mfc, flow, setpoint, fluid_number):
        max_flow_MFC = mfc.get_fluid_str_capacity(fluid_number)[1]
        within_absolute_tolerance = abs(flow - setpoint) < max_flow_MFC/100 # Bronkhorst MFCs have accuracy of 1% FS
        within_relative_tolerance = abs(flow - setpoint)/setpoint < self.RELATIVE_TOLERANCE_MFC
        return not (within_absolute_tolerance and within_relative_tolerance)

    def trigger_early_stop(self, reason=None, add_note_in_file=True):
        if add_note_in_file:
            if os.path.isfile(self.file_location):
                with open(self.file_location, 'r') as f:
                    lines = f.readlines()
                lines = [f'\nTHE CALIBRATION WAS ABORTED, THIS FILE IS NOT COMPLETE\nReason: {reason}\n'] + lines
                with open(self.file_location, 'w') as f:
                    f.writelines(lines)
        self.ui.calibration_signal.emit(self.get_progress(), True, 0, [], [], [])
    
    def write_elmb_reading(self, flow_setpoint, flow, is_curve_low, barcodes, channel_reading):
        '''Write the results of elmb.read_channel to a file.'''
        with open(self.file_location, 'a') as f:
            f.write(
                f'{datetime.now().strftime("%Y-%m-%d_%H:%M:%S")},' +
                f'{barcodes[0]},' +
                f'{barcodes[1]},' +
                f'{"low" if is_curve_low else "high"},' +
                f'{flow_setpoint},' +
                f'{flow},' +
                f'{channel_reading["slope1"]},' +
                f'{channel_reading["slope2"]},' +
                f'{channel_reading["T_amb1"]},' +
                f'{channel_reading["T_amb2"]},' +
                f'{channel_reading["I_low_in"]},' +
                f'{channel_reading["I_low_out"]},' +
                f'{channel_reading["I_high_in"]},' +
                f'{channel_reading["I_high_out"]},' +
                f'{channel_reading["t_wait1"]},' +
                f'{channel_reading["t_wait2"]},' +
                f'{channel_reading["t_sample1"]},' +
                f'{channel_reading["t_sample2"]}\n'
            )
        if self.is_dual_curve and is_curve_low and flow_setpoint == self.flows2[0]:
            # Write flow shared for curves to file twice with different curve attribute
            self.write_elmb_reading(flow_setpoint, flow, False, barcodes, channel_reading)

    def run(self):
        try:
            self.run_calibration()
        except Exception as e:
            msg = 'Exception while executing run_calibration!\n' + str(e)
            self.trigger_early_stop(msg)
            raise Exception(msg)
    
    def wait_until_stable_flow(self, mfc, flow_setpoint, fluid_number):
        '''Wait until 2 subsequent flow readings are sufficiently accurate. Raise an exception if it takes too long.'''
        mfc_reading1 = float('inf')
        mfc_reading2 = mfc.read_measurement()
        counter = 0
        while self.is_outside_tolerance(mfc, mfc_reading1, flow_setpoint, fluid_number) or \
            self.is_outside_tolerance(mfc, mfc_reading2, flow_setpoint, fluid_number):
            self.logger.info(f'Waiting for MFC {mfc.num_mfc} to reach setpoint {flow_setpoint}, current flow = {mfc_reading2}')
            time.sleep(1)
            mfc_reading1 = mfc_reading2
            mfc_reading2 = mfc.read_measurement()
            counter += 1
            if counter > 60:
                msg = f'MFC {mfc.num_mfc} cannot reach setpoint {flow_setpoint} within tolerance! Last flow reading = {mfc_reading2}'
                self.trigger_early_stop(msg)
                raise Exception(msg)
            if not self.running:
                self.trigger_early_stop('calibration was stopped manually')
                return

    def backpressure_outside_tolerance(self, pressure_reading):
        '''Check if the given backpressure is close enough to the setpoint.'''
        is_outside_tolerance = abs(pressure_reading - self.backpressure_setpoint) > 2.5/200 # 0.5% of 2.5 barg
        return is_outside_tolerance

    def wait_until_stable_backpressure(self):
        pressure1 = float('inf')
        pressure2 = self.ui.pressure_regulator.read_measurement()
        counter = 0
        while self.backpressure_outside_tolerance(pressure1) or self.backpressure_outside_tolerance(pressure2):
            self.logger.info(f'Waiting for BPR to reach setpoint...')
            time.sleep(1)
            pressure1 = pressure2
            pressure2 = self.ui.pressure_regulator.read_measurement()
            counter += 1
            if counter > 60:
                msg = f'Backpressure regulator cannot reach setpoint {self.backpressure_setpoint} within tolerance!'
                self.trigger_early_stop(msg)
                raise Exception(msg)
            if not self.running:
                self.trigger_early_stop('calibration was stopped manually')
                return


    def set_valve_state_and_pause(self, state, num, pause_time=1):
        '''Set the valve with the given number to the given state and
        pause a given number of seconds (to save the power supply from consecutive surges of current draw).'''
        self.ui.set_valve_state(state, num)
        time.sleep(pause_time)

    def run_calibration(self):
        '''Write all raw measurement data to file.
        Also keep track of measured slopes and pass these to main thread.'''

        # Set the valves appropriately based on the MFCs and manifolds in use.
        # De-energize all valves
        for valve_nb in range(1, 13):
            self.ui.set_valve_state(False, valve_nb)
        # Open valves for MFCs
        self.set_valve_state_and_pause(True, self.mfc1.num_mfc)
        if self.use_2_mfcs:
            self.set_valve_state_and_pause(True, self.mfc2.num_mfc)
        # Open valves for manifolds
        nb_manifolds = self.nb_flowcells//16 + (self.nb_flowcells%16 != 0)
        if nb_manifolds == 1:
            self.set_valve_state_and_pause(True, 5)
        elif nb_manifolds == 2:
            self.set_valve_state_and_pause(True, 6)
            self.set_valve_state_and_pause(True, 7)
        elif nb_manifolds == 3:
            self.set_valve_state_and_pause(True, 6)
            self.set_valve_state_and_pause(True, 8)
            self.set_valve_state_and_pause(True, 9)
        elif nb_manifolds == 4:
            self.set_valve_state_and_pause(True, 6)
            self.set_valve_state_and_pause(True, 8)
            self.set_valve_state_and_pause(True, 10)
            self.set_valve_state_and_pause(True, 11)

        # NOTE Valve 12 (Normally Open) is in parallel with BPR, close this valve if BPR is used.
        if self.use_backpressure_regulator:
            self.ui.set_valve_state(True, 12)
            self.ui.pressure_regulator.write_setpoint(self.backpressure_setpoint)
        else:
            self.ui.set_valve_state(False, 12)
        
        self.nb_wait_time = 0
        self.ui.calibration_signal.emit(self.get_progress(), False, 0, [], [], [])
        with open(self.file_location, 'w') as f:
            divider = '-'*5
            MFC_string = f'MFC {self.mfc1.num_mfc} ({self.percentage_mfc1:.1f}%) + MFC {self.mfc2.num_mfc} ({100.0-self.percentage_mfc1:.1f}%)' if self.use_2_mfcs else f'MFC {self.mfc1.num_mfc}'
            fluid_number_string = f'{self.fluid_number1} + {self.fluid_number2}' if self.use_2_mfcs else self.fluid_number1
            text_mix1, text_flow1 = self.mfc1.fluid_strings[self.fluid_number1].split(' ||| ')
            text_mix2, text_flow2 = self.mfc2.fluid_strings[self.fluid_number2].split(' ||| ')
            fluid_string = f'{text_mix1} ({self.percentage_mfc1:.1f}%) + {text_mix2} ({100.0-self.percentage_mfc1:.1f}%)' if self.use_2_mfcs else text_mix1
            flow_range_string = f'{text_flow1} + {text_flow2}' if self.use_2_mfcs else text_flow1
            f.write(dedent(f'''\
                # {divider}
                # file_format_version=1.4
                # number_of_flowcells={self.nb_flowcells}
                # ELMB_Delta_T={self.ui.elmb1.read_can(0x4000, 4)}
                # backpressure_setpoint_barg={self.backpressure_setpoint if self.use_backpressure_regulator else 'N/A'}
                # MFC={MFC_string}
                # fluid_number={fluid_number_string}
                # fluid={fluid_string}
                # flow_range={flow_range_string}
                # nb_repeats={self.nb_repeats}
                # is_dual_curve={self.is_dual_curve}
                # flows_curve_low={self.flows1}
                # flows_curve_high={self.flows2}
                # {divider}
            '''))
            f.write(f'timestamp,barcode1,barcode2,curve,flow_setpoint,flow,slope1,slope2,t_ambient1,t_ambient2,i_low_in,i_low_out,i_high_in,i_high_out,t_wait1,t_wait2,t_sample1,t_sample2\n')
        
        slopes = np.zeros((self.nb_flowcells, self.nb_repeats*len(self.all_flows)))
        
        all_flows_with_repeated = []
        for nb_flow, flow in enumerate(self.all_flows):
            is_curve_low = (nb_flow > len(self.flows2) - 2)
            if self.use_2_mfcs:
                flow_mfc1 = flow*self.percentage_mfc1/100.0
                flow_mfc2 = flow*(100.0-self.percentage_mfc1)/100.0
                self.mfc1.write_fluid_number(self.fluid_number1)
                self.mfc1.write_setpoint(flow_mfc1)
                self.mfc2.write_fluid_number(self.fluid_number2)
                self.mfc2.write_setpoint(flow_mfc2)
            else:
                self.mfc1.write_fluid_number(self.fluid_number1)
                flow_mfc1 = flow
                self.mfc1.write_setpoint(flow_mfc1)

            if flow > 0:
                time.sleep(5)
                self.wait_until_stable_flow(self.mfc1, flow_mfc1, self.fluid_number1)
                if self.use_2_mfcs:
                    self.wait_until_stable_flow(self.mfc2, flow_mfc2, self.fluid_number2)
                if self.use_backpressure_regulator:
                    self.wait_until_stable_backpressure()

                
            for nb_repeat in range(self.nb_repeats):
                self.logger.info(f'Starting repeat number {nb_repeat+1}/{self.nb_repeats} for setpoint {flow}')
                all_flows_with_repeated.append(flow)
                for (num_group, barcode_group) in enumerate(self.barcode_groups):
                    self.logger.debug(f'Starting ELMB channel reading for barcodes {barcode_group}')
                    
                    if not self.running:
                        self.trigger_early_stop('calibration was stopped manually')
                        return
                    
                    mfc_flow_measurement = self.mfc1.read_measurement() + self.mfc2.read_measurement() if self.use_2_mfcs else self.mfc1.read_measurement()
                    try:
                        if len(barcode_group) == 4:
                            elmb_reading_arr = parallel_function_execution(
                                lambda: self.ui.elmb1.read_channel(num_group),
                                lambda: self.ui.elmb2.read_channel(num_group)
                            )
                            for i, reading in enumerate(elmb_reading_arr):
                                self.write_elmb_reading(flow, mfc_flow_measurement, is_curve_low, barcode_group[2*i:2*i+2], reading)
                            slopes[4*num_group:4*num_group+4, nb_flow*self.nb_repeats+nb_repeat] = [
                                elmb_reading_arr[0]['slope1'],
                                elmb_reading_arr[0]['slope2'],
                                elmb_reading_arr[1]['slope1'],
                                elmb_reading_arr[1]['slope2'],
                            ]
                        else:
                            elmb_reading = self.ui.elmb1.read_channel(num_group)
                            self.write_elmb_reading(flow, mfc_flow_measurement, is_curve_low, barcode_group, elmb_reading)
                            slopes[4*num_group:4*num_group+2, nb_flow*self.nb_repeats+nb_repeat] = [
                                elmb_reading['slope1'],
                                elmb_reading['slope2']
                            ]
                    except Exception as e:
                        msg = f'Exception while reading ELMB channel(s) for barcodes {barcode_group}:\n' + str(e)
                        self.trigger_early_stop(msg)
                        raise Exception(msg)

                    self.nb_wait_time += 1
                    self.ui.calibration_signal.emit(
                        self.get_progress(),
                        False,
                        mfc_flow_measurement,
                        barcode_group,
                        all_flows_with_repeated,
                        slopes[4*num_group:4*num_group+len(barcode_group), 0:nb_flow*self.nb_repeats+nb_repeat+1]
                    )
        # Push calibration file to GitLab
        file_name = os.path.basename(self.file_location)
        self.logger.info(f'Pushing file to GitLab: {file_name}')
        self.run_shell_command(['git', 'add', f'{self.file_location}'])
        self.run_shell_command(['git', 'commit', '-m', f'Add calibration data: {file_name}'])
        self.run_shell_command(['git', 'pull'])
        self.run_shell_command(['git', 'push'])

    def run_shell_command(self, command):
        res = subprocess.run(command, capture_output=True)
        if res.returncode != 0:
            self.trigger_early_stop(add_note_in_file=False)
            raise Exception(f'Command failed to execute with returncode {res.returncode}:\n{" ".join(command)}\nstdout:\n{str(res.stdout)}')


class RunnableADCCalibration(Runnable):
    def __init__(self, ui, elmb, TIME_WINDOW=10, MOVING_AVG_NB_POINTS=5):
        super().__init__(ui)
        self.elmb = elmb
        self.TIME_WINDOW = TIME_WINDOW
        self.MOVING_AVG_NB_POINTS = MOVING_AVG_NB_POINTS
        self.adc_range = elmb.read_can(0x2100, 3)
        self.data_potentio_inflow =  []
        self.data_potentio_outflow = []
        self.clear_data = False
        self.logger = logging.getLogger('RunnableADCCalibration')
        self.logger.debug('Thread initialized')
        
    
    def run(self):
        while self.running:
            if self.clear_data:
                self.clear_data = False
                if self.ui.radioButton_smooth_adc.isChecked():
                    self.data_potentio_inflow =  self.data_potentio_inflow[-self.MOVING_AVG_NB_POINTS:]
                    self.data_potentio_outflow = self.data_potentio_outflow[-self.MOVING_AVG_NB_POINTS:]    
                else:
                    self.data_potentio_inflow =  []
                    self.data_potentio_outflow = []
            self.data_potentio_inflow.append(self.elmb.read_analogue_input(0, self.adc_range))
            self.data_potentio_outflow.append(self.elmb.read_analogue_input(1, self.adc_range))
            sample_period = 1.0/self.ui.spinBox_adc_rate.value()
            self.data_potentio_inflow = self.data_potentio_inflow[-int(self.TIME_WINDOW/sample_period):]
            self.data_potentio_outflow = self.data_potentio_outflow[-int(self.TIME_WINDOW/sample_period):]
            if self.ui.radioButton_smooth_adc.isChecked():
                data_inflow = self.smooth_data(self.data_potentio_inflow)
                data_outflow = self.smooth_data(self.data_potentio_outflow)
            else:
                data_inflow = self.data_potentio_inflow
                data_outflow = self.data_potentio_outflow
            self.ui.adc_sample_signal.emit(data_inflow, data_outflow)
            time.sleep(sample_period)
    
    def smooth_data(self, data):
        if len(data) < self.MOVING_AVG_NB_POINTS:
            return [np.mean(data)]
        filter = [1/self.MOVING_AVG_NB_POINTS] * self.MOVING_AVG_NB_POINTS
        return np.convolve(filter, data, 'valid')



class RunnableManualControl(Runnable):
    def __init__(self, ui, sample_period=0.5):
        super().__init__(ui)
        self.sample_period = sample_period
        self.logger = logging.getLogger('RunnableManualControl')
        self.logger.debug('Thread initialized')
    
    def run(self):
        while self.running:
            pressure1 = self.ui.PT1.read() # Pressure from PT1
            pressure2 = self.ui.PT2.read() # Pressure from PT2
            pressure3 = self.ui.pressure_regulator.read_measurement() # Pressure from backpressure regulator
            mfc_readings = [mfc.read_measurement() for mfc in self.ui.mfcs.mfcs]
            self.ui.manual_control_signal.emit(pressure1, pressure2, pressure3, mfc_readings) # PT1, PT2, backpressure, [mfc1, mfc2, mfc3, mfc4]
            time.sleep(self.sample_period)


class RunnableManualChannelReading(Runnable):
    def __init__(self, ui, elmb_nb, channel_elmb):
        super().__init__(ui)
        self.elmb_nb = elmb_nb
        self.channel_elmb = channel_elmb
        self.logger = logging.getLogger('RunnableManualChannelReading')
        self.logger.debug('Thread initialized')
    
    def run(self):
        self.ui.pushButton_manual_run.blockSignals(True)
        try:
            elmb = self.ui.elmb1 if self.elmb_nb==1 else self.ui.elmb2
            res = elmb.read_channel(self.channel_elmb)
            self.ui.manual_channel_signal.emit(True, res['slope1'], res['slope2'], res['time_measured'])
            self.ui.pushButton_manual_run.blockSignals(False)
        except Exception as e:
            self.ui.manual_channel_signal.emit(False, 0, 0, 0)
            self.ui.pushButton_manual_run.blockSignals(False)
            raise Exception(f'elmb.read_channel failed! Exception: {e}')


def calculate_temperature(voltage, current):
    '''Calculate the temperature in °C for a Pt100 sensor.'''
    return (voltage/current - 100)/0.385

class RunnableCableCheck(Runnable):
    def __init__(self, ui, nb_channels, NB_SAMPLES=5):
        super().__init__(ui)
        self.nb_channels = nb_channels
        self.NB_SAMPLES = NB_SAMPLES
        self.measurements = np.zeros(64)
        self.adc_range1 = ui.elmb1.read_can(0x2100, 3)
        self.adc_range2 = ui.elmb2.read_can(0x2100, 3)
        self.logger = logging.getLogger('RunnableCableCheck')
        self.logger.debug('Thread initialized')
    
    def run(self):
        self.ui.cable_check_signal_initial.emit(-2, 0, 0, 0, 0)
        # Read low currents in series
        self.ui.elmb1.set_adc_mode(current='low', resistance=100)
        self.ui.elmb2.set_adc_mode(current='low', resistance=100)
        # Measure averages of voltages for in- and out-flow
        voltage_inflow1, voltage_outflow1 = self.return_averaged_samples(
            lambda: (self.ui.elmb1.read_analogue_input(0), self.ui.elmb1.read_analogue_input(1))
        )
        voltage_inflow2, voltage_outflow2 = self.return_averaged_samples(
            lambda: (self.ui.elmb2.read_analogue_input(0), self.ui.elmb2.read_analogue_input(1))
        )
        current_inflow1, current_outflow1, current_inflow2, current_outflow2 = \
            voltage_inflow1/100, voltage_outflow1/100, voltage_inflow2/100, voltage_outflow2/100
        self.currents = current_inflow1, current_outflow1, current_inflow2, current_outflow2
        self.ui.cable_check_signal_initial.emit(-1, current_inflow1, current_outflow1, current_inflow2, current_outflow2)
        if not self.running:
            return
        
        # Read temperatures in parallel
        for current_channel in range(self.nb_channels//2):
            self.ui.cable_check_signal.emit(current_channel, self.measurements)
            self.ui.elmb1.set_adc_mode(current='low', resistance=current_channel)
            self.ui.elmb2.set_adc_mode(current='low', resistance=current_channel)
            voltages = parallel_function_execution(
                lambda: self.get_averaged_flowchannel_voltages(self.ui.elmb1, self.adc_range1, current_channel),
                lambda: self.get_averaged_flowchannel_voltages(self.ui.elmb2, self.adc_range2, current_channel)
            )
            volt1_elmb1, volt2_elmb1 = voltages[0]
            volt1_elmb2, volt2_elmb2 = voltages[1]
            self.measurements[4*current_channel]   = calculate_temperature(volt1_elmb1, current_inflow1)
            self.measurements[4*current_channel+1] = calculate_temperature(volt2_elmb1, current_outflow1)
            self.measurements[4*current_channel+2] = calculate_temperature(volt1_elmb2, current_inflow2)
            self.measurements[4*current_channel+3] = calculate_temperature(volt2_elmb2, current_outflow2)
            
            if not self.running:
                return
        
        if (self.nb_channels % 2) == 1:
            # Odd number of channels => read 1 extra channel with first ELMB
            current_channel = self.nb_channels//2
            self.ui.cable_check_signal.emit(current_channel, self.measurements)
            self.ui.elmb1.set_adc_mode(current='low', resistance=current_channel)
            volt1, volt2 = self.get_averaged_flowchannel_voltages(self.ui.elmb1, self.adc_range1, current_channel)
            self.measurements[4*current_channel]   = calculate_temperature(volt1, current_inflow1)
            self.measurements[4*current_channel+1] = calculate_temperature(volt2, current_outflow1)
        
        self.ui.cable_check_signal.emit(current_channel+1, self.measurements)
        
        
    
    def get_averaged_flowchannel_voltages(self, elmb, adc_range, current_channel):
        volt1, volt2 = self.return_averaged_samples(
            lambda: elmb.read_flowchannel_voltages(current_channel, adc_range)
        )
        return volt1, volt2
    
    
    def return_averaged_samples(self, callable):
        '''Return the result of averaging self.NB_SAMPLES samples with callable returning a 2-tuple.'''
        tot1 = tot2 = 0
        for _ in range(self.NB_SAMPLES):
            val1, val2 = callable()
            tot1 += val1
            tot2 += val2
        return (tot1/self.NB_SAMPLES, tot2/self.NB_SAMPLES)

    def measure_1_channel(self, channel_global, elmb_nb, channel_elmb):
        '''Return the temperatures for the given channel_global in [0, 31], elmb_nb in [1, 2] and channel_elmb in [0, 15]'''
        if elmb_nb == 1:
            self.ui.elmb1.set_adc_mode(current='low', resistance=channel_elmb)
            volt1, volt2 = self.get_averaged_flowchannel_voltages(self.ui.elmb1, self.adc_range1, channel_elmb)
            self.ui.elmb1.set_adc_mode('off')
            self.measurements[2*channel_global]   = calculate_temperature(volt1, self.currents[0])
            self.measurements[2*channel_global+1] = calculate_temperature(volt2, self.currents[1])
        else:
            self.ui.elmb2.set_adc_mode(current='low', resistance=channel_elmb)
            volt1, volt2 = self.get_averaged_flowchannel_voltages(self.ui.elmb2, self.adc_range2, channel_elmb)
            self.ui.elmb2.set_adc_mode('off')
            self.measurements[2*channel_global]   = calculate_temperature(volt1, self.currents[2])
            self.measurements[2*channel_global+1] = calculate_temperature(volt2, self.currents[3])
        
