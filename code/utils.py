import os
import traceback
import logging
import time
import struct

def relative_path(*path_elements):
    '''Return the full path relative to the base folder with the given path elements appended to it.'''
    return os.path.join(os.path.dirname(__file__), *path_elements)

def wrap_string(s, num_chars, max_lines):
    '''Convert the string into multiline with maximally num_chars characters on a line'''
    res = ''
    for i in range(max_lines-1):
        res += '\n' + s[i*num_chars:(i+1)*num_chars]
    res += '\n' + s[(max_lines-1)*num_chars:]
    return res.strip()


def revert_dictionary(d):
    '''Return a new dictionary with keys and values switched'''
    res = dict()
    for k, v in d.items():
        res[v] = k
    return res


def try_n_times(callable, n=2, logger=None):
    for _ in range(n-1):
        try:
            callable()
            return
        except Exception:
            msg = f'Error catched in try_n_times: {traceback.format_exc()}'
            if logger:
                logger.error(msg)
            else:
                logging.error(msg)
            time.sleep(0.2)
    callable()


def float_to_byte_arr(f):
    return list(struct.pack('!f', f))

def byte_arr_to_float(arr):
    return struct.unpack('!f', bytes(arr))[0]
