# Run the application

On the Beckhoff PLC (hostname PCDTGAS6), there are shortcuts on the desktop, home menu and taskbar to run the application.
If you want to run manually, run this in the [code/](.) folder.
```python
pip install -r requirements.txt
python finalwindow.py
```

The GUI of the application was developed with Python 3.11 and PySide6.
The GUI is defined in the file [mainwindow.ui](./mainwindow.ui).
To edit the GUI, run the command `pyside6-designer` and edit the this file.
After editing the file, it needs to be compiled into a Python file [`mainwindow.py`](./mainwindow.py) using this command:
```
pyside6-uic mainwindow.ui -o mainwindow.py
```