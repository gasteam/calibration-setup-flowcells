import sys, logging, traceback
import numpy as np
import pyads
from elmb import ElmbFlowscan, ElmbElmbio
from valves import ValvesNC, ValveNO
from pressure_transmitter import pressure_transmitter
from pressure_regulator import PressureRegulator
from mfc import Mfcs
from utils import *
from mainwindow import Ui_MainWindow
from PySide6 import QtWidgets, QtGui
from PySide6.QtCore import Qt, QThreadPool, Signal, QSize
from runnables import *
import pyqtgraph as pg
import functools
import re
import ctypes
myappid = 'cern.calibration.flowcell.setup.2024' # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    adc_sample_signal = Signal(list, list)
    cable_check_signal_initial = Signal(int, float, float, float, float)
    cable_check_signal = Signal(int, np.ndarray)
    manual_control_signal = Signal(float, float, float, list)
    manual_channel_signal = Signal(bool, int, int, int)
    calibration_signal = Signal(float, bool, float, list, list, np.ndarray)
    
    def __init__(self, plc):
        super(MainWindow, self).__init__()
        self.logger = logging.getLogger('MainWindow')
        self.setupUi(self)
        self.pool = QThreadPool.globalInstance()
        self.plc = plc
        self.check_state()
        self.elmb1 = ElmbFlowscan(plc, id=1)
        self.elmb2 = ElmbFlowscan(plc, id=2)
        self.elmb3 = ElmbElmbio(plc, id=3)
        app.aboutToQuit.connect(self.on_close_window)
        self.pushButton_error_clear.clicked.connect(self.textBrowser_error.clear)
        self.setWindowIcon(QtGui.QIcon(QtGui.QPixmap('img/icon_calibration.ico')))
        self.runnable_cable = self.runnable_manual_control = self.runnable_manual_channel = self.runnable_adc = self.runnable_calib = Runnable(self)
        self.tabWidget.setCurrentIndex(0)
        self.current_tab_index = 0
        self.tabWidget.currentChanged.connect(lambda index_changed_to: self.onTabChanged(index_changed_to))
        
        
        # Configuration tab
        self.comboBox_config_elmb_id.currentIndexChanged.connect(self.on_elmb_id_changed)
        self.on_elmb_id_changed()
        self.comboBox_elmbio_type.currentIndexChanged.connect(self.on_elmbio_type_changed)
        self.pushButton_write.clicked.connect(self.config_tab_write_clicked)
        self.pushButton_load.clicked.connect(self.config_tab_load_clicked)
        self.pushButton_save.clicked.connect(self.config_tab_save_clicked)
        
        # ADC calibration tab
        self.radioButton_adc.toggled.connect(lambda checked: self.onADCRunPressed(checked))
        self.adc_sample_signal.connect(lambda l1, l2: self.on_adc_sample_signal(l1, l2))
        self.pushButton_adc_clear.clicked.connect(self.on_adc_clear_graph)
        self.comboBox_adc_elmb_id.currentTextChanged.connect(self.on_adc_settings_changed)
        self.comboBox_adc_current.currentTextChanged.connect(self.on_adc_settings_changed)
        self.widget_graph_adc.setBackground('w')
        styles = {'color':'k', 'font-size': '12pt'}
        self.widget_graph_adc.setLabel('left', 'Voltage (V)', **styles)
        self.widget_graph_adc.setLabel('bottom', 'Time (s)', **styles)
        self.widget_graph_adc.addLegend()
        self.line_adc_voltage_red = self.widget_graph_adc.plot(pen={'color':'r', 'width':2}, name='Voltage potentio in-flow')
        self.line_adc_voltage_blue = self.widget_graph_adc.plot(pen={'color':'b', 'width':2}, name='Voltage potentio out-flow')
        self.line_adc_limit_lower = self.widget_graph_adc.plot(pen={'color':'g', 'width':2, 'style': Qt.DashLine}, name='Lower tolerance')
        self.line_adc_limit_upper = self.widget_graph_adc.plot(pen={'color':'g', 'width':2, 'style': Qt.DashLine}, name='Upper tolerance')
        self.widget_graph_adc.showGrid(x=True, y=True)

        # Cable check tab
        self.radioButton_cable.toggled.connect(lambda checked: self.on_cable_check_run_pressed(checked))
        self.pushButton_cable_run.clicked.connect(self.on_cable_run_clicked)
        self.widget_graphs_cable = [self.widget_graph_cable_1, self.widget_graph_cable_2, self.widget_graph_cable_3, self.widget_graph_cable_4]
        styles = {'color':'k', 'font-size':'10pt'}
        for i in range(4):
            wg = self.widget_graphs_cable[i]
            wg.showGrid(x=True, y=True)
            wg.getAxis('bottom').setTicks([[(v, str(v)) for v in range(1,17)]])
            wg.setTitle(f'Manifold {i+1}')
            wg.setBackground('w')
            wg.setLabel('left', 'Temperature (°C)', **styles)
            wg.setLabel('bottom', 'Flowcell', **styles)
            self.draw_graph_tab4(i, x=range(1,17), height=[0]*16)
        self.cable_check_signal_initial.connect(lambda num, current1, current2, current3, current4: self.on_cable_check_signal_initial(num, current1, current2, current3, current4))
        self.cable_check_signal.connect(lambda ch, temps: self.on_cable_check_signal(ch, temps))
    
        # Manual rack control tab    
        self.valves_nc = ValvesNC(plc, 11)
        self.buttons_valves = [
            self.pushButton_valve1, self.pushButton_valve2, self.pushButton_valve3, self.pushButton_valve4, self.pushButton_valve5, self.pushButton_valve6, self.pushButton_valve7, self.pushButton_valve8, self.pushButton_valve9, self.pushButton_valve10, self.pushButton_valve11
        ]
        ICON_SIZE = QSize(59, 50)
        for i in range(11):
            button_valve = self.buttons_valves[i]
            self.set_valve_state(self.valves_nc.read_valve_state(i+1), i+1)
            button_valve.setIconSize(ICON_SIZE)
            button_valve.clicked.connect(functools.partial(self.on_valve_clicked, i+1))
        self.valve_no = ValveNO(plc)
        self.set_valve_state(self.valve_no.read_valve_state(), 12)
        self.pushButton_valve12.setIconSize(ICON_SIZE)
        self.pushButton_valve12.clicked.connect(functools.partial(self.on_valve_clicked, 12))
        self.pushButton_manual_reset.clicked.connect(self.on_manual_control_reset)
        self.pushButton_manual_run.clicked.connect(self.on_pushButton_manual_run_clicked)
        
        self.PT1 = pressure_transmitter(plc, 1)
        self.PT2 = pressure_transmitter(plc, 2)
        
        self.pressure_regulator = PressureRegulator(plc)
        self.mfcs = Mfcs(plc, 4)
        self.mfc_block_fluid_setpoint_reset = False
        self.on_manual_mfc_changed(0)
        self.comboBox_manual_mfc.currentIndexChanged.connect(lambda new_index: self.on_manual_mfc_changed(new_index))
        self.comboBox_manual_fluid.currentIndexChanged.connect(lambda new_index: self.on_manual_fluid_changed(new_index))
        self.doubleSpinBox_setpoint_mfc.valueChanged.connect(lambda val: self.on_mfc_setpoint_changed(val))
        self.label_backpressure.setPixmap(QtGui.QPixmap(relative_path('img', 'backpressure_regulator.png')).scaledToWidth(100))
        self.manual_control_signal.connect(lambda p1, p2, p3, l: self.on_manual_control_signal(p1, p2, p3, l))
        self.manual_channel_signal.connect(lambda b, s1, s2, t: self.on_manual_channel_signal(b, s1, s2, t))
        self.checkBox_manual_bpr.stateChanged.connect(lambda is_checked: self.on_checkBox_manual_bpr_changed(is_checked))
        self.on_checkBox_manual_bpr_changed(False)
        self.doubleSpinBox_setpoint_backpressure.valueChanged.connect(lambda pressure: self.on_bpr_setpoint_changed(pressure))
        
        # Flowcell calibration tab
        self.label_3way1.setPixmap(QtGui.QPixmap(relative_path('img', '3way_valve1.png')).scaledToWidth(61))
        self.label_3way2.setPixmap(QtGui.QPixmap(relative_path('img', '3way_valve2.png')).scaledToWidth(61))
        self.spinBox_calib_nb_flows_1.valueChanged.connect(lambda val: self.on_calib_nb_flows_changed(val, 1))
        self.spinBox_calib_nb_flows_2.valueChanged.connect(lambda val: self.on_calib_nb_flows_changed(val, 2))
        self.checkBox_calib_enable_flows2.stateChanged.connect(lambda is_checked: self.on_calib_checkbox_flows2_changed(is_checked))
        self.on_calib_nb_flows_changed(self.spinBox_calib_nb_flows_1.value(), 1)
        self.on_calib_checkbox_flows2_changed(False)
        self.spinBox_calib_nb_flows_1.setStyleSheet('background-color: rgb(255, 255, 255);')
        self.doubleSpinBox_calib_max1.setStyleSheet('background-color: rgb(255, 255, 255);')
        self.pushButton_calib_autofill.clicked.connect(self.autofill_calib_flows)
        self.spinBox_calib_nb_flowcells.valueChanged.connect(lambda val: self.on_calib_nb_flowcells_changed(val))
        self.on_calib_nb_flowcells_changed(self.spinBox_calib_nb_flowcells.value())
        self.radioButton_calib_run.toggled.connect(lambda checked: self.on_calib_run_toggled(checked))
        self.pushButton_calib_save.clicked.connect(self.on_calib_save_file)
        self.pushButton_calib_load.clicked.connect(self.on_calib_load_file)
        self.on_calib_mfc_changed(mfc_nb=1, new_mfc_index=0)
        self.on_calib_mfc_changed(mfc_nb=2, new_mfc_index=1)
        self.comboBox_calib_mfc_1.currentIndexChanged.connect(lambda new_index: self.on_calib_mfc_changed(1, new_index))
        self.comboBox_calib_mfc_2.currentIndexChanged.connect(lambda new_index: self.on_calib_mfc_changed(2, new_index))
        self.pushButton_calib_estimate.clicked.connect(self.recalculate_calib_estimations)
        self.widget_graph_calib.setBackground('w')
        self.widget_graph_calib.setLabel('left', 'Slope (m°C/s)')
        self.widget_graph_calib.setLabel('bottom', 'Flow (l/h)')
        self.widget_graph_calib.addLegend(offset=(440, 25))
        self.widget_graph_calib.showGrid(x=True, y=True)
        self.widget_graph_calib.setTitle('Latest calibration data')
        self.calibration_signal.connect(lambda p, e, m, b, f, s: self.on_calibration_signal(p, e, m, b, f, s))
        self.on_checkBox_calib_use_2_mfcs_changed(False)
        self.checkBox_calib_use_2_mfcs.stateChanged.connect(lambda is_checked: self.on_checkBox_calib_use_2_mfcs_changed(is_checked))
        self.checkBox_calib_use_bpr.stateChanged.connect(lambda use_bpr: self.on_checkBox_calib_use_bpr_changed(use_bpr))
        self.on_checkBox_calib_use_bpr_changed(False)
        for i in range(1, 65):
            line_obj = self.findChild(QtWidgets.QLineEdit, f'lineEdit_calib_{i}')
            line_obj.textEdited.connect(lambda text, num_barcode=i: self.trim_barcode(num_barcode))
    
    
    def get_elmb(self, id):
        '''Return the ELMB with the given CAN-bus node ID'''
        if id == 1:
            return self.elmb1
        elif id == 2:
            return self.elmb2
        elif id == 3:
            return self.elmb3
        else:
            raise Exception(f'MainWindow.get_elmb: ELMB with id={id} not available!')
    
    def serial_int_to_string(self, i):
        '''Convert an U32 integer to a 4-character string serial number.'''
        hex_str = hex(i)[2:].zfill(8)
        serial_str = ''
        for i in range(4):
            serial_str = chr(int(hex_str[2*i:2*i+2], 16)) + serial_str
        return serial_str

    def set_elmb_info(self, elmb_id):
        version_hw = self.get_elmb(elmb_id).read_can(0x1009)
        version_fw = self.get_elmb(elmb_id).get_firmware_version()
        serial = self.get_elmb(elmb_id).read_can(0x3100)
        self.label_hw.setText(version_hw)
        self.label_fw.setText(version_fw)
        self.label_serial.setText(self.serial_int_to_string(serial))
    
    def has_flowscan_34(self, elmb):
        '''Return True iff the ELMB with the given ID has Flowscan v3.4'''
        version_fw = elmb.get_firmware_version()
        return ('FL34' in version_fw)
        
    
    def check_state(self):
        adsState, deviceState = self.plc.read_state()
        if adsState != 5 or deviceState != 0:
            raise Exception(f'Beckhoff PC not in Run mode or incorrect device state! (adsState, deviceState)=({adsState}, {deviceState})')
        
    def onTabChanged(self, index_changed_to):
        # self.current_tab_index --> has previous tab index
        if self.current_tab_index == 2:
            # Left "ADC calibration" tab -> stop running calibration
            self.radioButton_adc.setChecked(False)
        if index_changed_to == 3:
            # Entered "Manual rack control" -> start measurements
            self.runnable_manual_control = RunnableManualControl(ui=self)
            self.pool.start(self.runnable_manual_control)
        if self.current_tab_index == 3:
            # Left "Manual rack control" -> Reset all & stop measurements
            self.pushButton_manual_reset.click()
            self.runnable_manual_control.stop()
            self.runnable_manual_channel.stop()
        if self.current_tab_index == 4:
            # Left "Cable check" tab -> stop running
            self.radioButton_cable.setChecked(False)
        if self.current_tab_index == 5:
            # Left "Flowcell calibration" tab -> stop running
            self.radioButton_calib_run.setChecked(False)
        # Update current_tab_index
        self.current_tab_index = index_changed_to
    
    
    ## Manual rack control tab
    def on_pushButton_manual_run_clicked(self):
        manifold_nb = self.comboBox_manual_manifold_nb.currentIndex()+1 # in [1, 4]
        channel = self.comboBox_manual_flowcell_nbs.currentIndex()+1 # in [1, 8]
        self.label_manual_slope_msg.setText(f'Measuring slopes...')
        elmb_nb = 1 if channel%2==1 else 2
        channel_elmb = 4*(manifold_nb-1) + (channel-1)//2
        self.runnable_manual_channel = RunnableManualChannelReading(ui=self, elmb_nb=elmb_nb, channel_elmb=channel_elmb)
        self.pool.start(self.runnable_manual_channel)
    
    def on_checkBox_manual_bpr_changed(self, is_checked):
        if is_checked:
            self.doubleSpinBox_setpoint_backpressure.setReadOnly(False)
            self.doubleSpinBox_setpoint_backpressure.setStyleSheet('background-color: rgb(255, 255, 255);')
            # Trigger change of setpoint in BPR
            self.on_bpr_setpoint_changed(self.doubleSpinBox_setpoint_backpressure.value())
        else:
            self.doubleSpinBox_setpoint_backpressure.setReadOnly(True)
            self.doubleSpinBox_setpoint_backpressure.setStyleSheet('background-color: rgb(222, 221, 218);')
            self.pressure_regulator.write_setpoint(0)


    def on_bpr_setpoint_changed(self, pressure):
        self.pressure_regulator.write_setpoint(pressure)

    def on_manual_channel_signal(self, success, slope1, slope2, time_measured):
        if success:
            self.label_manual_slope_input.setText( str(slope1))
            self.label_manual_slope_output.setText(str(slope2))
            self.label_manual_time_channel.setText(str(time_measured))
            self.label_manual_slope_msg.setText('Done!')
        else:
            self.label_manual_slope_input.setText('')
            self.label_manual_slope_output.setText('')
            self.label_manual_time_channel.setText('')
            self.label_manual_slope_msg.setText('Failed!')
            
    
    def on_manual_control_signal(self, p1, p2, p3, l_mfcs):
        self.label_manual_pt1.setText('{:.2f} bar'.format(p1))
        self.label_manual_pt2.setText('{:.2f} bar'.format(p2))
        self.label_backpressure_indicator.setText('{:.2f} bar'.format(p3))
        labels_mfcs = [self.label_mfc1_flow, self.label_mfc2_flow, self.label_mfc3_flow, self.label_mfc4_flow]
        for i in range(self.mfcs.nb_mfcs):
            labels_mfcs[i].setText('{:.2f} l/h'.format(l_mfcs[i]))
        
        
    def on_valve_clicked(self, num):
        if num < 12:
            b_new = not self.valves_nc.read_valve_state(num)
        else:
            b_new = not self.valve_no.read_valve_state()
        self.set_valve_state(b_new, num)
            
    
    def set_valve_state(self, state, num):
        '''Set the valve with the given number to the given state.
        A state equal to True refers to an energized valve: open for a normally closed valve and vice versa.'''
        if num < 12:
            self.valves_nc.write_valve_state(num, state)
            if state:
                self.buttons_valves[num-1].setIcon(QtGui.QIcon(relative_path('img', 'valve_open.png')))
            else:
                self.buttons_valves[num-1].setIcon(QtGui.QIcon(relative_path('img', 'valve_closed.png')))
        else:
            self.valve_no.write_valve_state(state)
            if state:
                self.pushButton_valve12.setIcon(QtGui.QIcon(relative_path('img', 'valve_closed.png')))
            else:
                self.pushButton_valve12.setIcon(QtGui.QIcon(relative_path('img', 'valve_open.png')))
    
    def on_manual_mfc_changed(self, new_index):
        mfc = self.mfcs.mfcs[new_index]
        self.current_mfc = mfc
        # Update comboBox with fluids
        self.comboBox_manual_fluid.blockSignals(True)
        self.comboBox_manual_fluid.clear()
        for fluid_str in mfc.fluid_strings:
            self.comboBox_manual_fluid.addItem(fluid_str.replace(' ||| ', ', '))
        self.comboBox_manual_fluid.blockSignals(False)
        # Set to correct fluid
        self.mfc_block_fluid_setpoint_reset = True
        fluid_number = mfc.read_fluid_number()
        self.comboBox_manual_fluid.setCurrentIndex(fluid_number)
        self.on_manual_fluid_changed(fluid_number)
        self.mfc_block_fluid_setpoint_reset = False
        # Read setpoint and update spinBox
        setpoint = mfc.read_setpoint()
        self.doubleSpinBox_setpoint_mfc.setValue(setpoint)
        
    def on_manual_fluid_changed(self, new_index):
        mfc = self.current_mfc
        # Write fluid number
        mfc.write_fluid_number(new_index)
        self.current_mfc_capacity = mfc.read_capacity()
        # Update label
        labels_mfc_fluid_names = [self.label_mfc1_name, self.label_mfc2_name, self.label_mfc3_name, self.label_mfc4_name]
        mfc_index = self.comboBox_manual_mfc.currentIndex()
        text_mix, text_flow = mfc.fluid_strings[new_index].split(' ||| ')
        text = text_mix + '\n' + text_flow
        labels_mfc_fluid_names[mfc_index].setText(text)
        # Reset setpoint
        if not self.mfc_block_fluid_setpoint_reset:
            self.on_mfc_setpoint_changed(0)
        
    def on_mfc_setpoint_changed(self, setpoint):
        # Write setpoint & update label
        sp_min, sp_max = self.current_mfc_capacity
        setpoint = max(min(setpoint, sp_max), sp_min)
        self.current_mfc.write_setpoint(setpoint)
        self.doubleSpinBox_setpoint_mfc.setValue(setpoint)
    
    def on_manual_control_reset(self):
        for num in range(1, 13):
            self.set_valve_state(False, num)
        self.mfcs.reset_mfcs()
        self.on_mfc_setpoint_changed(0)
        # Reset backpressure regulator
        self.checkBox_manual_bpr.setChecked(False)
    
    
    ## Cable check tab 
    def on_cable_check_signal_initial(self, num, current1, current2, current3, current4):
        if num == -2:
            self.progressBar_cable.setValue(0)
            for i in range(4):
                self.draw_graph_tab4(i, height=[0]*16)
            self.label_progress_cable.setText(f'Measuring I_low...')
        elif num == -1:
            self.label_cable_current_inflow1.setText( f'  I_low_inflow1   = {round(current1*1e3, 3)} mA')
            self.label_cable_current_outflow1.setText(f'  I_low_outflow1 = {round(current2*1e3, 3)} mA')
            self.label_cable_current_inflow2.setText( f'  I_low_inflow2   = {round(current3*1e3, 3)} mA')
            self.label_cable_current_outflow2.setText(f'  I_low_outflow2 = {round(current4*1e3, 3)} mA')
        
    def on_cable_check_signal(self, ch_nb, measurements):
        '''ch_nb = number of channel in [0, 15] that is currently being read'''
        self.logger.debug(f'Got cable_check_signal: ch_nb={ch_nb}')
        matrix_measurements = np.resize(measurements, (4,16))
        for i in range(4):
            self.draw_graph_tab4(i, height=matrix_measurements[i])
        nb_channels = self.runnable_cable.nb_channels
        self.progressBar_cable.setValue( 100 * ch_nb // ((nb_channels+1)//2))
        if ch_nb == ( (nb_channels+1)//2 ):
            self.radioButton_cable.setChecked(False)
            return
        self.label_progress_cable.setText(f'Measuring channels {ch_nb+1}/{(nb_channels+1)//2}...')
    
    def draw_graph_tab4(self, num, height, x=range(1,17)):
        '''Draw a bar graph in the given PlotWidget in Tab 4 (Cable check)'''
        self.draw_bar_graph_item(self.widget_graphs_cable[num], x, height)

    def draw_bar_graph_item(self, plot_widget, x, height, width=0.5):
        self.remove_items_from_widget(plot_widget)
        plot_widget.addItem(pg.BarGraphItem(x=x, height=height, width=width, brush=(0, 100, 200)))

    def remove_items_from_widget(self, plot_widget):
        for item in plot_widget.listDataItems():
            plot_widget.removeItem(item)
    
    def on_cable_check_run_pressed(self, checked):
        if checked:
            nb_channels = min(32, self.spinBox_cable_nb.value()//2)
            self.spinBox_cable_nb.setValue(2*nb_channels)
            self.runnable_cable = RunnableCableCheck(ui=self, nb_channels=nb_channels)
            self.pool.start(self.runnable_cable)
        else:
            self.runnable_cable.stop()
            self.elmb1.set_adc_mode('off')
            self.elmb2.set_adc_mode('off')
            if self.progressBar_cable.value()==100:
                self.label_progress_cable.setText(f'Measurements finished!')
                self.plot_median_cable_graph()
            else:
                self.label_progress_cable.setText(f'Measurements stopped.')
    
    def plot_median_cable_graph(self):
        nb_ch = self.runnable_cable.nb_channels
        median_temp = np.median(self.runnable_cable.measurements[:(2*nb_ch)])
        nb_manifolds = nb_ch//8 + (nb_ch%8 != 0)
        for i in range(nb_manifolds):
            wg = self.widget_graphs_cable[i]
            wg.getPlotItem().plot([1,16], [median_temp]*2, pen={'color':'r', 'width':2}, name='median')
        
    def on_cable_run_clicked(self):
        manifold_nb = self.comboBox_cable_manifold_nb.currentIndex()+1 # in [1, 4]
        channel = self.comboBox_cable_flowcell_nbs.currentIndex()+1 # in [1, 8]
        self.label_cable_one_channel.setText(f' Reading manifold {manifold_nb}: flowcells {self.comboBox_cable_flowcell_nbs.currentText()}...')
        self.label_cable_one_channel.repaint()
        channel_global = 8*(manifold_nb-1) + (channel-1)
        elmb_nb = 1 if channel%2==1 else 2
        channel_elmb = 4*(manifold_nb-1) + (channel-1)//2
        self.runnable_cable.measure_1_channel(channel_global, elmb_nb, channel_elmb)
        matrix_measurements = np.resize(self.runnable_cable.measurements, (4,16))
        for i in range(4):
            self.draw_graph_tab4(i, height=matrix_measurements[i])
        self.plot_median_cable_graph()
        self.label_cable_one_channel.setText(' Done!')
        
    
    ## ADC calibration tab
    def onADCRunPressed(self, checked):
        elmb_id = self.comboBox_adc_elmb_id.currentIndex() + 1
        elmb = self.get_elmb(elmb_id)
        if checked:
            elmb.set_adc_mode(self.comboBox_adc_current.currentText())
            self.runnable_adc = RunnableADCCalibration(ui=self, elmb=elmb)
            self.pool.start(self.runnable_adc)
        else:
            self.runnable_adc.stop()
            elmb.set_adc_mode('off')
    
    def on_adc_sample_signal(self, data_potentio_inflow, data_potentio_outflow):
        nb_samples = len(data_potentio_inflow)
        sample_period = 1.0/self.spinBox_adc_rate.value()
        tt = [i*sample_period for i in range(-nb_samples+1, 1)]
        low_current_selected = self.comboBox_adc_current.currentText() == 'low'
        desired_voltage = 0.1450 if low_current_selected else 3.1005
        self.line_adc_voltage_red.setData( tt, data_potentio_inflow)
        self.line_adc_voltage_blue.setData(tt, data_potentio_outflow)
        self.line_adc_limit_lower.setData( [tt[0], tt[-1]], [desired_voltage-0.0001]*2)
        self.line_adc_limit_upper.setData( [tt[0], tt[-1]], [desired_voltage+0.0001]*2)
    
    def on_adc_settings_changed(self):
        self.radioButton_adc.setChecked(False)
    
    def on_adc_clear_graph(self):
        self.runnable_adc.clear_data = True
    
    ## Configuration tab
    def on_elmb_id_changed(self):
        elmb_id = self.comboBox_config_elmb_id.currentIndex() + 1
        if elmb_id == 3:
            self.set_flowscan_config_enable(False)
            self.on_elmbio_type_changed()
        else:
            self.set_flowscan_config_enable(True)
            self.set_elmbio_valve_config_enable(False)
            self.set_elmbio_pressure_config_enable(False)
    
    def on_elmbio_type_changed(self):
        valve_selected = self.comboBox_elmbio_type.currentIndex() == 0
        self.set_elmbio_valve_config_enable(valve_selected)
        self.set_elmbio_pressure_config_enable(not valve_selected)

    def set_flowscan_config_enable(self, enable):
        self.comboBox_elmbio_type.setEnabled(not enable)
        self.spinBox_samples_N.setReadOnly(not enable)
        self.spinBox_samples_M.setReadOnly(not enable)
        self.spinBox_DT.setReadOnly(not enable)
        self.spinBox_DT_timeout.setReadOnly(not enable)
        self.spinBox_settling.setReadOnly(not enable)
        self.spinBox_delay.setReadOnly(not enable)
        self.spinBox_opto_delay.setReadOnly(not enable)
    
    def set_elmbio_valve_config_enable(self, enable):
        self.checkBox_2140.setEnabled(enable)
        self.spinBox_2150.setReadOnly(not enable)
        self.checkBox_2300.setEnabled(enable)
        self.checkBox_6423.setEnabled(enable)
        self.doubleSpinBox_6424.setReadOnly(not enable)
        self.doubleSpinBox_6425.setReadOnly(not enable)
    
    def set_elmbio_pressure_config_enable(self, enable):
        self.comboBox_1802sub2.setEnabled(enable)
        self.spinBox_1802sub5.setReadOnly(not enable)
    

    def config_write_general_params(self, elmb):
        if self.has_flowscan_34(elmb):
            elmb.write_can(int(1000*self.spinBox_heartbeat.value()), 0x1017)
        else:
            elmb.write_can(int(self.spinBox_heartbeat.value()), 0x1017)
        elmb.write_can(self.spinBox_lifetime.value(), 0x100D)
        elmb.write_can(self.spinBox_busoff.value(),   0x3200, 3)
    
    def config_write_adc_params(self, elmb):
        elmb.write_can(self.spinBox_channels.value(), 0x2100, 1)
        elmb.write_adc_rate(self.comboBox_rate.currentText())
        # NOTE for ELMBio, writing ADC range sometimes fails on the first attempt
        try_n_times(lambda data=self.comboBox_range.currentText(): elmb.write_adc_range(data))
        elmb.write_adc_mode(self.comboBox_mode.currentText())
    
    def config_write_flowscan_params(self, elmb):
        transmission_type = 1 if self.checkBox_sync.isChecked() else 255
        elmb.write_can(transmission_type,               0x1803, 2)
        elmb.write_can(self.spinBox_samples_N.value(),  0x4000, 2)
        elmb.write_can(self.spinBox_samples_M.value(),  0x4000, 3)
        elmb.write_can(self.spinBox_DT.value(),         0x4000, 4)
        elmb.write_can(self.spinBox_DT_timeout.value(), 0x4000, 5)
        elmb.write_can(self.spinBox_settling.value(),   0x4000, 6)
        elmb.write_can(self.spinBox_delay.value(),      0x4000, 10)
        elmb.write_can(self.spinBox_opto_delay.value(), 0x2100, 19)

    def config_write_valve_params(self, elmb):
        elmb.write_can(self.checkBox_2140.isChecked(),  0x2140)
        elmb.write_can(self.spinBox_2150.value(),       0x2150)
        elmb.write_can(self.checkBox_2300.isChecked(),  0x2300)
        elmb.write_can(self.checkBox_6423.isChecked(),  0x6423)
        elmb.write_can(self.doubleSpinBox_6424.value()*1e6, 0x6424, 255) # Write in micro-V
        elmb.write_can(self.doubleSpinBox_6425.value()*1e6, 0x6425, 255) # Write in micro-V
    
    def config_write_pressure_params(self, elmb):
        elmb.write_can(int(self.comboBox_1802sub2.currentText()), 0x1802, 2)
        elmb.write_can(self.spinBox_1802sub5.value(),             0x1802, 5)
        
    def get_elmb_type_selected(self, elmb_id):
        if elmb_id == 3:
            valve_selected = self.comboBox_elmbio_type.currentIndex() == 0
            if valve_selected:
                return 'valve'
            else:
                return 'pressure'
        else:
            return 'flowcell'

    def config_tab_write_clicked(self):
        elmb_id = self.comboBox_config_elmb_id.currentIndex() + 1
        elmb = self.get_elmb(elmb_id)
        elmb_type = self.get_elmb_type_selected(elmb_id)
        self.update_progress_bar(0, 'Writing general parameters...')
        self.config_write_general_params(elmb)
        self.update_progress_bar(33, 'Writing ADC parameters...')
        self.config_write_adc_params(elmb)
        if elmb_type == 'flowcell':
            self.update_progress_bar(66, 'Writing Flowscan parameters...')
            self.config_write_flowscan_params(elmb)
        elif elmb_type == 'valve':
            self.update_progress_bar(66, 'Writing analysis valve parameters...')
            self.config_write_valve_params(elmb)
        else:
            self.update_progress_bar(66, 'Writing pressure sensor parameters...')
            self.config_write_pressure_params(elmb)
        self.update_progress_bar(100, 'Written all parameters!')
    
    def config_load_general_params(self, elmb):
        heartbeat_val = elmb.read_can(0x1017)
        if self.has_flowscan_34(elmb):
            self.spinBox_heartbeat.setValue(heartbeat_val if heartbeat_val<256 else heartbeat_val/1000)
        else:
            self.spinBox_heartbeat.setValue(heartbeat_val)
        self.spinBox_lifetime.setValue(   elmb.read_can(0x100D))
        self.spinBox_busoff.setValue(     elmb.read_can(0x3200, 3))
        
    def config_load_adc_params(self, elmb):
        self.spinBox_channels.setValue(elmb.read_can(0x2100, 1))
        rate =  ElmbFlowscan.rates_adc_for_code[  elmb.read_can(0x2100, 2)]
        range = ElmbFlowscan.ranges_adc_for_code[ elmb.read_can(0x2100, 3)]
        mode =  ElmbFlowscan.modes_adc_for_code[  elmb.read_can(0x2100, 4)]
        self.comboBox_rate.setCurrentText(rate)
        self.comboBox_range.setCurrentText(range)
        self.comboBox_mode.setCurrentText(mode)
        
    def config_load_flowcell_params(self, elmb):
        self.checkBox_sync.setChecked(    elmb.read_can(0x1803, 2) == 1)
        self.spinBox_samples_N.setValue(  elmb.read_can(0x4000, 2))
        self.spinBox_samples_M.setValue(  elmb.read_can(0x4000, 3))
        self.spinBox_DT.setValue(         elmb.read_can(0x4000, 4))
        self.spinBox_DT_timeout.setValue( elmb.read_can(0x4000, 5))
        self.spinBox_settling.setValue(   elmb.read_can(0x4000, 6))
        self.spinBox_delay.setValue(      elmb.read_can(0x4000, 10))
        self.spinBox_opto_delay.setValue( elmb.read_can(0x2100, 19))
        
    def config_load_valve_params(self, elmb):
        self.checkBox_2140.setChecked(      elmb.read_can(0x2140))
        self.spinBox_2150.setValue(         elmb.read_can(0x2150))
        self.checkBox_2300.setChecked(      elmb.read_can(0x2300))
        self.checkBox_6423.setChecked(      elmb.read_can(0x6423))
        self.doubleSpinBox_6424.setValue(   elmb.read_can(0x6424, 1)/1e6)
        self.doubleSpinBox_6425.setValue(   elmb.read_can(0x6425, 1)/1e6)
        
    def config_load_pressure_params(self, elmb):
        self.comboBox_1802sub2.setCurrentIndex(0 if elmb.read_can(0x1802, 2)==1 else 1)
        self.spinBox_1802sub5.setValue(elmb.read_can(0x1802, 5))

    def config_tab_load_clicked(self):
        elmb_id = self.comboBox_config_elmb_id.currentIndex() + 1
        elmb = self.get_elmb(elmb_id)
        elmb_type = self.get_elmb_type_selected(elmb_id)
        self.update_progress_bar(0, 'Loading general parameters...')
        self.set_elmb_info(elmb_id)
        self.config_load_general_params(elmb)
        self.update_progress_bar(33, 'Loading ADC parameters...')
        self.config_load_adc_params(elmb)
        if elmb_type == 'flowcell':
            self.update_progress_bar(66, 'Loading Flowscan parameters...')
            self.config_load_flowcell_params(elmb)
        elif elmb_type == 'valve':
            self.update_progress_bar(66, 'Loading analysis valve parameters...')
            self.config_load_valve_params(elmb)
        else:
            self.update_progress_bar(66, 'Loading pressure sensor parameters...')
            self.config_load_pressure_params(elmb)
        self.update_progress_bar(100, 'Loaded all parameters!')
    
    
    def config_tab_save_clicked(self):
        elmb_id = self.comboBox_config_elmb_id.currentIndex() + 1
        elmb = self.get_elmb(elmb_id)
        self.update_progress_bar(0, 'Saving parameters...')
        elmb.write_can('save', 0x1010, 1)
        self.update_progress_bar(100, 'Saved all parameters!')

    def update_progress_bar(self, percentage, label=None):
        self.progressBar_config.setValue(percentage)
        if label:
            self.label_progress_config.setText(label)
        QtWidgets.QApplication.processEvents()
    

    ## Flowcell calibration tab
    def on_calibration_signal(self, progress_float, early_stop, mfc_flow, barcodes, flows, slopes):
        self.progressBar_calib.setValue(int(100*progress_float))
        self.label_calib_mfc_flow.setText(f'{mfc_flow:.2f} l/h')
        self.label_calib_backpressure.setText(f'{self.pressure_regulator.read_measurement():.2f} barg')
        
        self.widget_graph_calib.clear()
        xx = flows
        colors = ['r', 'g', 'b', 'm']
        for i, barcode in enumerate(barcodes):
            yy = slopes[i]
            self.widget_graph_calib.addItem(pg.PlotDataItem(
                xx, yy,
                pen=None,
                symbolBrush=pg.mkBrush(colors[i]),
                symbolSize=8
            ))
            xx2, yy2 = self.calculate_averages_for_duplicates_xx(xx, yy)
            self.widget_graph_calib.addItem(pg.PlotDataItem(xx2, yy2, pen={'color': colors[i], 'width':2}, name=f'{barcode}'))
        if progress_float == 1 or early_stop:
            self.radioButton_calib_run.setChecked(False)
            
    def calculate_averages_for_duplicates_xx(self, xx, yy):
        '''Process the given arrays such that xx does not have duplicates and is sorted.
        The values of yy are averaged if there are several corresponding values in xx.'''
        zz = list(zip(xx, yy))
        xx_unique = sorted(list(set(xx)))
        yy_unique = []
        for x in xx_unique:
            ys = []
            for e in zz:
                if e[0] == x:
                    ys.append(e[1])
            yy_unique.append(np.mean(ys))
        return xx_unique, yy_unique
    
    def trim_barcode(self, num_barcode):
        '''Some UPC-E barcodes have a leading and trailing digit that's not part of the actual identifier.
        This function transforms a barcode into a 6-digit number by removing unnecessary digits.'''
        line_obj = self.findChild(QtWidgets.QLineEdit, f'lineEdit_calib_{num_barcode}')
        text = line_obj.text().strip()
        if len(text) == 8:
            text_new = text[1:7]
            self.logger.info(f'Trimming barcode {text}, transforming to {text_new}')
            line_obj.setText(text_new)

    def on_checkBox_calib_use_bpr_changed(self, use_bpr):
        if use_bpr:
            self.doubleSpinBox_calib_backpressure.setStyleSheet('background-color: rgb(255, 255, 255);')
            self.doubleSpinBox_calib_backpressure.setReadOnly(False)
        else:
            self.doubleSpinBox_calib_backpressure.setStyleSheet('background-color: rgb(222, 221, 218);')
            self.doubleSpinBox_calib_backpressure.setReadOnly(True)

    def on_checkBox_calib_use_2_mfcs_changed(self, is_checked):
        self.comboBox_calib_mfc_2.setEnabled(is_checked)
        self.comboBox_calib_fluid_2.setEnabled(is_checked)
        if is_checked:
            self.doubleSpinBox_callb_percentage_mfc1.setStyleSheet('background-color: rgb(255, 255, 255);')
            self.doubleSpinBox_callb_percentage_mfc1.setReadOnly(False)
        else:
            self.doubleSpinBox_callb_percentage_mfc1.setValue(100.0)
            self.doubleSpinBox_callb_percentage_mfc1.setStyleSheet('background-color: rgb(222, 221, 218);')
            self.doubleSpinBox_callb_percentage_mfc1.setReadOnly(True)

    
    def on_calib_mfc_changed(self, mfc_nb, new_mfc_index):
        '''mfc_nb = first MFC or second MFC (2 MFCs can be used in parallel as mixer).
        new_mfc_index = the newly selected MFC index (0-3).'''
        # Update comboBox with fluids
        mfc = self.mfcs.mfcs[new_mfc_index]
        comboBox_fluids = self.comboBox_calib_fluid_1 if mfc_nb==1 else self.comboBox_calib_fluid_2
        comboBox_fluids.clear()
        for fluid_str in mfc.fluid_strings:
            comboBox_fluids.addItem(fluid_str.replace(' ||| ', ', '))
        
    def on_calib_nb_flows_changed(self, new_value, nb_curve):
        for nb_flow in range(1, new_value+1):
            spinbox = self.findChild(QtWidgets.QDoubleSpinBox, f'doubleSpinBox_calib_curve{nb_curve}_{nb_flow}')
            spinbox.setStyleSheet('background-color: rgb(255, 255, 255);')
            spinbox.setReadOnly(False)
        for nb_flow in range(new_value+1, 13):
            spinbox = self.findChild(QtWidgets.QDoubleSpinBox, f'doubleSpinBox_calib_curve{nb_curve}_{nb_flow}')
            spinbox.setStyleSheet('background-color: rgb(222, 221, 218);')
            spinbox.setReadOnly(True)
            spinbox.setValue(0)
    
    def on_calib_checkbox_flows2_changed(self, is_checked):
        if is_checked:
            self.on_calib_nb_flows_changed(self.spinBox_calib_nb_flows_2.value(), 2)
            self.spinBox_calib_nb_flows_2.setStyleSheet('background-color: rgb(255, 255, 255);')
            self.spinBox_calib_nb_flows_2.setReadOnly(False)
            self.doubleSpinBox_calib_max2.setStyleSheet('background-color: rgb(255, 255, 255);')
            self.doubleSpinBox_calib_max2.setReadOnly(False)
        else:
            # Disable input elements
            self.on_calib_nb_flows_changed(0, 2)
            self.spinBox_calib_nb_flows_2.setStyleSheet('background-color: rgb(222, 221, 218);')
            self.spinBox_calib_nb_flows_2.setReadOnly(True)
            self.doubleSpinBox_calib_max2.setStyleSheet('background-color: rgb(222, 221, 218);')
            self.doubleSpinBox_calib_max2.setReadOnly(True)
        
    def on_calib_save_file(self):
        nb_flowcells = self.spinBox_calib_nb_flowcells.value()
        with open(relative_path('data', 'barcodes.csv'), 'w') as f:
            f.write('nb_flowcell,barcode\n')
            for i in range(1, nb_flowcells+1):
                line_obj = self.findChild(QtWidgets.QLineEdit, f'lineEdit_calib_{i}')
                barcode = line_obj.text()
                if barcode != '':
                    f.write(f'{i},{barcode}\n')
    
    def on_calib_load_file(self):
        max_nb_flowcell = 2
        with open(relative_path('data', 'barcodes.csv'), 'r') as f:
            lines = f.readlines()[1:]
        for line in lines:
            i, barcode = line.split(',')
            max_nb_flowcell = max(int(i), max_nb_flowcell)
            line_obj = self.findChild(QtWidgets.QLineEdit, f'lineEdit_calib_{i}')
            line_obj.setText(barcode.strip())
        self.spinBox_calib_nb_flowcells.setValue(max_nb_flowcell + (max_nb_flowcell%2))
    
    def get_calib_limit_flows_MFC(self):
        '''Return the minimum non-zero and maximum flow based on the saved fluid strings.
        For 2 MFCs used in parallel, this involves calculating which MFC is the bottleneck.'''
        mfc1 = self.mfcs.mfcs[self.comboBox_calib_mfc_1.currentIndex()]
        min_flow_MFC1, max_flow_MFC1 = mfc1.get_fluid_str_capacity(self.comboBox_calib_fluid_1.currentIndex())
        if self.checkBox_calib_use_2_mfcs.isChecked():
            percentage_mfc1 = self.doubleSpinBox_callb_percentage_mfc1.value()
            mfc2 = self.mfcs.mfcs[self.comboBox_calib_mfc_2.currentIndex()]
            min_flow_MFC2, max_flow_MFC2 = mfc2.get_fluid_str_capacity(self.comboBox_calib_fluid_2.currentIndex())
            min_flow = max(
                min_flow_MFC1*100.0/percentage_mfc1,
                min_flow_MFC2*100.0/(100.0-percentage_mfc1)
            )
            max_flow = min(
                max_flow_MFC1*100.0/percentage_mfc1,
                max_flow_MFC2*100.0/(100.0-percentage_mfc1)
            )
            self.logger.debug(f'MFC combination min, max flow = ({min_flow:.2f}, {max_flow:.2f})')
            return min_flow, max_flow
        return min_flow_MFC1, max_flow_MFC1

    def get_calib_min_flow_MFC(self):
        '''Return the minimum non-zero flow based on the saved fluid strings.'''
        return self.get_calib_limit_flows_MFC()[0]
    
    def autofill_calib_flows(self):
        '''Autofill calibration flows. For curve 1, more points are taken closer to 0. For curve 2, a uniform distribution is chosen.'''
        min_flow_MFC = self.get_calib_min_flow_MFC()
        max_flow_curve1 = self.doubleSpinBox_calib_max1.value()
        max_flow_curve2 = self.doubleSpinBox_calib_max2.value()
        if self.checkBox_calib_enable_flows2.isChecked() and max_flow_curve2 < max_flow_curve1:
            raise Exception(f'The max flow of curve 2 ({max_flow_curve2}) should be higher than curve 1 ({max_flow_curve1})!')
        for nb_curve in [1,2]:
            if nb_curve == 2 and not self.checkBox_calib_enable_flows2.isChecked():
                continue # Do not autofill flows
            max_flow = max_flow_curve1 if nb_curve==1 else max_flow_curve2
            nb_flows = self.findChild(QtWidgets.QSpinBox, f'spinBox_calib_nb_flows_{nb_curve}').value()
            # Set list with flows
            if nb_curve == 1 and self.checkBox_calib_nonuniform.isChecked():
                # Set distribution of flows as average of uniform and logarithmic distribution
                s1 = np.linspace(0, max_flow, nb_flows)
                s2 = np.geomspace(min_flow_MFC, max_flow, nb_flows-1)
                s2 = [0] + [f for f in s2]
                flows = [(f1+f2)/2 for f1,f2 in zip(s1,s2)]
            else:  
                min_flow = 0 if nb_curve==1 else max_flow_curve1
                flows = np.linspace(min_flow, max_flow, nb_flows)
            for i in range(nb_flows):
                flow = flows[i]
                if flow > 0:
                    flow = max(flows[i], min_flow_MFC) # Make sure non-zero flows are not too low for MFC
                spinbox = self.findChild(QtWidgets.QDoubleSpinBox, f'doubleSpinBox_calib_curve{nb_curve}_{i+1}')
                spinbox.setValue(flow)
    
    def on_calib_nb_flowcells_changed(self, nb):
        nb_flowcells = 2*((nb+1)//2)
        for i in range(1, nb_flowcells+1):
            line_obj = self.findChild(QtWidgets.QLineEdit, f'lineEdit_calib_{i}')
            line_obj.setStyleSheet('background-color: rgb(255, 255, 255);')
            line_obj.setReadOnly(False)
        for i in range(nb_flowcells+1, 65):
            line_obj = self.findChild(QtWidgets.QLineEdit, f'lineEdit_calib_{i}')
            line_obj.setStyleSheet('background-color: rgb(222, 221, 218);')
            line_obj.setReadOnly(True)
            line_obj.clear()
    
    def recalculate_calib_estimations(self):
        nb_flows = self.spinBox_calib_nb_flows_1.value()
        if self.checkBox_calib_enable_flows2.isChecked():
            nb_flows += self.spinBox_calib_nb_flows_2.value()
            nb_flows -= 1 # First flow of second curve will not be measured
        nb_repeats = self.spinBox_calib_nb_repeats.value()
        nb_flowcells = self.spinBox_calib_nb_flowcells.value()
        time_channel = self.spinBox_time_channel_reading.value()
        nb_channels = (nb_flowcells+1)//2
        nb_channels_elmb1 = (nb_channels+1)//2 # The first ELMB is the bottleneck
        time_total = nb_flows*nb_repeats*time_channel*nb_channels_elmb1 # in seconds
        time_total = int(time_total)
        time_hours = time_total//3600
        time_minutes = (time_total//60) % 60
        self.label_calib_time_needed.setText(f'{time_hours}h {time_minutes}min')
        flows = self.get_calib_flows(1)
        if self.checkBox_calib_enable_flows2.isChecked():
            flows2 = self.get_calib_flows(2)
            flows += flows2[1:] # First flow of second curve will not be measured
        flow_avg = np.mean(flows)
        volume_total = flow_avg*nb_repeats*nb_flows*time_channel/3600*nb_channels_elmb1 # in liters
        self.label_calib_gas_consumption.setText(f'{volume_total:.1f} liters')
        self.logger.debug(f'recalculate_calib_estimations finished!\nnb_channels={nb_channels}, nb_flows={nb_flows}, flow_avg={flow_avg}')
        
    
    def get_calib_flows(self, nb_curve):
        '''Return a list with the calibration flows for the given curve (1 or 2)'''
        if nb_curve == 2 and not self.checkBox_calib_enable_flows2.isChecked():
            return []
        nb_flows = self.findChild(QtWidgets.QSpinBox, f'spinBox_calib_nb_flows_{nb_curve}').value()
        flows = []
        for nb_flow in range(1, nb_flows+1):
            spinbox = self.findChild(QtWidgets.QDoubleSpinBox, f'doubleSpinBox_calib_curve{nb_curve}_{nb_flow}')
            flows.append(spinbox.value())
        return flows
    
    def stop_calib_and_raise_exception(self, msg):
        self.radioButton_calib_run.setChecked(False)
        raise Exception(msg)
    
    def check_calib_inputs(self, flows1, flows2, barcodes):
        # Check that flows are higher than MFC minimum
        min_flow_MFC, max_flow_MFC = self.get_calib_limit_flows_MFC()
        for flow in flows1 + flows2:
            MFC_str = 'MFCs' if self.checkBox_calib_use_2_mfcs.isChecked() else 'MFC'
            if flow > 0 and flow < min_flow_MFC:
                self.stop_calib_and_raise_exception(f'Flow={flow} is too low for {MFC_str}!')
            if flow > max_flow_MFC:
                self.stop_calib_and_raise_exception(f'Flow={flow} is too high for {MFC_str}!')
        # Check that flow ranges share value
        if len(flows2) > 0:
            if flows1[-1] != flows2[0]:
                self.stop_calib_and_raise_exception(f'Last flow of first curve {flows1[-1]} and first flow of second curve {flows2[0]} need to match!')
        # Check that there are no barcode duplicates
        for i in range(len(barcodes)):
            barcode = barcodes[i]
            if barcode in barcodes[i+1:]:
                self.stop_calib_and_raise_exception(f'Duplicate barcode for flowcell {i+1}! Barcode: {barcode}')
        
    def on_calib_run_toggled(self, is_checked):
        # If 2 MFCs are used, check that they're different
        if self.checkBox_calib_use_2_mfcs.isChecked():
            first_mfc_index =  self.comboBox_calib_mfc_1.currentIndex()
            second_mfc_index = self.comboBox_calib_mfc_2.currentIndex()
            if first_mfc_index == second_mfc_index:
                self.stop_calib_and_raise_exception('The first and second MFC need to be different! Please make a new MFC selection.')

        nb = self.spinBox_calib_nb_flowcells.value()
        nb_flowcells = 2*((nb+1)//2)
        self.spinBox_calib_nb_flowcells.setValue(nb_flowcells)
        
        if is_checked:
            self.label_calib_progress.setText('Running calibration...')
            self.progressBar_calib.setValue(0)
            delta1, delta2 = self.elmb1.read_can(0x4000, 4), self.elmb2.read_can(0x4000, 4)
            if delta1 != delta2:
                self.stop_calib_and_raise_exception(f'The ELMBs have different configurations for Delta T! ({delta1}°C and {delta2}°C)\nUse an equal configuration instead.')
            flows1 = self.get_calib_flows(1)
            flows2 = self.get_calib_flows(2)
            barcodes = []
            for i in range(1, nb_flowcells+1):
                line_obj = self.findChild(QtWidgets.QLineEdit, f'lineEdit_calib_{i}')
                m = re.search(r'^\s*\d{6}\s*$', line_obj.text())
                if not m:
                    self.stop_calib_and_raise_exception(f'Flowcell {i} has invalid barcode! Barcode: {line_obj.text()}')
                barcodes.append(int(line_obj.text()))
            self.check_calib_inputs(flows1, flows2, barcodes)
            
            self.runnable_calib = RunnableFlowcellCalibration(
                ui=self,
                barcodes=barcodes,
                flows1=flows1,
                flows2=flows2,
                nb_repeats=self.spinBox_calib_nb_repeats.value(),
                mfc1=self.mfcs.mfcs[self.comboBox_calib_mfc_1.currentIndex()],
                fluid_number1=self.comboBox_calib_fluid_1.currentIndex(),
                use_2_mfcs=self.checkBox_calib_use_2_mfcs.isChecked(),
                mfc2=self.mfcs.mfcs[self.comboBox_calib_mfc_2.currentIndex()],
                fluid_number2=self.comboBox_calib_fluid_2.currentIndex(),
                percentage_mfc1=self.doubleSpinBox_callb_percentage_mfc1.value(),
                use_backpressure_regulator=self.checkBox_calib_use_bpr.isChecked(),
                backpressure_setpoint=self.doubleSpinBox_calib_backpressure.value()
            )
            self.pool.start(self.runnable_calib)
        else:
            self.runnable_calib.stop()
            self.mfcs.reset_mfcs()
            self.pressure_regulator.write_setpoint(0)
            self.label_calib_progress.setText('Stopped calibration')
            self.label_calib_mfc_flow.setText('')
            # De-energize all valves
            for valve_nb in range(1, 13):
                self.set_valve_state(False, valve_nb)
    

    def on_close_window(self):
        self.runnable_adc.stop()
        self.runnable_cable.stop()
        self.runnable_manual_control.stop()
        self.runnable_manual_channel.stop()
        self.runnable_calib.stop()
        try:
            self.elmb1.set_adc_mode('off')
        except:
            pass
        try:
            self.elmb2.set_adc_mode('off')
        except:
            pass
        try:
            self.mfcs.reset_mfcs()
        except:
            pass
        try:
            self.pressure_regulator.write_setpoint(0)
        except:
            pass
        try:
            self.valves_nc.close_all_valves()
            self.valve_no.write_valve_state(False)
        except:
            pass
        try:
            self.plc.close()
        except:
            pass


def excepthook(exc_type, exc_value, exc_tb):
    tb = ''.join(traceback.format_exception(exc_type, exc_value, exc_tb))
    logging.error('Error catched!\n' + tb)
    window.textBrowser_error.append(tb)


if __name__=='__main__':
    
    LOGGING_LEVEL = 'DEBUG'
    sys.excepthook = excepthook
    logging.basicConfig(level=LOGGING_LEVEL, format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    plc = pyads.Connection('127.0.0.1.1.1', 851)
    plc.open()
    
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow(plc)

    # window.showMaximized() # NOTE enable this line to maximize window at time of opening
    
    window.show()
    app.exec()

