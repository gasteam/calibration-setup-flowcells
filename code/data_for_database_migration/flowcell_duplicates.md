# TOTAL NB OF DUPLICATES = 196 

## Legend:
experiment, system, rack, position, channel (1-18), is_input_channel


Duplicate id: 383004, #duplicates=2
ALICE, HMP, 61, 1, 2, False
ALICE, HMP, 61, 1, 5, True
NOTE no webmas txt
TODO first flowcell location actually has 383024

Duplicate id: 203058, #duplicates=2
ALICE, TOF, 61, 1, 1, True
ALICE, TOF, 62, 1, 8, True
NOTE no webmas txt
TODO second flowcel location actually has 203085

NOTE ALICE CPV rack 62 has correct flowcells, but all mangled up...

Duplicate id: 601127, #duplicates=2
ATLAS, MDT, 61, 1, 6, True
ATLAS, MDT, 61, 2, 5, True
NOTE rack in stop
NOTE same webmas calibration params
TODO first flowcell location actually has 601130

Duplicate id: 105583, #duplicates=2
ATLAS, TGC, 74, 1, 12, False
ATLAS, TGC, 69, 1, 2, False
NOTE same webmas calibration params

Duplicate id: 105553, #duplicates=2
ATLAS, TGC, 70, 1, 8, False
ATLAS, TGC, 69, 1, 6, False
NOTE same webmas calibration params



Duplicate id: 202017, #duplicates=2
CMS, CSC, 63, 1, 5, True
CMS, CSC, 65, 1, 18, False
Duplicate id: 202089, #duplicates=2
CMS, CSC, 64, 1, 4, False
CMS, CSC, 64, 1, 10, False
NOTE no webmas txt


Duplicate id: 301224, #duplicates=2
CMS, DT, 64, 3, 7, True
CMS, DT, 64, 3, 8, True
NOTE different calibration params

Duplicate id: 301283, #duplicates=2
CMS, DT, 65, 1, 2, False
CMS, DT, 65, 1, 12, True
NOTE same webmas calibration params

Duplicate id: 301730, #duplicates=2
CMS, DT, 62, 1, 10, False
CMS, DT, 62, 2, 7, True
NOTE same webmas calibration params

Duplicate id: 301723, #duplicates=2
CMS, DT, 62, 1, 11, True
CMS, DT, 62, 1, 13, True
NOTE same webmas calibration params


NOTE CMS GEM racks 63 and 66 not installed ! --> All CMS GEM flowcells should be fine

Duplicate id: 109250, #duplicates=3
CMS, GEM, 66, 1, 1, True
CMS, GEM, 62, 1, 1, True
CMS, GEM, 63, 1, 1, True
Duplicate id: 109270, #duplicates=3
CMS, GEM, 66, 1, 1, False
CMS, GEM, 62, 1, 1, False
CMS, GEM, 63, 1, 1, False
Duplicate id: 109251, #duplicates=3
CMS, GEM, 66, 1, 2, True
CMS, GEM, 62, 1, 2, True
CMS, GEM, 63, 1, 2, True
Duplicate id: 109271, #duplicates=3
CMS, GEM, 66, 1, 2, False
CMS, GEM, 62, 1, 2, False
CMS, GEM, 63, 1, 2, False
Duplicate id: 109252, #duplicates=3
CMS, GEM, 66, 1, 3, True
CMS, GEM, 62, 1, 3, True
CMS, GEM, 63, 1, 3, True
Duplicate id: 109272, #duplicates=3
CMS, GEM, 66, 1, 3, False
CMS, GEM, 62, 1, 3, False
CMS, GEM, 63, 1, 3, False
Duplicate id: 109253, #duplicates=3
CMS, GEM, 66, 1, 4, True
CMS, GEM, 62, 1, 4, True
CMS, GEM, 63, 1, 4, True
Duplicate id: 109273, #duplicates=3
CMS, GEM, 66, 1, 4, False
CMS, GEM, 62, 1, 4, False
CMS, GEM, 63, 1, 4, False
Duplicate id: 109254, #duplicates=3
CMS, GEM, 66, 1, 5, True
CMS, GEM, 62, 1, 5, True
CMS, GEM, 63, 1, 5, True
Duplicate id: 109274, #duplicates=3
CMS, GEM, 66, 1, 5, False
CMS, GEM, 62, 1, 5, False
CMS, GEM, 63, 1, 5, False
Duplicate id: 109255, #duplicates=3
CMS, GEM, 66, 1, 6, True
CMS, GEM, 62, 1, 6, True
CMS, GEM, 63, 1, 6, True
Duplicate id: 109275, #duplicates=3
CMS, GEM, 66, 1, 6, False
CMS, GEM, 62, 1, 6, False
CMS, GEM, 63, 1, 6, False
Duplicate id: 109256, #duplicates=3
CMS, GEM, 66, 1, 7, True
CMS, GEM, 62, 1, 7, True
CMS, GEM, 63, 1, 7, True
Duplicate id: 109276, #duplicates=3
CMS, GEM, 66, 1, 7, False
CMS, GEM, 62, 1, 7, False
CMS, GEM, 63, 1, 7, False
Duplicate id: 109257, #duplicates=3
CMS, GEM, 66, 1, 8, True
CMS, GEM, 62, 1, 8, True
CMS, GEM, 63, 1, 8, True
Duplicate id: 109277, #duplicates=3
CMS, GEM, 66, 1, 8, False
CMS, GEM, 62, 1, 8, False
CMS, GEM, 63, 1, 8, False
Duplicate id: 109258, #duplicates=3
CMS, GEM, 66, 1, 9, True
CMS, GEM, 62, 1, 9, True
CMS, GEM, 63, 1, 9, True
Duplicate id: 109278, #duplicates=3
CMS, GEM, 66, 1, 9, False
CMS, GEM, 62, 1, 9, False
CMS, GEM, 63, 1, 9, False
Duplicate id: 109259, #duplicates=3
CMS, GEM, 66, 1, 10, True
CMS, GEM, 62, 1, 10, True
CMS, GEM, 63, 1, 10, True
Duplicate id: 109279, #duplicates=3
CMS, GEM, 66, 1, 10, False
CMS, GEM, 62, 1, 10, False
CMS, GEM, 63, 1, 10, False
Duplicate id: 109260, #duplicates=3
CMS, GEM, 66, 1, 11, True
CMS, GEM, 62, 1, 11, True
CMS, GEM, 63, 1, 11, True
Duplicate id: 109280, #duplicates=3
CMS, GEM, 66, 1, 11, False
CMS, GEM, 62, 1, 11, False
CMS, GEM, 63, 1, 11, False
Duplicate id: 109261, #duplicates=3
CMS, GEM, 66, 1, 12, True
CMS, GEM, 62, 1, 12, True
CMS, GEM, 63, 1, 12, True
Duplicate id: 109281, #duplicates=3
CMS, GEM, 66, 1, 12, False
CMS, GEM, 62, 1, 12, False
CMS, GEM, 63, 1, 12, False
Duplicate id: 109262, #duplicates=3
CMS, GEM, 66, 1, 13, True
CMS, GEM, 62, 1, 13, True
CMS, GEM, 63, 1, 13, True
Duplicate id: 109282, #duplicates=3
CMS, GEM, 66, 1, 13, False
CMS, GEM, 62, 1, 13, False
CMS, GEM, 63, 1, 13, False
Duplicate id: 109263, #duplicates=3
CMS, GEM, 66, 1, 14, True
CMS, GEM, 62, 1, 14, True
CMS, GEM, 63, 1, 14, True
Duplicate id: 109283, #duplicates=3
CMS, GEM, 66, 1, 14, False
CMS, GEM, 62, 1, 14, False
CMS, GEM, 63, 1, 14, False
Duplicate id: 109266, #duplicates=3
CMS, GEM, 66, 1, 15, True
CMS, GEM, 62, 1, 15, True
CMS, GEM, 63, 1, 15, True
Duplicate id: 109284, #duplicates=3
CMS, GEM, 66, 1, 15, False
CMS, GEM, 62, 1, 15, False
CMS, GEM, 63, 1, 15, False
Duplicate id: 109267, #duplicates=3
CMS, GEM, 66, 1, 16, True
CMS, GEM, 62, 1, 16, True
CMS, GEM, 63, 1, 16, True
Duplicate id: 109285, #duplicates=3
CMS, GEM, 66, 1, 16, False
CMS, GEM, 62, 1, 16, False
CMS, GEM, 63, 1, 16, False
Duplicate id: 109268, #duplicates=3
CMS, GEM, 66, 1, 17, True
CMS, GEM, 62, 1, 17, True
CMS, GEM, 63, 1, 17, True
Duplicate id: 109286, #duplicates=3
CMS, GEM, 66, 1, 17, False
CMS, GEM, 62, 1, 17, False
CMS, GEM, 63, 1, 17, False
Duplicate id: 109269, #duplicates=3
CMS, GEM, 66, 1, 18, True
CMS, GEM, 62, 1, 18, True
CMS, GEM, 63, 1, 18, True
Duplicate id: 109287, #duplicates=3
CMS, GEM, 66, 1, 18, False
CMS, GEM, 62, 1, 18, False
CMS, GEM, 63, 1, 18, False


Duplicate id: 113095, #duplicates=2
CMS, RPC, 73, 2, 13, False
CMS, RPC, 64, 1, 13, True
NOTE same webmas calibration params

Duplicate id: 103033, #duplicates=2
CMS, RPC, 70, 1, 3, False
CMS, RPC, 64, 1, 1, True
NOTE same webmas calibration params

Duplicate id: 103001, #duplicates=5
CMS, RPC, 74, 1, 10, True
CMS, TOTEM_GAS, 61, 1, 1, True
CMS, TOTEM_GAS, 61, 1, 7, True
CMS, TOTEM_T1, 61, 1, 1, True
CMS, TOTEM_T1, 61, 1, 7, True

Duplicate id: 103132, #duplicates=2
CMS, RPC, 74, 2, 12, False
CMS, RPC, 68, 2, 8, False
NOTE same webmas calibration params

Duplicate id: 103156, #duplicates=2
CMS, RPC, 85, 1, 4, True
CMS, RPC, 68, 1, 3, True
NOTE same webmas calibration params

Duplicate id: 103157, #duplicates=2
CMS, RPC, 85, 1, 5, True
CMS, RPC, 68, 1, 4, False
NOTE same webmas calibration params

Duplicate id: 103162, #duplicates=2
CMS, RPC, 85, 1, 5, False
CMS, RPC, 86, 2, 10, False
NOTE same webmas calibration params

Duplicate id: 103163, #duplicates=2
CMS, RPC, 85, 1, 8, False
CMS, RPC, 68, 1, 4, True
NOTE same webmas calibration params


Duplicate id: 102603, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 1, False
CMS, TOTEM_T1, 61, 1, 1, False
Duplicate id: 102640, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 2, True
CMS, TOTEM_T1, 61, 1, 2, True
Duplicate id: 102602, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 2, False
CMS, TOTEM_T1, 61, 1, 2, False
Duplicate id: 102634, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 3, True
CMS, TOTEM_T1, 61, 1, 3, True
Duplicate id: 102611, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 3, False
CMS, TOTEM_T1, 61, 1, 3, False
Duplicate id: 102635, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 4, True
CMS, TOTEM_T1, 61, 1, 4, True
Duplicate id: 102609, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 4, False
CMS, TOTEM_T1, 61, 1, 4, False
Duplicate id: 102614, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 5, True
CMS, TOTEM_T1, 61, 1, 5, True
Duplicate id: 102608, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 5, False
CMS, TOTEM_T1, 61, 1, 5, False
Duplicate id: 102606, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 6, True
CMS, TOTEM_T1, 61, 1, 6, True
Duplicate id: 102601, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 6, False
CMS, TOTEM_T1, 61, 1, 6, False
Duplicate id: 102610, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 7, False
CMS, TOTEM_T1, 61, 1, 7, False
Duplicate id: 102616, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 8, True
CMS, TOTEM_T1, 61, 1, 8, True
Duplicate id: 102613, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 8, False
CMS, TOTEM_T1, 61, 1, 8, False
Duplicate id: 102639, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 9, True
CMS, TOTEM_T1, 61, 1, 9, True
Duplicate id: 102612, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 9, False
CMS, TOTEM_T1, 61, 1, 9, False
Duplicate id: 102641, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 10, True
CMS, TOTEM_T1, 61, 1, 10, True
Duplicate id: 102615, #duplicates=2
CMS, TOTEM_GAS, 61, 1, 10, False
CMS, TOTEM_T1, 61, 1, 10, False
Duplicate id: 109141, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 1, True
CMS, TOTEM_T2, 61, 1, 1, True
Duplicate id: 109105, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 1, False
CMS, TOTEM_T2, 61, 1, 1, False
Duplicate id: 109118, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 2, True
CMS, TOTEM_T2, 61, 1, 2, True
Duplicate id: 109106, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 2, False
CMS, TOTEM_T2, 61, 1, 2, False
Duplicate id: 109145, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 3, True
CMS, TOTEM_T2, 61, 1, 3, True
Duplicate id: 109104, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 3, False
CMS, TOTEM_T2, 61, 1, 3, False
Duplicate id: 109146, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 4, True
CMS, TOTEM_T2, 61, 1, 4, True
Duplicate id: 109108, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 4, False
CMS, TOTEM_T2, 61, 1, 4, False
Duplicate id: 109147, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 5, True
CMS, TOTEM_T2, 61, 1, 5, True
Duplicate id: 109107, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 5, False
CMS, TOTEM_T2, 61, 1, 5, False
Duplicate id: 109148, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 6, True
CMS, TOTEM_T2, 61, 1, 6, True
Duplicate id: 109151, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 6, False
CMS, TOTEM_T2, 61, 1, 6, False
Duplicate id: 109131, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 7, True
CMS, TOTEM_T2, 61, 1, 7, True
Duplicate id: 109119, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 7, False
CMS, TOTEM_T2, 61, 1, 7, False
Duplicate id: 109130, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 8, True
CMS, TOTEM_T2, 61, 1, 8, True
Duplicate id: 109140, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 8, False
CMS, TOTEM_T2, 61, 1, 8, False
Duplicate id: 109143, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 9, True
CMS, TOTEM_T2, 61, 1, 9, True
Duplicate id: 109116, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 9, False
CMS, TOTEM_T2, 61, 1, 9, False
Duplicate id: 109144, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 10, True
CMS, TOTEM_T2, 61, 1, 10, True
Duplicate id: 109117, #duplicates=2
CMS, TOTEM_GAS, 61, 2, 10, False
CMS, TOTEM_T2, 61, 1, 10, False
Duplicate id: 102619, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 1, True
CMS, TOTEM_T1, 62, 1, 1, True
Duplicate id: 102605, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 1, False
CMS, TOTEM_T1, 62, 1, 1, False
Duplicate id: 102629, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 2, True
CMS, TOTEM_T1, 62, 1, 2, True
Duplicate id: 102617, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 2, False
CMS, TOTEM_T1, 62, 1, 2, False
Duplicate id: 102630, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 3, True
CMS, TOTEM_T1, 62, 1, 3, True
Duplicate id: 102638, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 3, False
CMS, TOTEM_T1, 62, 1, 3, False
Duplicate id: 102626, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 4, True
CMS, TOTEM_T1, 62, 1, 4, True
Duplicate id: 102647, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 4, False
CMS, TOTEM_T1, 62, 1, 4, False
Duplicate id: 102625, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 5, True
CMS, TOTEM_T1, 62, 1, 5, True
Duplicate id: 102645, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 5, False
CMS, TOTEM_T1, 62, 1, 5, False
Duplicate id: 102628, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 6, True
CMS, TOTEM_T1, 62, 1, 6, True
Duplicate id: 102646, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 6, False
CMS, TOTEM_T1, 62, 1, 6, False
Duplicate id: 102618, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 7, True
CMS, TOTEM_T1, 62, 1, 7, True
Duplicate id: 102644, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 7, False
CMS, TOTEM_T1, 62, 1, 7, False
Duplicate id: 102627, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 8, True
CMS, TOTEM_T1, 62, 1, 8, True
Duplicate id: 102648, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 8, False
CMS, TOTEM_T1, 62, 1, 8, False
Duplicate id: 102622, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 9, True
CMS, TOTEM_T1, 62, 1, 9, True
Duplicate id: 102649, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 9, False
CMS, TOTEM_T1, 62, 1, 9, False
Duplicate id: 102621, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 10, True
CMS, TOTEM_T1, 62, 1, 10, True
Duplicate id: 102620, #duplicates=2
CMS, TOTEM_GAS, 62, 1, 10, False
CMS, TOTEM_T1, 62, 1, 10, False
Duplicate id: 109150, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 1, True
CMS, TOTEM_T2, 62, 1, 1, True
Duplicate id: 109136, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 1, False
CMS, TOTEM_T2, 62, 1, 1, False
Duplicate id: 109124, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 2, True
CMS, TOTEM_T2, 62, 1, 2, True
Duplicate id: 109125, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 2, False
CMS, TOTEM_T2, 62, 1, 2, False
Duplicate id: 109127, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 3, True
CMS, TOTEM_T2, 62, 1, 3, True
Duplicate id: 109134, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 3, False
CMS, TOTEM_T2, 62, 1, 3, False
Duplicate id: 109156, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 4, True
CMS, TOTEM_T2, 62, 1, 4, True
Duplicate id: 109135, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 4, False
CMS, TOTEM_T2, 62, 1, 4, False
Duplicate id: 109155, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 5, True
CMS, TOTEM_T2, 62, 1, 5, True
Duplicate id: 109133, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 5, False
CMS, TOTEM_T2, 62, 1, 5, False
Duplicate id: 109154, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 6, True
CMS, TOTEM_T2, 62, 1, 6, True
Duplicate id: 109132, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 6, False
CMS, TOTEM_T2, 62, 1, 6, False
Duplicate id: 109149, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 7, True
CMS, TOTEM_T2, 62, 1, 7, True
Duplicate id: 109137, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 7, False
CMS, TOTEM_T2, 62, 1, 7, False
Duplicate id: 109153, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 8, True
CMS, TOTEM_T2, 62, 1, 8, True
Duplicate id: 109139, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 8, False
CMS, TOTEM_T2, 62, 1, 8, False
Duplicate id: 109122, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 9, True
CMS, TOTEM_T2, 62, 1, 9, True
Duplicate id: 109128, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 9, False
CMS, TOTEM_T2, 62, 1, 9, False
Duplicate id: 109152, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 10, True
CMS, TOTEM_T2, 62, 1, 10, True
Duplicate id: 109129, #duplicates=2
CMS, TOTEM_GAS, 62, 2, 10, False
CMS, TOTEM_T2, 62, 1, 10, False

Duplicate id: 102480, #duplicates=2
LHCb, GEM, 61, 1, 1, True
LHCb, MUON, 61, 2, 1, True
Duplicate id: 102475, #duplicates=2
LHCb, GEM, 61, 1, 1, False
LHCb, MUON, 61, 2, 1, False
Duplicate id: 102484, #duplicates=2
LHCb, GEM, 61, 1, 2, True
LHCb, MUON, 61, 2, 2, True
Duplicate id: 102460, #duplicates=2
LHCb, GEM, 61, 1, 2, False
LHCb, MUON, 61, 2, 2, False
Duplicate id: 102471, #duplicates=2
LHCb, GEM, 61, 1, 3, True
LHCb, MUON, 61, 2, 3, True
Duplicate id: 102459, #duplicates=2
LHCb, GEM, 61, 1, 3, False
LHCb, MUON, 61, 2, 3, False
Duplicate id: 102461, #duplicates=2
LHCb, GEM, 61, 1, 4, True
LHCb, MUON, 61, 2, 4, True
Duplicate id: 102485, #duplicates=2
LHCb, GEM, 61, 1, 4, False
LHCb, MUON, 61, 2, 4, False
Duplicate id: 102470, #duplicates=2
LHCb, GEM, 61, 1, 5, True
LHCb, MUON, 61, 2, 5, True
Duplicate id: 102489, #duplicates=2
LHCb, GEM, 61, 1, 5, False
LHCb, MUON, 61, 2, 5, False
Duplicate id: 102482, #duplicates=2
LHCb, GEM, 61, 1, 6, True
LHCb, MUON, 61, 2, 6, True
Duplicate id: 102472, #duplicates=2
LHCb, GEM, 61, 1, 6, False
LHCb, MUON, 61, 2, 6, False
Duplicate id: 102502, #duplicates=2
LHCb, GEM, 62, 1, 1, True
LHCb, MUON, 62, 2, 1, True
Duplicate id: 102493, #duplicates=2
LHCb, GEM, 62, 1, 1, False
LHCb, MUON, 62, 2, 1, False
Duplicate id: 102500, #duplicates=2
LHCb, GEM, 62, 1, 2, True
LHCb, MUON, 62, 2, 2, True
Duplicate id: 102495, #duplicates=2
LHCb, GEM, 62, 1, 2, False
LHCb, MUON, 62, 2, 2, False
Duplicate id: 102501, #duplicates=2
LHCb, GEM, 62, 1, 3, True
LHCb, MUON, 62, 2, 3, True
Duplicate id: 102497, #duplicates=2
LHCb, GEM, 62, 1, 3, False
LHCb, MUON, 62, 2, 3, False
Duplicate id: 102504, #duplicates=2
LHCb, GEM, 62, 1, 4, True
LHCb, MUON, 62, 2, 4, True
Duplicate id: 102496, #duplicates=2
LHCb, GEM, 62, 1, 4, False
LHCb, MUON, 62, 2, 4, False
Duplicate id: 102503, #duplicates=2
LHCb, GEM, 62, 1, 5, True
LHCb, MUON, 62, 2, 5, True
Duplicate id: 102494, #duplicates=2
LHCb, GEM, 62, 1, 5, False
LHCb, MUON, 62, 2, 5, False
Duplicate id: 102458, #duplicates=2
LHCb, GEM, 62, 1, 6, True
LHCb, MUON, 62, 2, 6, True
Duplicate id: 102492, #duplicates=2
LHCb, GEM, 62, 1, 6, False
LHCb, MUON, 62, 2, 6, False
