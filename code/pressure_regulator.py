import pyads
import logging
import time
from utils import *

class PressureRegulator:
    
    def __init__(self, plc, delay=0.2):
        self.logger = logging.getLogger('Pressure_regulator')
        self.plc = plc
        self.delay = delay
        self.check_state()

    def check_state(self):
        if not self.plc.read_by_name(f'MAIN.bpr_status') == 0:
            raise Exception(f'Backpressure regulator in error state!')
    
    def write_setpoint(self, setpoint):
        '''Write a setpoint for the backpressure (in barg)'''
        self.plc.write_by_name(f'MAIN.bpr_setpoint_write', float_to_byte_arr(setpoint))
        self.logger.debug(f'Setpoint written: {setpoint}')
        time.sleep(self.delay)
        setpoint_read = self.read_setpoint()
        if abs(setpoint_read-setpoint) > 1e-2*setpoint:
            raise Exception(f'Write setpoint {setpoint} unsuccessful! {setpoint_read} != {setpoint}')

    def read_setpoint(self):
        '''Read the setpoint for the backpressure (in barg)'''
        return byte_arr_to_float(self.plc.read_by_name(f'MAIN.bpr_setpoint_read'))
    
    def read_measurement(self):
        '''Read the value of the backpressure (in barg)'''
        measurement = self.plc.read_by_name(f'MAIN.bpr_measure')
        return byte_arr_to_float(measurement)


if __name__ == '__main__':
    
    LOGGING_LEVEL = 'DEBUG'

    try:
        logging.basicConfig(level=LOGGING_LEVEL, format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

        plc = pyads.Connection('127.0.0.1.1.1', 851)
        plc.open()
        
        bpr = PressureRegulator(plc)

        bpr.write_setpoint(0.5)
        while True:
            print(bpr.read_measurement(), bpr.read_pressure())
            time.sleep(.5)

    finally:
        try:
            bpr.write_setpoint(0)
        except:
            pass
        plc.close()
