import pyads
import logging
import time
from utils import *
import re

class Mfcs:
    
    def __init__(self, plc, nb_mfcs=4):
        self.plc = plc
        self.nb_mfcs = nb_mfcs
        self.mfcs = [Mfc(plc, i) for i in range(1, nb_mfcs+1)]
    
    def reset_mfcs(self):
        for mfc in self.mfcs:
            mfc.write_setpoint(0)


class Mfc:
    
    def __init__(self, plc, num_mfc, delay=0.2):
        self.logger = logging.getLogger(f'MFC {num_mfc}')
        self.num_mfc = num_mfc
        self.plc = plc
        self.delay = delay
        self.check_state()
        self.fluid_strings = self.read_all_fluids()
    
    def check_state(self):
        # if self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_status') != 0:
        # TODO look into why status switches to 16
        if not self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_status') in [0, 16]:
            raise Exception(f'MFC in error state!')
        
    def write_fluid_number(self, num):
        self.plc.write_by_name(f'MAIN.mfc{self.num_mfc}_fluid_number_write', num)
        self.logger.debug(f'Fluid number written: {num}')
        time.sleep(self.delay)
        num_read = self.read_fluid_number()
        if num_read != num:
            raise Exception(f'Write fluid number {num} unsuccessful! {num_read} != {num}')

    def write_setpoint(self, setpoint):
        self.plc.write_by_name(f'MAIN.mfc{self.num_mfc}_setpoint_write', float_to_byte_arr(setpoint))
        self.logger.debug(f'Setpoint written: {setpoint}')
        time.sleep(self.delay)
        setpoint_read = self.read_setpoint()
        if abs(setpoint_read-setpoint) > 1e-2*setpoint:
            raise Exception(f'Write setpoint {setpoint} unsuccessful! {setpoint_read} != {setpoint}')
    
    def read_setpoint(self):
        return byte_arr_to_float(self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_setpoint_read'))
      
    def read_measurement(self):
        measurement = self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_measure')
        return byte_arr_to_float(measurement)
    
    def read_fluid_number(self):
        return self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_fluid_number_read')
    
    def read_fluid_name(self):
        arr = self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_fluid_name')
        return ''.join([chr(n) for n in arr]).strip()
    
    def read_capacity(self):
        c_low  = self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_capacity_0')
        c_high = self.plc.read_by_name(f'MAIN.mfc{self.num_mfc}_capacity_100')
        return byte_arr_to_float(c_low), byte_arr_to_float(c_high)
    
    def get_all_fluids(self):
        res = []
        fluid_num = 0
        while True:
            try:
                self.write_fluid_number(fluid_num)
                fluid_name = self.read_fluid_name().strip()
                capacity = self.read_capacity()
                res.append('{} ||| {:.1f}-{:.1f} l/h'.format(fluid_name, capacity[0], capacity[1]))
                fluid_num += 1
            except:
                break
        self.write_fluid_number(0)
        return res
    
    def read_all_fluids(self):
        # NOTE: can change/remove cached files for new MFCs
        try:
            with open(relative_path('data', f'fluids_mfc{self.num_mfc}.txt'), 'r') as f:
                fluids = f.read().splitlines()
        except:
            fluids = self.get_all_fluids()
            with open(relative_path('data', f'fluids_mfc{self.num_mfc}.txt'), 'w') as f:
                for l in fluids:
                    f.write(f'{l}\n')
        return fluids

    def get_fluid_str_capacity(self, fluid_number):
        '''Return the minimun and maximum flow setting according to the saved fluid strings.'''
        fluid_str = self.fluid_strings[fluid_number]
        m = re.search(r'\|\|\| (\d+\.?\d*)-(\d+\.?\d*)', fluid_str)
        if not m:
            raise Exception(f'Cannot extract capacity of MFC {self.num_mfc} with fluid string {fluid_str}!')
        return float(m.group(1)), float(m.group(2))

def float_to_byte_arr(f):
    return list(struct.pack('!f', f))

def byte_arr_to_float(arr):
    return struct.unpack('!f', bytes(arr))[0]



if __name__ == '__main__':
    
    LOGGING_LEVEL = 'DEBUG'

    try:
        logging.basicConfig(level=LOGGING_LEVEL, format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

        plc = pyads.Connection('127.0.0.1.1.1', 851)
        plc.open()
        
        
        mfc3 = Mfc(plc, 3)
        print(mfc3.get_all_fluids())
        
        mfc4 = Mfc(plc, 4)
        print(mfc4.get_all_fluids())
    
    
    finally:
        try:
            # mfc.write_setpoint(0)
            pass
        except:
            pass
        plc.close()
