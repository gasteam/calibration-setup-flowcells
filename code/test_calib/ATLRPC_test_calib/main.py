from elmb import ElmbFlowscan
from mfc import Mfc
import os, pyads, logging, time, csv
from datetime import datetime


logging.basicConfig(level='DEBUG', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

plc = pyads.Connection('127.0.0.1.1.1', 851)
plc.open()

mfc3 = Mfc(plc, 3)
mfc4 = Mfc(plc, 4)
elmb = ElmbFlowscan(plc)

mfc3.write_fluid_number(2) # R134A 0-120 L/h
mfc4.write_fluid_number(5) # CO2   0-32 L/h


# NOTE mfc3 can handle setpoints >=2.3
# NOTE mfc4 can handle setpoints >=0.7


def write_slopes_for_zero_flow(nb_slopes=1, nb_channels=16):
    mfc3.write_setpoint(0)
    mfc4.write_setpoint(0)
    
    # Read channels
    fields = ['time', 'percentage_freon', 'flow', 'error_freon', 'error_co2', 'flowcell', 'slope']
    rows = []
    flow = 0
    percentage_freon = -1
    for nb_slope in range(nb_slopes):
        logging.info(f'Starting channel readout cycle {nb_slope+1}/{nb_slopes}')
        for ch in range(nb_channels):
            if ch in [0, 7]:
                # Skip channel (bad cable)
                continue
            res = elmb.read_channel(ch)
            rows.append([datetime.now().strftime('%H:%M'), percentage_freon, flow, -1, -1, 2*ch,   res['slope1']])
            rows.append([datetime.now().strftime('%H:%M'), percentage_freon, flow, -1, -1, 2*ch+1, res['slope2']])
    
    filename = 'results.csv'
    if not os.path.exists(filename):
        with open(filename, 'w') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(fields)
    with open(filename, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(rows)
        
        



def write_slopes_for_flow(setpoint_mfc3, setpoint_mfc4, nb_slopes=1, nb_channels=16):
    mfc3.write_setpoint(setpoint_mfc3)
    mfc4.write_setpoint(setpoint_mfc4)

    # Wait a bit for a steady value
    for _ in range(3):
        time.sleep(10)
        mmt3 = mfc3.read_measurement()
        mmt4 = mfc4.read_measurement()
        error3 = 0
        error4 = 0
        if setpoint_mfc3 != 0:
            error3 = 100*(setpoint_mfc3 - mmt3)/setpoint_mfc3
            logging.info(f'MFC3 setpoint: {setpoint_mfc3} L/h, measurement: {mmt3:.2f}, error: {error3:.2f} %')
        if setpoint_mfc4 != 0:
            error4 = 100*(setpoint_mfc4 - mmt4)/setpoint_mfc4
            logging.info(f'MFC4 setpoint: {setpoint_mfc4} L/h, measurement: {mmt4:.2f}, error: {error4:.2f} %')
    
    
    # Read channels
    fields = ['time', 'percentage_freon', 'flow', 'error_freon', 'error_co2', 'flowcell', 'slope']
    rows = []
    flow = setpoint_mfc3 + setpoint_mfc4
    percentage_freon = round(100 * setpoint_mfc3 / flow)
    for nb_slope in range(nb_slopes):
        logging.info(f'Starting channel readout cycle {nb_slope+1}/{nb_slopes}')
        for ch in range(nb_channels):
            if ch in [0, 7]:
                # Skip channel (bad cable)
                continue
            res = elmb.read_channel(ch)
            rows.append([datetime.now().strftime('%H:%M'), percentage_freon, flow, error3, error4, 2*ch,   res['slope1']])
            rows.append([datetime.now().strftime('%H:%M'), percentage_freon, flow, error3, error4, 2*ch+1, res['slope2']])
    
    filename = 'results.csv'
    if not os.path.exists(filename):
        with open(filename, 'w') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(fields)
    with open(filename, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(rows)
            

try:
    
    ## Pure freon
    
    # Flush (already done)
    mfc3.write_setpoint(100)
    mfc4.write_setpoint(0)
    time.sleep(180)
    
    
    
    flows1 = [2.3, 4, 6, 9, 12, 16, 20, 25, 30, 35, 40]
    for flow_freon in flows1[::-1]:
        write_slopes_for_flow(
            setpoint_mfc3=flow_freon,
            setpoint_mfc4=0,
            nb_slopes=1
        )
        
    
    write_slopes_for_zero_flow() # Do once
    
    
    ## Mix
    
    # Flush
    mfc3.write_setpoint(100*0.7)
    mfc4.write_setpoint(100*0.3)
    time.sleep(300)
    
    flows2 = [3.3, 5, 7.5, 10, 13, 16, 20, 25, 30, 35, 40]
    for flow in flows2[::-1]:
        write_slopes_for_flow(
            setpoint_mfc3=flow*0.7,
            setpoint_mfc4=flow*0.3,
            nb_slopes=1
        )

finally:
    mfc3.write_setpoint(0)
    mfc4.write_setpoint(0)
