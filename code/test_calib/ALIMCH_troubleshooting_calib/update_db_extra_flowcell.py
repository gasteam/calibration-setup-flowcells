import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import sys
path_root = Path(__file__).parents[3]
sys.path.append(str(path_root))
from dotenv import load_dotenv
load_dotenv('.env')
from general_operations import *
from datetime import datetime

## DB operations
# - Add barcodes to DB
# - Add calibrations
# - Update flowcell states: both old and new!


FLOWCELL = 113310

def get_calib_params(filename):
    df = pd.read_csv(filename)
    barcodes = list(set(df.flowcell))
    calib_params = dict()
    for barcode in barcodes:
        if barcode != FLOWCELL:
            # Skip
            continue
        df_flowcell = df[df.flowcell == barcode]
        df_flowcell = df_flowcell.sort_values(by = 'flow')
        params = np.polynomial.polynomial.polyfit(df_flowcell['slope'], df_flowcell['flow'], 4)
        calib_params[barcode] = [params, (max(df_flowcell['slope']), min(df_flowcell['slope']))]
    return calib_params

if __name__ == '__main__':
    
    params62 = get_calib_params('../test_calib/ALIMCH_troubleshooting_calib/results_60.csv')
    params_flowcell = params62[FLOWCELL]
    
    # # Add barcodes
    # add_flowcell(FLOWCELL, 3)
    
    # # Add calibrations
    # barcode, d = FLOWCELL, params_flowcell
    # params, slopes = d
    # add_calibration(
    #     flowcell_id = barcode,
    #     timestamp = datetime.today(),
    #     calibration_gas = '85Ar15CO2',
    #     low_flow_min = 0,
    #     low_flow_max = 60,
    #     slope_low_flow_min = slopes[0],
    #     slope_low_flow_max = slopes[1],
    #     low_x0 = params[0],
    #     low_x1 = params[1],
    #     low_x2 = params[2],
    #     low_x3 = params[3],
    #     low_x4 = params[4],
    #     high_flow_enabled = False
    # )
        
    

    # # Add new flowcell locations 
    # i = 12
    # barcode = FLOWCELL
    # if i < 16:
    #     is_input = True
    #     ch = i+1
    # else:
    #     is_input = False
    #     ch = i-15
    # loc_id = get_location_id(
    #     experiment = 'ALICE',
    #     system = 'MCH',
    #     rack = 61,
    #     position = 1
    # )
    # calib_id = get_most_recent_calibration_id(barcode)
    # add_state_flowcell(
    #     flowcell_id = barcode,
    #     timestamp = datetime.now(),
    #     location_id = loc_id,
    #     active_calibration_id = calib_id,
    #     channel = ch,
    #     is_input = is_input
    # )
    
    
    # # Move old flowcells
    # b = 106162
    # calib_id = get_most_recent_calibration_id(b)
    # add_state_flowcell(
    #     flowcell_id = b,
    #     timestamp = datetime.now(),
    #     location_id = 485,
    #     active_calibration_id = calib_id
    # )
