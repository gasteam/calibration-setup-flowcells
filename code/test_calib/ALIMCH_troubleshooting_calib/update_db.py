import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import sys
path_root = Path(__file__).parents[3]
sys.path.append(str(path_root))
from dotenv import load_dotenv
load_dotenv('.env')
from general_operations import *
from datetime import datetime

## DB operations
# - Add barcodes to DB
# - Add calibrations
# - Update flowcell states: both old and new!


def get_calib_params(filename):
    df = pd.read_csv(filename)
    barcodes = list(set(df.flowcell))
    calib_params = dict()
    for barcode in barcodes:
        if barcode in [113310, 113208]:
            # Skip
            continue
        df_flowcell = df[df.flowcell == barcode]
        df_flowcell = df_flowcell.sort_values(by = 'flow')
        params = np.polynomial.polynomial.polyfit(df_flowcell['slope'], df_flowcell['flow'], 4)
        calib_params[barcode] = [params, (max(df_flowcell['slope']), min(df_flowcell['slope']))]
    return calib_params

if __name__ == '__main__':
    params61 = get_calib_params('../test_calib/ALIMCH_troubleshooting_calib/results_20.csv')
    params62 = get_calib_params('../test_calib/ALIMCH_troubleshooting_calib/results_60.csv')
    
    # # Add barcodes
    # for barcode in params61:
    #     add_flowcell(barcode, 3)
    # for barcode in params62:
    #     add_flowcell(barcode, 3)
    
    # # Add calibrations
    # for barcode, d in params61.items():
    #     params, slopes = d
    #     add_calibration(
    #         flowcell_id = barcode,
    #         timestamp = datetime.today(),
    #         calibration_gas = '85Ar15CO2',
    #         low_flow_min = 0,
    #         low_flow_max = 20,
    #         slope_low_flow_min = slopes[0],
    #         slope_low_flow_max = slopes[1],
    #         low_x0 = params[0],
    #         low_x1 = params[1],
    #         low_x2 = params[2],
    #         low_x3 = params[3],
    #         low_x4 = params[4],
    #         high_flow_enabled = False
    #     )
        
    # for barcode, d in params62.items():
    #     params, slopes = d
    #     add_calibration(
    #         flowcell_id = barcode,
    #         timestamp = datetime.today(),
    #         calibration_gas = '85Ar15CO2',
    #         low_flow_min = 0,
    #         low_flow_max = 60,
    #         slope_low_flow_min = slopes[0],
    #         slope_low_flow_max = slopes[1],
    #         low_x0 = params[0],
    #         low_x1 = params[1],
    #         low_x2 = params[2],
    #         low_x3 = params[3],
    #         low_x4 = params[4],
    #         high_flow_enabled = False
    #     )

    # Add new flowcell locations
    # for i, barcode in enumerate(list(sorted(params61.keys()))):
    #     if i < 16:
    #         is_input = True
    #         ch = i+1
    #     else:
    #         is_input = False
    #         ch = i-15
    #     loc_id = get_location_id(
    #         experiment = 'ALICE',
    #         system = 'MCH',
    #         rack = 61,
    #         position = 1
    #     )
    #     calib_id = get_most_recent_calibration_id(barcode)
    #     add_state_flowcell(
    #         flowcell_id = barcode,
    #         timestamp = datetime.now(),
    #         location_id = loc_id,
    #         active_calibration_id = calib_id,
    #         channel = ch,
    #         is_input = is_input
    #     )
    # for i, barcode in enumerate(list(sorted(params62.keys()))):
    #     if i < 12:
    #         is_input = True
    #         ch = i+1
    #     else:
    #         is_input = False
    #         ch = i-11
    #     loc_id = get_location_id(
    #         experiment = 'ALICE',
    #         system = 'MCH',
    #         rack = 62,
    #         position = 1
    #     )
    #     calib_id = get_most_recent_calibration_id(barcode)
    #     add_state_flowcell(
    #         flowcell_id = barcode,
    #         timestamp = datetime.now(),
    #         location_id = loc_id,
    #         active_calibration_id = calib_id,
    #         channel = ch,
    #         is_input = is_input
    #     )
    
    # Move old flowcells
    # barcodes_old = [
    #     106109,
    #     106115,
    #     106116,
    #     106117,
    #     106118,
    #     106119,
    #     106120,
    #     106121,
    #     106122,
    #     106123,
    #     106124,
    #     106125,
    #     106126,
    #     106127,
    #     106128,
    #     106129,
    #     106130,
    #     106131,
    #     106132,
    #     106137,
    #     106144,
    #     106147,
    #     106148,
    #     106149,
    #     206024,
    #     206022,
    #     206021,
    #     206020,
    #     206019,
    #     206018,
    #     206017,
    #     206016,
    #     206001,
    #     206002,
    #     206003,
    #     206004,
    #     206005,
    #     206006,
    #     206007,
    #     206008,
    #     206009,
    #     206010,
    #     206011,
    #     206012,
    #     206013,
    #     206014,
    #     206015,
    #     106112,
    #     106110,
    #     106133,
    #     106135,
    #     106142,
    #     106143,
    #     106146,
    #     106111,
    #     206028
    # ]
    
    # for b in barcodes_old:
    #     calib_id = get_most_recent_calibration_id(b)
    #     add_state_flowcell(
    #         flowcell_id = b,
    #         timestamp = datetime.now(),
    #         location_id = 485,
    #         active_calibration_id = calib_id
    #     )
