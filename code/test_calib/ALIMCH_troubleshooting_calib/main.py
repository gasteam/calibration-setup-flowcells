
from pathlib import Path
import sys
path_root = Path(__file__).parents[2]
sys.path.append(str(path_root))
from elmb import ElmbFlowscan
from mfc import Mfc
import os, pyads, logging, time, csv
from datetime import datetime

barcodes = [
    106150,
    106151,
    106152,
    106153,
    106154,
    106155,
    106156,
    106157,
    106158,
    106159,
    106160,
    106161,
    106162,
    106163,
    106164,
    106165,
    106182,
    106183,
    106184,
    106185,
    106186,
    106187,
    106188,
    106189,
    106190,
    106191,
    106192,
    106193,
    106194,
    106195,
    106196,
    106197
]

barcodes2 = [
    113208,
    113247,
    113263,
    113264,
    113265,
    113272,
    113277,
    113289,
    113292,
    113293,
    113295,
    113296,
    113298,
    113299,
    113300,
    113306,
    113307,
    113310,
    113312,
    113313,
    113314,
    113315,
    113316,
    113320,
    113103,
    113168
]

logging.basicConfig(level='INFO', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

plc = pyads.Connection('127.0.0.1.1.1', 851)
plc.open()

mfc1 = Mfc(plc, 1)
mfc4 = Mfc(plc, 4)
elmb = ElmbFlowscan(plc, id=1)


mfc1.write_fluid_number(2) # 0-20, min 0.3
mfc4.write_fluid_number(0) # 0-60


def calculate_temperature(voltage, current):
    '''Calculate the temperature in °C for a Pt100 sensor.'''
    return (voltage/current - 100)/0.385

def cable_check(nb_channels=16):
    adc_range = elmb.read_can(0x2100, 3)
    elmb.set_adc_mode(current='low', resistance=100)
    voltage_inflow1, voltage_outflow1 = elmb.read_analogue_input(0), elmb.read_analogue_input(1)
    current_inflow, current_outflow = voltage_inflow1/100, voltage_outflow1/100
    for current_channel in range(nb_channels):
        elmb.set_adc_mode(current='low', resistance=current_channel)
        voltages = elmb.read_flowchannel_voltages(current_channel, adc_range)
        temp_in = calculate_temperature(voltages[0], current_inflow)
        temp_out = calculate_temperature(voltages[1], current_outflow)
        print(f'Channel {current_channel}: {temp_in:.2f}   {temp_out:.2f}')
    elmb.set_adc_mode('off')     


def write_slopes_for_flow(mfc, setpoint, nb_channels=16):
    
    mfc.write_setpoint(setpoint)

    # Wait a bit for a steady value
    for _ in range(3):
        time.sleep(10)
        flow = mfc.read_measurement()
        logging.info(f'Flow = {flow}')
    
    # Read channels
    fields = ['time', 'setpoint', 'flow', 'flowcell', 'slope']
    rows = []
    
    for nb_slope in range(2):
        logging.info(f'Starting channel readout cycle {nb_slope+1}/2')
        for ch in range(nb_channels):
            res = elmb.read_channel(ch)
            rows.append([datetime.now().strftime('%H:%M'), setpoint, mfc.read_measurement(), barcodes[2*ch], res['slope1']])
            rows.append([datetime.now().strftime('%H:%M'), setpoint, mfc.read_measurement(), barcodes[2*ch+1], res['slope2']])
    
    filename = 'results.csv'
    if not os.path.exists(filename):
        with open(filename, 'w') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(fields)
    with open(filename, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(rows)

try:
    
    flows = [0, 1.9, 3.7, 5.6, 8, 10.8, 14.6, 20]

    for setpoint in flows[::-1]:
        write_slopes_for_flow(
            mfc1,
            setpoint
        )
    
    # flows = [0, 4.8, 9.5, 14.8, 21, 28, 36, 44, 52, 60]
    # for setpoint in flows[::-1]:
    #     write_slopes_for_flow(
    #         mfc4,
    #         setpoint,
    #         nb_channels=13
    #     )

finally:
    mfc1.write_setpoint(0)
    mfc4.write_setpoint(0)


