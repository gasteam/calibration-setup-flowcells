import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from polyfit import PolynomRegressor, Constraints
from datetime import datetime

from dotenv import load_dotenv
load_dotenv('.env')
from pathlib import Path
import sys
path_root = Path(__file__).parents[3]
sys.path.append(str(path_root))
from general_operations import *



df63 = pd.read_excel('../test_calib/ATLTGC_MD_hotfix/rack_63.xlsx', usecols=[2, 8, 11], names=['flowcell', 'slope', 'flow'])
df67 = pd.read_excel('../test_calib/ATLTGC_MD_hotfix/rack_67.xlsx', usecols=[2, 8, 11], names=['flowcell', 'slope', 'flow'])


flowcells_63_1 = [
    105116,
    105179,
    105180,
    105189,
    105190,
    105217,
    105218,
    105219,
    105220,
    115074,
    115075,
    115076,
    115077,
    115078,
    115082,
    115083,
    115084,
    115085,
    115086,
    115087,
]

flowcells_63_2 = [
    105152,
    105207,
    105211,
    105229,
    105238,
    105253,
    105263,
    105264,
    105272,
    105281,
    105283,
    105296,
    105509,
    105510,
    105513,
    105514,
    105547,
    105548,
    105565,
    105566,
]

flowcells_67_1 = [
    115002,
    115003,
    115004,
    115005,
    115006,
    115007,
    115008,
    115066,
    115067,
    115068,
    115069,
    115070,
    115071,
    115072,
    115080,
    115081,
    115205,
    115222,
    115223,
    115224,
]

flowcells_67_2 = [
    105212,
    105268,
    115203,
    115204,
    115206,
    115207,
    115208,
    115209,
    115210,
    115211,
    115212,
    115213,
    115214,
    115215,
    115216,
    115217,
    115218,
    115219,
    115220,
    115221,
]

flowcells_63 = sorted(flowcells_63_1 + flowcells_63_2)
flowcells_67 = sorted(flowcells_67_1 + flowcells_67_2)

def get_flowcell_ids(df):
    return sorted(list(set(df['flowcell'])))

# Remove flowcells that are not installed
df63_to_remove = set(get_flowcell_ids(df63)) - set(flowcells_63)
df67_to_remove = set(get_flowcell_ids(df67)) - set(flowcells_67)
df63 = df63[~df63['flowcell'].isin(df63_to_remove)]
df67 = df67[~df67['flowcell'].isin(df67_to_remove)]

def get_artifical_point(slopes, flows, flow_desired):
    flow1, flow2 = sorted(list(set(flows)))[-2:]
    assert flow1 < flow_desired
    assert flow2 < flow_desired
    slopes1 = []
    slopes2 = []
    for flow, slope in zip(flows, slopes):
        if flow == flow1:
            slopes1.append(slope)
        if flow == flow2:
            slopes2.append(slope)
    slope1 = np.mean(slopes1)
    slope2 = np.mean(slopes2)
    slope_desired_flow = slope1 + (slope2-slope1)/(flow2-flow1)*(flow_desired-flow1)
    return slope_desired_flow, flow_desired

def add_point_for_flow(slopes, flows, flow_to_add):
    slope_new, flow_new = get_artifical_point(slopes, flows, flow_to_add)
    slopes.append(slope_new)
    flows.append(flow_new)




DEG = 4
df = pd.concat([df63, df67])

def get_coeffs_flowcell(flowcell):
    df_flowcell = df[(df['flowcell']==flowcell)]
    
    slopes = list(df_flowcell['slope'])
    flows = list(df_flowcell['flow'])
    add_point_for_flow(slopes, flows, 60)
    add_point_for_flow(slopes, flows, 95)
    add_point_for_flow(slopes, flows, 100)
    
    polyestimator = PolynomRegressor(deg=DEG)
    monotone_constraint = Constraints(monotonicity='dec')
    X = np.array(slopes).reshape((-1,1))
    y = np.array(flows)
    polyestimator.fit(X, y, loss = 'l2', constraints={0: monotone_constraint})
    
    return polyestimator.coeffs_


# Rack 63 pos 1 input output, then rack 63 pos 2 input output etc.
flowcells_ordered = [
    115074, 115076, 115078, 115083, 115085, 115087, 105218, 105220, 105179, 105189,
    115075, 115077, 115082, 115084, 115086, 105217, 105219, 105116, 105180, 105190,
    105152, 105211, 105238, 105263, 105272, 105283, 105509, 105513, 105547, 105565,
    105207, 105229, 105253, 105264, 105281, 105296, 105510, 105514, 105548, 105566,
    115205, 115002, 115003, 115004, 115005, 115006, 115007, 115008, 115222, 115080,
    115223, 115066, 115067, 115068, 115069, 115070, 115071, 115072, 115224, 115081,
    115203, 115206, 115208, 115210, 115212, 115214, 115216, 115218, 105212, 115220,
    115204, 115207, 115209, 115211, 115213, 115215, 115217, 115219, 105268, 115221,
]

def add_data(index_offset, rack, pos, is_input, timestamp_calib):
    for i in range(10):
        flowcell_id = flowcells_ordered[index_offset + i]
        coeffs = get_coeffs_flowcell(flowcell_id)
        df_flowcell = df[(df['flowcell']==flowcell_id)]
        
        calibration_id = add_calibration(
            flowcell_id = flowcell_id,
            timestamp = timestamp_calib,
            calibration_gas = 'CO2',
            low_flow_min = 0,
            low_flow_max = 30,
            slope_low_flow_min = max(df_flowcell['slope']),
            slope_low_flow_max = min(df_flowcell['slope']),
            low_x0 = coeffs[0],
            low_x1 = coeffs[1],
            low_x2 = coeffs[2],
            low_x3 = coeffs[3],
            low_x4 = coeffs[4],
            high_flow_enabled = False
        )
        
        loc_id = get_location_id(
                experiment = 'ATLAS',
                system = 'TGC',
                rack = rack,
                position = pos
            )
    
        add_state_flowcell(
                flowcell_id = flowcell_id,
                timestamp = datetime.now(),
                location_id = loc_id,
                active_calibration_id = calibration_id,
                channel = 1+i,
                is_input = is_input
            )

# add_data(0,  63, 1, True, datetime(2021, 11, 18))
# add_data(10, 63, 1, False, datetime(2021, 11, 18))
# add_data(20, 63, 2, True, datetime(2021, 11, 18))
# add_data(30, 63, 2, False, datetime(2021, 11, 18))
# add_data(40, 67, 1, True, datetime(2021, 12, 2))
# add_data(50, 67, 1, False, datetime(2021, 12, 2))
# add_data(60, 67, 2, True, datetime(2021, 12, 2))
# add_data(70, 67, 2, False, datetime(2021, 12, 2))
