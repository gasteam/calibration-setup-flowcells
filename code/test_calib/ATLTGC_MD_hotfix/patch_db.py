from datetime import datetime

from dotenv import load_dotenv
load_dotenv('.env')
from pathlib import Path
import sys
path_root = Path(__file__).parents[3]
sys.path.append(str(path_root))
from general_operations import *

# Rack 69, output of CH2, 6
# Set to 105583 and 105553

# Rack 70 (EM LEVEL 4 SIDE C), output of CH8: set to 105553
# Rack 74 (EM LEVEL 4 SIDE A), output of CH12: set to 105583

loc_id = get_location_id(
    experiment = 'ATLAS',
    system = 'TGC',
    rack = 69,
    position = 1
)

def add_new_state(barcode, channel):
    calib_id = get_most_recent_calibration_id(barcode)
    add_state_flowcell(
        flowcell_id = barcode,
        timestamp = datetime.now(),
        location_id = loc_id,
        active_calibration_id = calib_id,
        channel = channel,
        is_input = False
    )

add_new_state(105583, 2)
add_new_state(105553, 6)

