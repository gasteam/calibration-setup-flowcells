import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from polyfit import PolynomRegressor, Constraints
from datetime import datetime


from dotenv import load_dotenv
load_dotenv('.env')
from pathlib import Path
import sys
path_root = Path(__file__).parents[3]
sys.path.append(str(path_root))
from general_operations import *




df63 = pd.read_excel('../test_calib/ATLTGC_MD_hotfix/rack_63.xlsx', usecols=[2, 8, 11], names=['flowcell', 'slope', 'flow'])
df67 = pd.read_excel('../test_calib/ATLTGC_MD_hotfix/rack_67.xlsx', usecols=[2, 8, 11], names=['flowcell', 'slope', 'flow'])


flowcells_63_1 = [
    105116,
    105179,
    105180,
    105189,
    105190,
    105217,
    105218,
    105219,
    105220,
    115074,
    115075,
    115076,
    115077,
    115078,
    115082,
    115083,
    115084,
    115085,
    115086,
    115087,
]

flowcells_63_2 = [
    105152,
    105207,
    105211,
    105229,
    105238,
    105253,
    105263,
    105264,
    105272,
    105281,
    105283,
    105296,
    105509,
    105510,
    105513,
    105514,
    105547,
    105548,
    105565,
    105566,
]

flowcells_67_1 = [
    115002,
    115003,
    115004,
    115005,
    115006,
    115007,
    115008,
    115066,
    115067,
    115068,
    115069,
    115070,
    115071,
    115072,
    115080,
    115081,
    115205,
    115222,
    115223,
    115224,
]

flowcells_67_2 = [
    105212,
    105268,
    115203,
    115204,
    115206,
    115207,
    115208,
    115209,
    115210,
    115211,
    115212,
    115213,
    115214,
    115215,
    115216,
    115217,
    115218,
    115219,
    115220,
    115221,
]

flowcells_63 = sorted(flowcells_63_1 + flowcells_63_2)
flowcells_67 = sorted(flowcells_67_1 + flowcells_67_2)

def get_flowcell_ids(df):
    return sorted(list(set(df['flowcell'])))

# Remove flowcells that are not installed
df63_to_remove = set(get_flowcell_ids(df63)) - set(flowcells_63)
df67_to_remove = set(get_flowcell_ids(df67)) - set(flowcells_67)
df63 = df63[~df63['flowcell'].isin(df63_to_remove)]
df67 = df67[~df67['flowcell'].isin(df67_to_remove)]

def get_artifical_point(slopes, flows, flow_desired):
    flow1, flow2 = sorted(list(set(flows)))[-2:]
    assert flow1 < flow_desired
    assert flow2 < flow_desired
    slopes1 = []
    slopes2 = []
    for flow, slope in zip(flows, slopes):
        if flow == flow1:
            slopes1.append(slope)
        if flow == flow2:
            slopes2.append(slope)
    slope1 = np.mean(slopes1)
    slope2 = np.mean(slopes2)
    slope_desired_flow = slope1 + (slope2-slope1)/(flow2-flow1)*(flow_desired-flow1)
    return slope_desired_flow, flow_desired

def add_point_for_flow(slopes, flows, flow_to_add):
    slope_new, flow_new = get_artifical_point(slopes, flows, flow_to_add)
    slopes.append(slope_new)
    flows.append(flow_new)




DEG = 4
df = pd.concat([df63, df67])

# def get_coeffs_flowcell(flowcell):
def get_slope_min(flowcell):
    df_flowcell = df[(df['flowcell']==flowcell)]
    
    slopes = list(df_flowcell['slope'])
    flows = list(df_flowcell['flow'])
    add_point_for_flow(slopes, flows, 60)
    add_point_for_flow(slopes, flows, 95)
    add_point_for_flow(slopes, flows, 100)
    
    polyestimator = PolynomRegressor(deg=DEG)
    monotone_constraint = Constraints(monotonicity='dec')
    X = np.array(slopes).reshape((-1,1))
    y = np.array(flows)
    polyestimator.fit(X, y, loss = 'l2', constraints={0: monotone_constraint})
    
    return min(slopes)
    # return polyestimator.coeffs_






# Rack 63 pos 1 input output, then rack 63 pos 2 input output etc.
flowcells_ordered = [
    115074, 115076, 115078, 115083, 115085, 115087, 105218, 105220, 105179, 105189,
    115075, 115077, 115082, 115084, 115086, 105217, 105219, 105116, 105180, 105190,
    105152, 105211, 105238, 105263, 105272, 105283, 105509, 105513, 105547, 105565,
    105207, 105229, 105253, 105264, 105281, 105296, 105510, 105514, 105548, 105566,
    115205, 115002, 115003, 115004, 115005, 115006, 115007, 115008, 115222, 115080,
    115223, 115066, 115067, 115068, 115069, 115070, 115071, 115072, 115224, 115081,
    115203, 115206, 115208, 115210, 115212, 115214, 115216, 115218, 105212, 115220,
    115204, 115207, 115209, 115211, 115213, 115215, 115217, 115219, 105268, 115221,
]


def get_calibrations(flowcell_id: int):
    command = dedent(f'''\
        select timestamp,
            low_flow_min, low_flow_max,
            slope_low_flow_min, slope_low_flow_max,
            low_x0, low_x1, low_x2, low_x3, low_x4
        from calibrations
        where flowcell_id = {flowcell_id}
    ''')
    return execute_command_db_return_df(command)

params_new = dict()
params_old = dict()

def get_all_params(row):
    return row['slope_low_flow_max'], row['slope_low_flow_min'], [row['low_x0'], row['low_x1'], row['low_x2'], row['low_x3'], row['low_x4']]

for i, flowcell in enumerate(flowcells_ordered):
    df2 = get_calibrations(flowcell)
    
    # Round timestamps are new calibrations
    if i < 40:
        for _, row in df2.iterrows():
            if row['timestamp'] == pd.Timestamp(2021, 11, 18):
                params_new[flowcell] = get_all_params(row)
            else:
                params_old[flowcell] = get_all_params(row)

    else:
        for _, row in df2.iterrows():
            if row['timestamp'] == pd.Timestamp(2021, 12, 2):
                params_new[flowcell] = get_all_params(row)
            else:
                params_old[flowcell] = get_all_params(row)



params_reference = {
    115147: ([3731, 5075, 6076], [1.4255E+03, -6.7696E-01, 1.0926E-04, -5.9992E-09, 0.0000E+00], [6.4605E+02, -1.8041E-01, 3.2472E-06, 1.5768E-09, 0.0000E+00]),
    115148: ([3533, 4870, 5854], [1.8011E+03, -9.0188E-01, 1.5249E-04, -8.7094E-09, 0.0000E+00], [2.7383E+02, 2.0002E-02, -3.2160E-05, 3.5603E-09, 0.0000E+00]),
    115149: ([3838, 5332, 6350], [1.4202E+03, -6.4123E-01, 9.8440E-05, -5.1463E-09, 0.0000E+00], [9.9589E+02, -4.3708E-01, 6.6987E-05, -3.6282E-09, 0.0000E+00]),
}

plot_data_reference = dict()
for k, v in params_reference.items():
    slope_limits, params_low, params_high = v
    xx_low = np.linspace(slope_limits[1], slope_limits[2], 50)
    xx_high = np.linspace(slope_limits[0], slope_limits[1], 50)
    yy_low = [np.dot(params_low, [1, x, x**2, x**3, x**4]) for x in xx_low]
    yy_high = [np.dot(params_high, [1, x, x**2, x**3, x**4]) for x in xx_high]
    plot_data_reference[k] = (list(xx_high)+list(xx_low), yy_high+yy_low)

# for k, v in plot_data_reference.items():
#     xx, yy = v
#     plt.plot(xx, yy, label=k)
# plt.grid()
# plt.legend()
# plt.show()

xx_reference, yy_reference = plot_data_reference[115148]


for i, flowcell in enumerate(flowcells_ordered):
    
    slope_low_flow_max, slope_low_flow_min, my_params_new = params_new[flowcell]
    _, _, my_params_old = params_old[flowcell]
    
    # xx = np.linspace(slope_low_flow_max, slope_low_flow_min, 100)
    xx = np.linspace(get_slope_min(flowcell), slope_low_flow_min, 100)
    
    yy_new = [np.dot(my_params_new, [1, x, x**2, x**3, x**4]) for x in xx]
    yy_old = [np.dot(my_params_old, [1, x, x**2, x**3, x**4]) for x in xx]
    
    xx_reference_plot = [x - max(xx_reference) + max(xx) for x in xx_reference]
    
    rack = 63 if i < 40 else 67
    position = 1 if (i%40 < 20) else 2
    is_input = ((i%20) < 10)
    channel = (i%10) + 1
    
    input_str = "input" if is_input else "output"
    title = f'Rack {rack} pos. {position} {input_str} CH{channel} (flowcell {flowcell})'
    
    fig = plt.figure()
    plt.title(title)
    plt.plot(xx, yy_new, label='New')
    plt.plot(xx, yy_old, label='Old')
    plt.plot(xx_reference_plot, yy_reference, 'k--', label='Reference')
    plt.xlabel('Slope (m°C/s)')
    plt.ylabel('Flow (normal l/h)')
    plt.legend()
    plt.grid()
    
    plt.savefig(f'temp/{title}.jpg')
    
    # plt.show()
    # break

