
from pathlib import Path
import sys
path_root = Path(__file__).parents[2]
sys.path.append(str(path_root))
from elmb import ElmbFlowscan
from mfc import Mfc
import os, pyads, logging, time, csv
from datetime import datetime

## Flowcells of different types

node_id = 1

logging.basicConfig(level='DEBUG', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

plc = pyads.Connection('127.0.0.1.1.1', 851)
plc.open()

mfc4 = Mfc(plc, 4)

elmb = ElmbFlowscan(plc, id=node_id)

mfc4.write_fluid_number(5) # 0-32


def calculate_temperature(voltage, current):
    '''Calculate the temperature in °C for a Pt100 sensor.'''
    return (voltage/current - 100)/0.385

def cable_check(nb_channels=8):
    adc_range = elmb.read_can(0x2100, 3)
    elmb.set_adc_mode(current='low', resistance=100)
    voltage_inflow1, voltage_outflow1 = elmb.read_analogue_input(0), elmb.read_analogue_input(1)
    current_inflow, current_outflow = voltage_inflow1/100, voltage_outflow1/100
    for current_channel in range(nb_channels):
        elmb.set_adc_mode(current='low', resistance=current_channel)
        voltages = elmb.read_flowchannel_voltages(current_channel, adc_range)
        temp_in = calculate_temperature(voltages[0], current_inflow)
        temp_out = calculate_temperature(voltages[1], current_outflow)
        print(f'Channel {current_channel}: {temp_in:.2f}   {temp_out:.2f}')
    elmb.set_adc_mode('off')     


def write_slopes_for_flow(mfc, setpoint, nb_channels=8, nb_repeat=2):
    
    mfc.write_setpoint(setpoint)

    # Wait a bit for a steady value
    for _ in range(2):
        time.sleep(10)
        flow = mfc.read_measurement()
        logging.info(f'Flow = {flow}')
    
    # Read channels
    fields = ['time', 'time_taken', 'ELMB', 'setpoint', 'flow', 'flowcell', 'slope', 'all_elmb_data']
    rows = []
    
    for nb_slope in range(nb_repeat):
        logging.info(f'Starting channel readout cycle {nb_slope+1}/{nb_repeat}')
        for ch in range(nb_channels):
            if ch == 5:
                # skip
                continue
            try:
                res = elmb.read_channel(ch)
            except Exception as e:
                print(f'Exception catched for channel {ch}')
                print(str(e))
                continue
            rows.append([datetime.now().strftime('%H:%M'), res['time_measured'], node_id, setpoint, mfc.read_measurement(), 2*ch+1, res['slope1'], str(res)])
            rows.append([datetime.now().strftime('%H:%M'), res['time_measured'], node_id, setpoint, mfc.read_measurement(), 2*ch+2, res['slope2'], str(res)])
    
    filename = f'results_final_ELMB{node_id}_2.csv'
    if not os.path.exists(filename):
        with open(filename, 'w') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(fields)
    with open(filename, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(rows)

try:
    # cable_check()
    # sys.exit()
    
    
    flows = [0, 5, 10, 20, 30]
    # flows = [0, 30]

    for setpoint in flows[::-1]:
        write_slopes_for_flow(
            mfc4,
            setpoint,
            nb_channels=2,
            nb_repeat=2
        )

finally:
    mfc4.write_setpoint(0)


