from pathlib import Path
import sys
path_root = Path(__file__).parents[2]
sys.path.append(str(path_root))
from elmb import ElmbFlowscan
import pyads


node_id = int(sys.argv[1])

plc = pyads.Connection('127.0.0.1.1.1', 851)
plc.open()
elmb = ElmbFlowscan(plc, id=node_id)

res = elmb.read_can(0x2100, 19)
print(res)
elmb.write_can(75, 0x2100, 19)
res = elmb.read_can(0x2100, 19)
print(res)

# res = elmb.read_channel(0)
# with open('test_results.txt', 'a') as f:
#     f.write(f'1 {res}\n')
