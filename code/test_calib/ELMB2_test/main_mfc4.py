import sys
sys.path.append('../..')
from elmb import ElmbFlowscan
from mfc import Mfc
import os, pyads, logging, time, csv
from datetime import datetime

logging.basicConfig(level='DEBUG', format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

plc = pyads.Connection('127.0.0.1.1.1', 851)
plc.open()

mfc4 = Mfc(plc, 4)
elmb = ElmbFlowscan(plc, id=1)

mfc4.write_fluid_number(5) # CO2   0-32 L/h

# NOTE mfc3 can handle setpoints >=2.3


def write_slopes_for_flow(setpoint_mfc4, nb_slopes=1, nb_channels=16):
    mfc4.write_setpoint(setpoint_mfc4)

    # Wait a bit for a steady value
    for _ in range(2):
        time.sleep(10)
        mmt4 = mfc4.read_measurement()
        error4 = 0
        if setpoint_mfc4 != 0:
            error4 = 100*(setpoint_mfc4 - mmt4)/setpoint_mfc4
            logging.info(f'MFC4 setpoint: {setpoint_mfc4} L/h, measurement: {mmt4:.2f}, error: {error4:.2f} %')
    
    
    # Read channels
    fields = ['time', 'ELMB', 'flow', 'error_flow', 'flowcell', 'slope']
    rows = []
    for nb_slope in range(nb_slopes):
        logging.info(f'Starting channel readout cycle {nb_slope+1}/{nb_slopes}')
        for ch in range(nb_channels):
            if ch not in [3, 4]:
                # Skip channels
                continue
            res = elmb.read_channel(ch)
            rows.append([datetime.now().strftime('%H:%M'), '?', setpoint_mfc4, error4, 2*ch,   res['slope1']])
            rows.append([datetime.now().strftime('%H:%M'), '?', setpoint_mfc4, error4, 2*ch+1, res['slope2']])
    
    filename = 'results.csv'
    if not os.path.exists(filename):
        with open(filename, 'w') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(fields)
    with open(filename, 'a') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(rows)
            

try:
    
    
    mfc4.write_setpoint(30)
    # time.sleep(180)
    time.sleep(10)
    
    
    # flows = [0, 2.3, 4, 6, 9, 12, 16, 20, 25, 30]
    flows = [0, 30]
    for flow in flows[::-1]:
        write_slopes_for_flow(
            setpoint_mfc4=flow,
            nb_slopes=1
        )
    
finally:
    mfc4.write_setpoint(0)
