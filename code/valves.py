import pyads
import logging


class ValvesNC:

    def __init__(self, plc, nb_valves=11):
        self.logger = logging.getLogger('Valves normally closed')
        self.plc = plc
        self.nb_valves = nb_valves
    
    def read_valve_state(self, nb):
        return self.plc.read_by_name(f'MAIN.valve{nb}_state')
    
    def write_valve_state(self, nb, b):
        self.logger.debug(('Opening' if b else 'Closing') + f' valve {nb}...')
        self.plc.write_by_name(f'MAIN.valve{nb}_state', b)
    
    def close_all_valves(self):
        for i in range(1, self.nb_valves+1):
            self.write_valve_state(i, False)


class ValveNO:

    def __init__(self, plc):
        self.logger = logging.getLogger('Valve normally open')
        self.plc = plc
      
    def read_valve_state(self):
        return self.plc.read_by_name(f'MAIN.valve12_state')
    
    def write_valve_state(self, b):
        self.logger.debug(('Closing' if b else 'Opening') + f' valve 12...')
        self.plc.write_by_name(f'MAIN.valve12_state', b)
    
    

if __name__ == '__main__':
    
    LOGGING_LEVEL = 'DEBUG'

    try:
        logging.basicConfig(level=LOGGING_LEVEL, format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

        plc = pyads.Connection('127.0.0.1.1.1', 851)
        plc.open()
        
        valves = ValvesNC(plc, 1)
        

    
    finally:
        try:
            valves.close_all_valves()
        except:
            pass
        plc.close()
