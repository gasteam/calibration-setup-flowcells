from dotenv import load_dotenv
load_dotenv('code/flowcell_db/.env')

from general_operations import *
import pandas as pd
import numpy as np
from pathlib import Path

def sub(v, default):
    '''Return default argument if value is NaN, otherwise return value itself'''
    if pd.isnull(v):
        return default
    return v

def write_calibrations():
    command = dedent(f'''\
        select c.* from latest_states_flowcell lsf
        inner join calibrations c on lsf.active_calibration_id = c.id
    ''')
    df = execute_command_db_return_df(command)

    with open('code/flowcell_db/database_source_files/CalibConstants.txt', 'w') as f:
        f.write(
            '"ID","CellID",' +
            '"low_x0","low_x1","low_x2","low_x3","low_x4",' +
            '"low_ZeroFlowSlope",' +
            '"low_minFlow","low_maxFlow",' +
            '"high_x0","high_x1","high_x2","high_x3","high_x4",' +
            '"high_ZeroFlowSlope",' +
            '"high_minFlow","high_maxFlow",' +
            '"DateTimeStamp","CalibrationGas"\n'
        )

        df = df.reset_index()
        for _, row in df.iterrows():
        
            high_minFlow = row['low_flow_max'] if row['high_flow_enabled'] else -1
            high_ZeroFlowSlope = row['slope_low_flow_max'] if row['high_flow_enabled'] else -1

            f.write(
                f"0,{row['flowcell_id']}," +
                f"{row['low_x0']},{row['low_x1']},{row['low_x2']},{row['low_x3']},{row['low_x4']}," +
                f"{row['slope_low_flow_min']}," +
                f"{row['low_flow_min']},{row['low_flow_max']}," +
                f"{sub(row['high_x0'],0)},{sub(row['high_x1'],0)},{sub(row['high_x2'],0)},{sub(row['high_x3'],0)},{sub(row['high_x4'],0)}," +
                f"{high_ZeroFlowSlope}," +
                f"{high_minFlow},{sub(row['high_flow_max'],-1)}," +
                f"\"{row['timestamp'].strftime('%d/%m/%Y %X')}\",\"{row['calibration_gas']}\"\n"
            )


class files_access:
    '''Object to build information that goes in 2 Access DB tables'''
    
    def __init__(self, name_table_access_db):
        self.name_table_access_db = name_table_access_db
        self.elmb_to_flowcells = dict() # Map ELMB info to flowcell_id array
    
    def add_data(self, node_id, rack, position, flowcell_id, channel, is_input):
        key = (node_id, rack, position)
        # Add ELMB if not exists
        self.elmb_to_flowcells[key] = self.elmb_to_flowcells.get(key, np.zeros((2,18), int))
        # Update flowcell_id array
        array_flowcells = self.elmb_to_flowcells[key]
        if array_flowcells[0 if is_input else 1, channel-1] != 0:
            print(f'Warning! Table {self.name_table_access_db} has >1 flowcells in same position in rack {rack}, position {position}, channel {channel}, input={is_input}')
        array_flowcells[0 if is_input else 1, channel-1] = flowcell_id


def add_dummy_flowcells(flowcell_id_array, dummy_flowcell_id=777):
    '''Put dummy flowcells instead of zeros where necessary'''
    for i in range(len(flowcell_id_array)):
        if flowcell_id_array[-1-i] != 0:
            # Found last channel with non-zero flowcell ids => replace all zeros before this channel with the dummy flowcell id
            for k in range(-1-i, -len(flowcell_id_array)-1, -1):
                if flowcell_id_array[k] == 0:
                    flowcell_id_array[k] = dummy_flowcell_id
            break
                    
                    
def write_files():
    '''Write the Access DB files for the Node IDs and flowcell locations'''
    
    command = dedent(f'''\
        select l.id, l.name_table_access_db, lse.node_id, l.rack, l.position, lsf.flowcell_id, lsf.channel, lsf.is_input from latest_states_elmb lse 
        join locations l on l.id = lse.location_id
        join latest_states_flowcell lsf on lsf.location_id = l.id 
        where l.experiment != '256' and l.name_table_access_db is not null 
    ''')
    df = execute_command_db_return_df(command)
    dict_files = dict()
    
    df = df.reset_index()
    for _, row in df.iterrows():
        name_table = row['name_table_access_db']
        dict_files[name_table] = dict_files.get(name_table, files_access(name_table))
        dict_files[name_table].add_data(row['node_id'], row['rack'], row['position'], row['flowcell_id'], row['channel'], row['is_input'])
        
    
    # Write data to files
    for name_table, obj_files in dict_files.items():
        
        keys_iter = list(obj_files.elmb_to_flowcells.keys())
        keys_iter.sort(key=lambda x: (x[1],x[2]))
        
        with open(f'code/flowcell_db/database_source_files/{name_table} NodeID.txt', 'w') as f:
            f.write(
                '"ID","Node ID","Rack","Position",' + \
                '"Pos 4","Pos 5","Pos 6","Pos 7","Pos 8","Pos 9","Pos 10","Pos 11","Pos 12","Pos 13","Pos 14","Pos 15","Pos 16","Pos 17","Pos 18"\n'
            )
            id_table = 1
            for key in keys_iter:
                node_id, rack, position = key
                f.write(
                    f'{id_table},{node_id},{rack},{position},' + \
                    f'0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n'
                )
                id_table += 1
        
        with open(f'code/flowcell_db/database_source_files/{name_table}.txt', 'w') as f:
            f.write(
                '"ID","Pos 1","Pos 2","Pos 3","Pos 4","Pos 5","Pos 6","Pos 7","Pos 8","Pos 9","Pos 10","Pos 11","Pos 12","Pos 13","Pos 14","Pos 15","Pos 16","Pos 17","Pos 18"\n'
            )
            id_table = 1
            for key in keys_iter:
                array_flowcell_ids = obj_files.elmb_to_flowcells[key]                
                input_ids = array_flowcell_ids[0]
                output_ids = array_flowcell_ids[1]
                
                # Put dummy flowcells instead of zeros where necessary
                add_dummy_flowcells(input_ids)
                add_dummy_flowcells(output_ids)
                
                input_str = ','.join([str(n) for n in input_ids])
                output_str = ','.join([str(n) for n in output_ids])
                f.write(
                    f'{id_table},{input_str}\n'
                )
                f.write(
                    f'{id_table+1},{output_str}\n'
                )
                id_table += 2


if __name__ == '__main__':
    Path('code/flowcell_db/database_source_files').mkdir(exist_ok=True)
    write_calibrations()
    write_files()
