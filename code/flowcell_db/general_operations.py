from raw_operations import *
import pandas as pd


def get_location_id(
    experiment: str,
    system: str,
    rack: int,
    position: int
):
    '''Return the id for the specified location'''
    command = dedent(f'''\
    SELECT id FROM public.locations
    WHERE experiment = {get_sql_str(experiment)}
        AND system = {get_sql_str(system)}
        AND rack = {rack}
        AND position = {position}
    ''')
    return execute_command_db_return_1(command)


def get_current_state_flowcell(flowcell_id: int):
    '''Return a dataframe with the current state of the given flowcell'''
    command = dedent(f'''\
        select sf.*
        from public.states_flowcell sf
        inner join (
            select flowcell_id, max(timestamp) timestamp
            from public.states_flowcell
            where flowcell_id = {flowcell_id}
            group by flowcell_id
        ) sf_max
        on sf.flowcell_id = sf_max.flowcell_id and sf.timestamp = sf_max.timestamp
    ''')
    return execute_command_db_return_df(command).fillna('NULL')


def change_active_calibration_flowcell(
    flowcell_id: int,
    timestamp: datetime,
    active_calibration_id: int
):
    '''Change the active calibration of a flowcell
    (i.e. the calibration parameters that are used in the gas systems).'''
    current_state = get_current_state_flowcell(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=timestamp,
        location_id=current_state.location_id[0],
        active_calibration_id=active_calibration_id,
        channel=current_state.channel[0],
        is_input=current_state.is_input[0]
    )


def get_most_recent_calibration_id(flowcell_id: int):
    command = dedent(f'''\
        select id
        from calibrations c
        inner join (
            select max(timestamp) timestamp
            from calibrations
            where flowcell_id = {flowcell_id}
        ) temp
        on c.timestamp = temp.timestamp
        where flowcell_id = {flowcell_id}
    ''')
    return execute_command_db_return_1(command)
    

def update_state_to_most_recent_calibration(flowcell_id: int, timestamp: datetime):
    '''Update the state of the given flowcell to the most recent calibration performed.'''
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    change_active_calibration_flowcell(flowcell_id, timestamp, calibration_id)
    

    
def move_flowcell(
    flowcell_id: int,
    timestamp: datetime,
    experiment: str,
    system: str,
    rack: int,
    position: int,
    channel: int,
    is_input: bool
):
    '''Change the physical location of a flowcell'''
    current_state = get_current_state_flowcell(flowcell_id)
    location_id = get_location_id(experiment, system, rack, position)
    add_state_flowcell(
        flowcell_id,
        timestamp,
        location_id,
        current_state.active_calibration_id[0],
        channel,
        is_input
    )

