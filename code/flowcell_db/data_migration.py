
from dotenv import load_dotenv
load_dotenv('code/flowcell_db/.env')
from general_operations import *
import pandas as pd
import math
import numpy as np
import logging

logging.basicConfig(level='INFO')

## Order of migration
# 0. locations
# 1. ELMB Node IDs
# 2. flowcells
# 3. calibrations
# 4. states_flowcell
# 5. bugfix: add missing flowcell states (caused by dummy flowcells)



def add_locations():
    add_location('256', '0', 0, 0, 'Location for spare components in building 256')
    
    add_location('ALICE', 'MCH', 61, 1, 'Muon tracker', name_table_access_db='ALICE CPC MUON TRACK')
    add_location('ALICE', 'MCH', 62, 1, 'Muon tracker', name_table_access_db='ALICE CPC MUON TRACK')
    add_location('ALICE', 'CPV', 61, 1, 'Distribution', name_table_access_db='ALICE CPV')
    add_location('ALICE', 'CPV', 62, 1, 'Envelope of rack 61', name_table_access_db='ALICE CPV')
    add_location('ALICE', 'HMP', 61, 1, name_table_access_db='ALICE HMPID')
    add_location('ALICE', 'HMP', 62, 1, name_table_access_db='ALICE HMPID')
    add_location('ALICE', 'HMP', 63, 1, name_table_access_db='ALICE HMPID')
    add_location('ALICE', 'PMD', 61, 1, 'Unused', name_table_access_db='ALICE PMD')
    add_location('ALICE', 'MID', 61, 1, 'Muon identifier', name_table_access_db='ALICE RPC MUON TRIG')
    add_location('ALICE', 'MID', 61, 2, 'Muon identifier', name_table_access_db='ALICE RPC MUON TRIG')
    add_location('ALICE', 'TOF', 61, 1, 'Bottom', name_table_access_db='ALICE TOF BOTTOM')
    add_location('ALICE', 'TOF', 62, 1, 'Top', name_table_access_db='ALICE TOF TOP')
    add_location('ALICE', 'TRD', 6164, 1, 'Physical rack 1, contains WinCC racks 61-64', name_table_access_db='ALICE TRD RACK 1')
    add_location('ALICE', 'TRD', 6570, 1, 'Physical rack 2, contains WinCC racks 65-70', name_table_access_db='ALICE TRD RACK 2')
    add_location('ALICE', 'TRD', 7174, 1, 'Physical rack 3, contains WinCC racks 71-74', name_table_access_db='ALICE TRD RACK 3')


    add_location('ATLAS', 'MMG', 61, 1, 'EI level 4 side C', name_table_access_db='ATLAS CSC EI LEVEL 4 SIDE C')
    add_location('ATLAS', 'MMG', 62, 1, 'EI level 4 side A', name_table_access_db='ATLAS CSC EI LEVEL 4 SIDE A')

    add_location('ATLAS', 'MDT', 61, 1, 'EI level 4 side C', name_table_access_db='ATLAS MDT GAS EI LEVEL 4 SIDE C')
    add_location('ATLAS', 'MDT', 61, 2, 'EI level 4 side C', name_table_access_db='ATLAS MDT GAS EI LEVEL 4 SIDE C')
    add_location('ATLAS', 'MDT', 62, 1, 'EM level bottom side C', name_table_access_db='ATLAS MDT GAS EM LEVEL BOT SIDE C')
    add_location('ATLAS', 'MDT', 62, 2, 'EM level bottom side C', name_table_access_db='ATLAS MDT GAS EM LEVEL BOT SIDE C')
    add_location('ATLAS', 'MDT', 63, 1, 'EM level top side C', name_table_access_db='ATLAS MDT GAS EM LEVEL TOP SIDE C')
    add_location('ATLAS', 'MDT', 63, 2, 'EM level top side C', name_table_access_db='ATLAS MDT GAS EM LEVEL TOP SIDE C')
    add_location('ATLAS', 'MDT', 64, 1, 'EO level bottom side C', name_table_access_db='ATLAS MDT GAS EO LEVEL BOT SIDE C')
    add_location('ATLAS', 'MDT', 64, 2, 'EO level bottom side C', name_table_access_db='ATLAS MDT GAS EO LEVEL BOT SIDE C')
    add_location('ATLAS', 'MDT', 65, 1, 'EO level top side C', name_table_access_db='ATLAS MDT GAS EO LEVEL TOP SIDE C')
    add_location('ATLAS', 'MDT', 65, 2, 'EO level top side C', name_table_access_db='ATLAS MDT GAS EO LEVEL TOP SIDE C')
    add_location('ATLAS', 'MDT', 66, 1, 'Barrel level 0', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 0')
    add_location('ATLAS', 'MDT', 66, 2, 'Barrel level 0', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 0')
    add_location('ATLAS', 'MDT', 67, 1, 'Barrel level 1', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 1')
    add_location('ATLAS', 'MDT', 67, 2, 'Barrel level 1', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 1')
    add_location('ATLAS', 'MDT', 68, 1, 'Barrel level 4', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 4')
    add_location('ATLAS', 'MDT', 68, 2, 'Barrel level 4', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 4')
    add_location('ATLAS', 'MDT', 69, 1, 'Barrel level 7', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 7')
    add_location('ATLAS', 'MDT', 69, 2, 'Barrel level 7', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 7')
    add_location('ATLAS', 'MDT', 70, 1, 'Barrel level 8', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 8')
    add_location('ATLAS', 'MDT', 70, 2, 'Barrel level 8', name_table_access_db='ATLAS MDT GAS BARREL LEVEL 8')
    add_location('ATLAS', 'MDT', 71, 1, 'EI level 4 side A', name_table_access_db='ATLAS MDT GAS EI LEVEL 4 SIDE A')
    add_location('ATLAS', 'MDT', 71, 2, 'EI level 4 side A', name_table_access_db='ATLAS MDT GAS EI LEVEL 4 SIDE A')
    add_location('ATLAS', 'MDT', 72, 1, 'EM level bottom side A', name_table_access_db='ATLAS MDT GAS EM LEVEL BOT SIDE A')
    add_location('ATLAS', 'MDT', 72, 2, 'EM level bottom side A', name_table_access_db='ATLAS MDT GAS EM LEVEL BOT SIDE A')
    add_location('ATLAS', 'MDT', 73, 1, 'EM level top side A', name_table_access_db='ATLAS MDT GAS EM LEVEL TOP SIDE A')
    add_location('ATLAS', 'MDT', 73, 2, 'EM level top side A', name_table_access_db='ATLAS MDT GAS EM LEVEL TOP SIDE A')
    add_location('ATLAS', 'MDT', 74, 1, 'EO level bottom side A', name_table_access_db='ATLAS MDT GAS EO LEVEL BOT SIDE A')
    add_location('ATLAS', 'MDT', 74, 2, 'EO level bottom side A', name_table_access_db='ATLAS MDT GAS EO LEVEL BOT SIDE A')
    add_location('ATLAS', 'MDT', 75, 1, 'EO level top side A', name_table_access_db='ATLAS MDT GAS EO LEVEL TOP SIDE A')
    add_location('ATLAS', 'MDT', 75, 2, 'EO level top side A', name_table_access_db='ATLAS MDT GAS EO LEVEL TOP SIDE A')

    add_location('ATLAS', 'RPC', 61, 1, 'Barrel level 0', name_table_access_db='ATLAS RPC BARREL LEVEL 0')
    add_location('ATLAS', 'RPC', 61, 2, 'Barrel level 0', name_table_access_db='ATLAS RPC BARREL LEVEL 0')
    add_location('ATLAS', 'RPC', 62, 1, 'Barrel level 1', name_table_access_db='ATLAS RPC BARREL LEVEL 1')
    add_location('ATLAS', 'RPC', 62, 2, 'Barrel level 1', name_table_access_db='ATLAS RPC BARREL LEVEL 1')
    add_location('ATLAS', 'RPC', 63, 1, 'Barrel level 4', name_table_access_db='ATLAS RPC BARREL LEVEL 4')
    add_location('ATLAS', 'RPC', 63, 2, 'Barrel level 4', name_table_access_db='ATLAS RPC BARREL LEVEL 4')
    add_location('ATLAS', 'RPC', 64, 1, 'Barrel level 7', name_table_access_db='ATLAS RPC BARREL LEVEL 7')
    add_location('ATLAS', 'RPC', 64, 2, 'Barrel level 7', name_table_access_db='ATLAS RPC BARREL LEVEL 7')
    add_location('ATLAS', 'RPC', 65, 1, 'Barrel level 8', name_table_access_db='ATLAS RPC BARREL LEVEL 8')
    add_location('ATLAS', 'RPC', 65, 2, 'Barrel level 8', name_table_access_db='ATLAS RPC BARREL LEVEL 8')
    add_location('ATLAS', 'RPC', 66, 1, 'Barrel level 0.5', name_table_access_db='ATLAS RPC BARREL LEVEL 0,5')
    add_location('ATLAS', 'RPC', 67, 1, 'Barrel level 2', name_table_access_db='ATLAS RPC BARREL LEVEL 2')
    add_location('ATLAS', 'RPC', 67, 2, 'Barrel level 2', name_table_access_db='ATLAS RPC BARREL LEVEL 2')
    add_location('ATLAS', 'RPC', 68, 1, 'Barrel level 7.5', name_table_access_db='ATLAS RPC BARREL LEVEL 7,5')
    add_location('ATLAS', 'RPC', 68, 2, 'Barrel level 7.5', name_table_access_db='ATLAS RPC BARREL LEVEL 7,5')
    add_location('ATLAS', 'RPC', 69, 1, 'Barrel level 8.5', name_table_access_db='ATLAS RPC BARREL LEVEL 8,5')

    add_location('ATLAS', 'TGC', 61, 1, 'EM level bottom side C, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL BOT SIDE C')
    add_location('ATLAS', 'TGC', 62, 1, 'EM level 4 side C, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL 4 SIDE C')
    add_location('ATLAS', 'TGC', 62, 2, 'EM level 4 side C, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL 4 SIDE C')
    add_location('ATLAS', 'TGC', 63, 1, 'EI level 4 side C, distribution', name_table_access_db='ATLAS TGC GAS EI LEVEL 4 SIDE C')
    add_location('ATLAS', 'TGC', 63, 2, 'EI level 4 side C, distribution', name_table_access_db='ATLAS TGC GAS EI LEVEL 4 SIDE C')
    add_location('ATLAS', 'TGC', 64, 1, 'EM level top side C, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL TOP SIDE C')
    add_location('ATLAS', 'TGC', 65, 1, 'EM level bottom side A, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL BOT SIDE A')
    add_location('ATLAS', 'TGC', 66, 1, 'EM level 4 side A, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL 4 SIDE A')
    add_location('ATLAS', 'TGC', 66, 2, 'EM level 4 side A, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL 4 SIDE A')
    add_location('ATLAS', 'TGC', 67, 1, 'EI level 4 side A, distribution', name_table_access_db='ATLAS TGC GAS EI LEVEL 4 SIDE A')
    add_location('ATLAS', 'TGC', 67, 2, 'EI level 4 side A, distribution', name_table_access_db='ATLAS TGC GAS EI LEVEL 4 SIDE A')
    add_location('ATLAS', 'TGC', 68, 1, 'EM level top side A, distribution', name_table_access_db='ATLAS TGC GAS EM LEVEL TOP SIDE A')
    add_location('ATLAS', 'TGC', 69, 1, 'EM level bottom side C, envelope of rack 61', name_table_access_db='ATLAS TGC GAS EM LEVEL BOT SIDE C')
    add_location('ATLAS', 'TGC', 70, 1, 'EM level 4 side C, envelope of rack 62', name_table_access_db='ATLAS TGC GAS EM LEVEL 4 SIDE C')
    add_location('ATLAS', 'TGC', 71, 1, 'EI level 4 side C, envelope of rack 63', name_table_access_db='ATLAS TGC GAS EI LEVEL 4 SIDE C')
    add_location('ATLAS', 'TGC', 72, 1, 'EM level top side C, envelope of rack 64', name_table_access_db='ATLAS TGC GAS EM LEVEL TOP SIDE C')
    add_location('ATLAS', 'TGC', 73, 1, 'EM level bottom side A, envelope of rack 65', name_table_access_db='ATLAS TGC GAS EM LEVEL BOT SIDE A')
    add_location('ATLAS', 'TGC', 74, 1, 'EM level 4 side A, envelope of rack 66', name_table_access_db='ATLAS TGC GAS EM LEVEL 4 SIDE A')
    add_location('ATLAS', 'TGC', 75, 1, 'EI level 4 side A, envelope of rack 67', name_table_access_db='ATLAS TGC GAS EI LEVEL 4 SIDE A')
    add_location('ATLAS', 'TGC', 76, 1, 'EM level top side A, envelope of rack 68', name_table_access_db='ATLAS TGC GAS EM LEVEL TOP SIDE A')

    add_location('ATLAS', 'TRT', 61, 1, 'Endcap', name_table_access_db='ATLAS TRT ENDCAP')
    add_location('ATLAS', 'TRT', 62, 1, 'Barrel', name_table_access_db='ATLAS TRT BARREL')
    add_location('ATLAS', 'TRT', 62, 2, 'Barrel', name_table_access_db='ATLAS TRT BARREL')
    add_location('ATLAS', 'TRT', 62, 3, 'Barrel', name_table_access_db='ATLAS TRT BARREL')
    add_location('ATLAS', 'TRT', 63, 1, 'Endcap', name_table_access_db='ATLAS TRT ENDCAP')
    add_location('ATLAS', 'TRT', 64, 1, 'CO2 envelope', name_table_access_db='ATLAS TRT CO2')
    add_location('ATLAS', 'TRT', 64, 2, 'CO2 envelope', name_table_access_db='ATLAS TRT CO2')
    

    add_location('CMS', 'TOTEM_GAS', 61, 1, 'Gas +', name_table_access_db='TOTEM GAS +')
    add_location('CMS', 'TOTEM_GAS', 61, 2, 'Gas +', name_table_access_db='TOTEM GAS +')
    add_location('CMS', 'TOTEM_GAS', 62, 1, 'Gas -', name_table_access_db='TOTEM GAS -')
    add_location('CMS', 'TOTEM_GAS', 62, 2, 'Gas -', name_table_access_db='TOTEM GAS -')
    add_location('CMS', 'TOTEM_T1', 61, 1, 'T1-', name_table_access_db='TOTEM T1-')
    add_location('CMS', 'TOTEM_T1', 62, 1, 'T1+', name_table_access_db='TOTEM T1+')
    add_location('CMS', 'TOTEM_T2', 61, 1, 'T2-', name_table_access_db='TOTEM T2-')
    add_location('CMS', 'TOTEM_T2', 62, 1, 'T2+', name_table_access_db='TOTEM T2+')

    add_location('CMS', 'CSC', 61, 1, 'ME-1', name_table_access_db='CMS CSC GAS ME-1')
    add_location('CMS', 'CSC', 62, 1, 'ME-1', name_table_access_db='CMS CSC GAS ME-1')
    add_location('CMS', 'CSC', 63, 1, 'ME-2/-3', name_table_access_db='CMS CSC GAS ME-2_-3')
    add_location('CMS', 'CSC', 64, 1, 'ME-2/-3', name_table_access_db='CMS CSC GAS ME-2_-3')
    add_location('CMS', 'CSC', 65, 1, 'ME-4', name_table_access_db='CMS CSC GAS ME-4')
    add_location('CMS', 'CSC', 66, 1, 'ME+1', name_table_access_db='CMS CSC GAS ME+1')
    add_location('CMS', 'CSC', 67, 1, 'ME+1', name_table_access_db='CMS CSC GAS ME+1')
    add_location('CMS', 'CSC', 68, 1, 'ME+2/+3', name_table_access_db='CMS CSC GAS ME+2_+3')
    add_location('CMS', 'CSC', 69, 1, 'ME+2/+3', name_table_access_db='CMS CSC GAS ME+2_+3')
    add_location('CMS', 'CSC', 70, 1, 'ME+4', name_table_access_db='CMS CSC GAS ME+4')

    add_location('CMS', 'DT', 61, 1, 'MB-2', name_table_access_db='CMS DT GAS MB-2')
    add_location('CMS', 'DT', 61, 2, 'MB-2', name_table_access_db='CMS DT GAS MB-2')
    add_location('CMS', 'DT', 61, 3, 'MB-2', name_table_access_db='CMS DT GAS MB-2')
    add_location('CMS', 'DT', 61, 4, 'MB-2', name_table_access_db='CMS DT GAS MB-2')
    add_location('CMS', 'DT', 62, 1, 'MB-1', name_table_access_db='CMS DT GAS MB-1')
    add_location('CMS', 'DT', 62, 2, 'MB-1', name_table_access_db='CMS DT GAS MB-1')
    add_location('CMS', 'DT', 62, 3, 'MB-1', name_table_access_db='CMS DT GAS MB-1')
    add_location('CMS', 'DT', 62, 4, 'MB-1', name_table_access_db='CMS DT GAS MB-1')
    add_location('CMS', 'DT', 63, 1, 'MB0', name_table_access_db='CMS DT GAS MB0')
    add_location('CMS', 'DT', 63, 2, 'MB0', name_table_access_db='CMS DT GAS MB0')
    add_location('CMS', 'DT', 63, 3, 'MB0', name_table_access_db='CMS DT GAS MB0')
    add_location('CMS', 'DT', 63, 4, 'MB0', name_table_access_db='CMS DT GAS MB0')
    add_location('CMS', 'DT', 64, 1, 'MB+1', name_table_access_db='CMS DT GAS MB+1')
    add_location('CMS', 'DT', 64, 2, 'MB+1', name_table_access_db='CMS DT GAS MB+1')
    add_location('CMS', 'DT', 64, 3, 'MB+1', name_table_access_db='CMS DT GAS MB+1')
    add_location('CMS', 'DT', 64, 4, 'MB+1', name_table_access_db='CMS DT GAS MB+1')
    add_location('CMS', 'DT', 65, 1, 'MB+2', name_table_access_db='CMS DT GAS MB+2')
    add_location('CMS', 'DT', 65, 2, 'MB+2', name_table_access_db='CMS DT GAS MB+2')
    add_location('CMS', 'DT', 65, 3, 'MB+2', name_table_access_db='CMS DT GAS MB+2')
    add_location('CMS', 'DT', 65, 4, 'MB+2', name_table_access_db='CMS DT GAS MB+2')

    add_location('CMS', 'GEM', 61, 1, 'RE-2', name_table_access_db='CMS GEM GAS RE-2')
    add_location('CMS', 'GEM', 62, 1, 'RE-1', name_table_access_db='CMS GEM GAS RE-1')
    add_location('CMS', 'GEM', 63, 1, 'RE0', name_table_access_db='CMS GEM GAS RE0')
    add_location('CMS', 'GEM', 64, 1, 'RE+1', name_table_access_db='CMS GEM GAS RE+1')
    add_location('CMS', 'GEM', 65, 1, 'RE+2', name_table_access_db='CMS GEM GAS RE+2')
    add_location('CMS', 'GEM', 66, 1, 'RE+3', name_table_access_db='CMS GEM GAS RE+3')

    add_location('CMS', 'RPC', 61, 1, 'RE-1', name_table_access_db='CMS RPC GAS RE-1')
    add_location('CMS', 'RPC', 62, 1, 'RE-2', name_table_access_db='CMS RPC GAS RE-2')
    add_location('CMS', 'RPC', 63, 1, 'RE-3', name_table_access_db='CMS RPC GAS RE-3')
    add_location('CMS', 'RPC', 64, 1, 'RE-4', name_table_access_db='CMS RPC GAS RE-4')
    add_location('CMS', 'RPC', 65, 1, 'RE-1', name_table_access_db='CMS RPC GAS RE-1')
    add_location('CMS', 'RPC', 66, 1, 'RE-2', name_table_access_db='CMS RPC GAS RE-2')
    add_location('CMS', 'RPC', 67, 1, 'RE-3', name_table_access_db='CMS RPC GAS RE-3')
    add_location('CMS', 'RPC', 68, 1, 'RE-4', name_table_access_db='CMS RPC GAS RE-4')
    add_location('CMS', 'RPC', 68, 2, 'RE-4', name_table_access_db='CMS RPC GAS RE-4')
    add_location('CMS', 'RPC', 69, 1, 'RB-2', name_table_access_db='CMS RPC GAS RB-2')
    add_location('CMS', 'RPC', 69, 2, 'RB-2', name_table_access_db='CMS RPC GAS RB-2')
    add_location('CMS', 'RPC', 70, 1, 'RB-1', name_table_access_db='CMS RPC GAS RB-1')
    add_location('CMS', 'RPC', 70, 2, 'RB-1', name_table_access_db='CMS RPC GAS RB-1')
    add_location('CMS', 'RPC', 71, 1, 'RB0', name_table_access_db='CMS RPC GAS RB0')
    add_location('CMS', 'RPC', 71, 2, 'RB0', name_table_access_db='CMS RPC GAS RB0')
    add_location('CMS', 'RPC', 72, 1, 'RB+1', name_table_access_db='CMS RPC GAS RB+1')
    add_location('CMS', 'RPC', 72, 2, 'RB+1', name_table_access_db='CMS RPC GAS RB+1')
    add_location('CMS', 'RPC', 73, 1, 'RB+2', name_table_access_db='CMS RPC GAS RB+2')
    add_location('CMS', 'RPC', 73, 2, 'RB+2', name_table_access_db='CMS RPC GAS RB+2')
    add_location('CMS', 'RPC', 74, 1, 'RB-2', name_table_access_db='CMS RPC GAS RB-2')
    add_location('CMS', 'RPC', 74, 2, 'RB-2', name_table_access_db='CMS RPC GAS RB-2')
    add_location('CMS', 'RPC', 75, 1, 'RB-1', name_table_access_db='CMS RPC GAS RB-1')
    add_location('CMS', 'RPC', 75, 2, 'RB-1', name_table_access_db='CMS RPC GAS RB-1')
    add_location('CMS', 'RPC', 76, 1, 'RB0', name_table_access_db='CMS RPC GAS RB0')
    add_location('CMS', 'RPC', 76, 2, 'RB0', name_table_access_db='CMS RPC GAS RB0')
    add_location('CMS', 'RPC', 77, 1, 'RB+1', name_table_access_db='CMS RPC GAS RB+1')
    add_location('CMS', 'RPC', 77, 2, 'RB+1', name_table_access_db='CMS RPC GAS RB+1')
    add_location('CMS', 'RPC', 78, 1, 'RB+2', name_table_access_db='CMS RPC GAS RB+2')
    add_location('CMS', 'RPC', 78, 2, 'RB+2', name_table_access_db='CMS RPC GAS RB+2')
    add_location('CMS', 'RPC', 79, 1, 'RE+1', name_table_access_db='CMS RPC GAS RE+1')
    add_location('CMS', 'RPC', 80, 1, 'RE+2', name_table_access_db='CMS RPC GAS RE+2')
    add_location('CMS', 'RPC', 81, 1, 'RE+3', name_table_access_db='CMS RPC GAS RE+3')
    add_location('CMS', 'RPC', 82, 1, 'RE+4', name_table_access_db='CMS RPC GAS RE+4')
    add_location('CMS', 'RPC', 83, 1, 'RE+1', name_table_access_db='CMS RPC GAS RE+1')
    add_location('CMS', 'RPC', 84, 1, 'RE+2', name_table_access_db='CMS RPC GAS RE+2')
    add_location('CMS', 'RPC', 85, 1, 'RE+3', name_table_access_db='CMS RPC GAS RE+3')
    add_location('CMS', 'RPC', 86, 1, 'RE+4', name_table_access_db='CMS RPC GAS RE+4')
    add_location('CMS', 'RPC', 86, 2, 'RE+4', name_table_access_db='CMS RPC GAS RE+4')
    add_location('CMS', 'RPC', 87, 1, 'RE-3', name_table_access_db='CMS RPC GAS RE-3')
    add_location('CMS', 'RPC', 88, 1, 'RE+3', name_table_access_db='CMS RPC GAS RE+3')


    add_location('LHCb', 'GEM', 61, 1, 'side A', name_table_access_db='LHCb GEM SIDE A')
    add_location('LHCb', 'GEM', 62, 1, 'side C', name_table_access_db='LHCb GEM SIDE C')

    add_location('LHCb', 'MUON', 61, 1, 'M1 GEM side A', name_table_access_db='LHCb MUON M1 GEM SIDE A')
    add_location('LHCb', 'MUON', 61, 2, 'M1 GEM side A', name_table_access_db='LHCb MUON M1 GEM SIDE A')
    add_location('LHCb', 'MUON', 62, 1, 'M1 GEM side C', name_table_access_db='LHCb MUON M1 GEM SIDE C')
    add_location('LHCb', 'MUON', 62, 2, 'M1 GEM side C', name_table_access_db='LHCb MUON M1 GEM SIDE C')

    add_location('LHCb', 'MUON', 63, 1, 'M2M3 side A', name_table_access_db='LHCb MUON M2M3 SIDE A')
    add_location('LHCb', 'MUON', 63, 2, 'M2M3 side A', name_table_access_db='LHCb MUON M2M3 SIDE A')
    add_location('LHCb', 'MUON', 64, 1, 'M2M3 side C', name_table_access_db='LHCb MUON M2M3 SIDE C')
    add_location('LHCb', 'MUON', 64, 2, 'M2M3 side C', name_table_access_db='LHCb MUON M2M3 SIDE C')
    add_location('LHCb', 'MUON', 65, 1, 'M4M5 side A', name_table_access_db='LHCb MUON M4M5 SIDE A')
    add_location('LHCb', 'MUON', 65, 2, 'M4M5 side A', name_table_access_db='LHCb MUON M4M5 SIDE A')
    add_location('LHCb', 'MUON', 66, 1, 'M4M5 side C', name_table_access_db='LHCb MUON M4M5 SIDE C')
    add_location('LHCb', 'MUON', 66, 2, 'M4M5 side C', name_table_access_db='LHCb MUON M4M5 SIDE C')

    add_location('LHCb', 'OT', 61, 1, 'Side A', name_table_access_db='LHCb MUON OT SIDE A')
    add_location('LHCb', 'OT', 62, 1, 'Side C', name_table_access_db='LHCb MUON OT SIDE C')


counter_elmb = 100 # Counter for placeholder elmb_ids

def node_id_helper(node_id, experiment, system, rack, position, description='unused'):
    # NOTE: the argument description is never used, this is intentional.
    global counter_elmb
    elmb_id = f'placeholder_{counter_elmb}'
    add_elmb(elmb_id, 'ELMB_v1')
    add_state_elmb(datetime(2024, 4, 12), node_id, get_location_id(experiment, system, rack, position), elmb_id=elmb_id)
    counter_elmb += 1


def add_node_ids():
    node_id_helper(1, 'ALICE', 'MCH', 61, 1)
    node_id_helper(2, 'ALICE', 'MCH', 62, 1)
    node_id_helper(1, 'ALICE', 'CPV', 61, 1)
    node_id_helper(2, 'ALICE', 'CPV', 62, 1)
    node_id_helper(1, 'ALICE', 'HMP', 61, 1)
    node_id_helper(2, 'ALICE', 'HMP', 62, 1)
    node_id_helper(3, 'ALICE', 'HMP', 63, 1)
    node_id_helper(1, 'ALICE', 'PMD', 61, 1)
    node_id_helper(1, 'ALICE', 'MID', 61, 1)
    node_id_helper(2, 'ALICE', 'MID', 61, 2)
    node_id_helper(1, 'ALICE', 'TOF', 61, 1)
    node_id_helper(2, 'ALICE', 'TOF', 62, 1)
    node_id_helper(1, 'ALICE', 'TRD', 6164, 1)
    node_id_helper(2, 'ALICE', 'TRD', 6570, 1)
    node_id_helper(3, 'ALICE', 'TRD', 7174, 1)


    node_id_helper(1, 'ATLAS', 'MMG', 61, 1)
    node_id_helper(2, 'ATLAS', 'MMG', 62, 1)
    
    node_id_helper(1, 'ATLAS', 'MDT', 61, 1)
    node_id_helper(2, 'ATLAS', 'MDT', 61, 2)
    node_id_helper(3, 'ATLAS', 'MDT', 62, 1)
    node_id_helper(4, 'ATLAS', 'MDT', 62, 2)
    node_id_helper(5, 'ATLAS', 'MDT', 63, 1)
    node_id_helper(6, 'ATLAS', 'MDT', 63, 2)
    node_id_helper(7, 'ATLAS', 'MDT', 64, 1)
    node_id_helper(8, 'ATLAS', 'MDT', 64, 2)
    node_id_helper(9, 'ATLAS', 'MDT', 65, 1)
    node_id_helper(10, 'ATLAS', 'MDT', 65, 2, 'EO level top side C')
    node_id_helper(11, 'ATLAS', 'MDT', 66, 1, 'Barrel level 0')
    node_id_helper(12, 'ATLAS', 'MDT', 66, 2, 'Barrel level 0')
    node_id_helper(13, 'ATLAS', 'MDT', 67, 1, 'Barrel level 1')
    node_id_helper(14, 'ATLAS', 'MDT', 67, 2, 'Barrel level 1')
    node_id_helper(15, 'ATLAS', 'MDT', 68, 1, 'Barrel level 4')
    node_id_helper(16, 'ATLAS', 'MDT', 68, 2, 'Barrel level 4')
    node_id_helper(17, 'ATLAS', 'MDT', 69, 1, 'Barrel level 7')
    node_id_helper(18, 'ATLAS', 'MDT', 69, 2, 'Barrel level 7')
    node_id_helper(19, 'ATLAS', 'MDT', 70, 1, 'Barrel level 8')
    node_id_helper(20, 'ATLAS', 'MDT', 70, 2, 'Barrel level 8')
    node_id_helper(21, 'ATLAS', 'MDT', 71, 1, 'EI level 4 side A')
    node_id_helper(22, 'ATLAS', 'MDT', 71, 2, 'EI level 4 side A')
    node_id_helper(23, 'ATLAS', 'MDT', 72, 1, 'EM level bottom side A')
    node_id_helper(24, 'ATLAS', 'MDT', 72, 2, 'EM level bottom side A')
    node_id_helper(25, 'ATLAS', 'MDT', 73, 1, 'EM level top side A')
    node_id_helper(26, 'ATLAS', 'MDT', 73, 2, 'EM level top side A')
    node_id_helper(27, 'ATLAS', 'MDT', 74, 1, 'EO level bottom side A')
    node_id_helper(28, 'ATLAS', 'MDT', 74, 2, 'EO level bottom side A')
    node_id_helper(29, 'ATLAS', 'MDT', 75, 1, 'EO level top side A')
    node_id_helper(30, 'ATLAS', 'MDT', 75, 2, 'EO level top side A')

    node_id_helper(1, 'ATLAS', 'RPC', 61, 1, 'Barrel level 0')
    node_id_helper(2, 'ATLAS', 'RPC', 61, 2, 'Barrel level 0')
    node_id_helper(3, 'ATLAS', 'RPC', 62, 1, 'Barrel level 1')
    node_id_helper(4, 'ATLAS', 'RPC', 62, 2, 'Barrel level 1')
    node_id_helper(5, 'ATLAS', 'RPC', 63, 1, 'Barrel level 4')
    node_id_helper(16, 'ATLAS', 'RPC', 63, 2, 'Barrel level 4')
    node_id_helper(6, 'ATLAS', 'RPC', 64, 1, 'Barrel level 7')
    node_id_helper(7, 'ATLAS', 'RPC', 64, 2, 'Barrel level 7')
    node_id_helper(8, 'ATLAS', 'RPC', 65, 1, 'Barrel level 8')
    node_id_helper(9, 'ATLAS', 'RPC', 65, 2, 'Barrel level 8')
    node_id_helper(10, 'ATLAS', 'RPC', 66, 1, 'Barrel level 0.5')
    node_id_helper(11, 'ATLAS', 'RPC', 67, 1, 'Barrel level 2')
    node_id_helper(12, 'ATLAS', 'RPC', 67, 2, 'Barrel level 2')
    node_id_helper(13, 'ATLAS', 'RPC', 68, 1, 'Barrel level 7.5')
    node_id_helper(14, 'ATLAS', 'RPC', 68, 2, 'Barrel level 7.5')
    node_id_helper(15, 'ATLAS', 'RPC', 69, 1, 'Barrel level 8.5')

    node_id_helper(1, 'ATLAS', 'TGC', 61, 1, 'EM level bottom side C, distribution')
    node_id_helper(2, 'ATLAS', 'TGC', 62, 1, 'EM level 4 side C, distribution')
    node_id_helper(3, 'ATLAS', 'TGC', 62, 2, 'EM level 4 side C, distribution')
    node_id_helper(4, 'ATLAS', 'TGC', 63, 1, 'EI level 4 side C, distribution')
    node_id_helper(19, 'ATLAS', 'TGC', 63, 2, 'EI level 4 side C, distribution')
    node_id_helper(5, 'ATLAS', 'TGC', 64, 1, 'EM level top side C, distribution')
    node_id_helper(6, 'ATLAS', 'TGC', 65, 1, 'EM level bottom side A, distribution')
    node_id_helper(7, 'ATLAS', 'TGC', 66, 1, 'EM level 4 side A, distribution')
    node_id_helper(8, 'ATLAS', 'TGC', 66, 2, 'EM level 4 side A, distribution')
    node_id_helper(9, 'ATLAS', 'TGC', 67, 1, 'EI level 4 side A, distribution')
    node_id_helper(20, 'ATLAS', 'TGC', 67, 2, 'EI level 4 side A, distribution')
    node_id_helper(10, 'ATLAS', 'TGC', 68, 1, 'EM level top side A, distribution')
    node_id_helper(11, 'ATLAS', 'TGC', 69, 1, 'EM level bottom side C, envelope of rack 61')
    node_id_helper(12, 'ATLAS', 'TGC', 70, 1, 'EM level 4 side C, envelope of rack 62')
    node_id_helper(13, 'ATLAS', 'TGC', 71, 1, 'EI level 4 side C, envelope of rack 63')
    node_id_helper(14, 'ATLAS', 'TGC', 72, 1, 'EM level top side C, envelope of rack 64')
    node_id_helper(15, 'ATLAS', 'TGC', 73, 1, 'EM level bottom side A, envelope of rack 65')
    node_id_helper(16, 'ATLAS', 'TGC', 74, 1, 'EM level 4 side A, envelope of rack 66')
    node_id_helper(17, 'ATLAS', 'TGC', 75, 1, 'EI level 4 side A, envelope of rack 67')
    node_id_helper(18, 'ATLAS', 'TGC', 76, 1, 'EM level top side A, envelope of rack 68')

    node_id_helper(1, 'ATLAS', 'TRT', 61, 1, 'Endcap')
    node_id_helper(2, 'ATLAS', 'TRT', 62, 1, 'Barrel')
    node_id_helper(3, 'ATLAS', 'TRT', 62, 2, 'Barrel')
    node_id_helper(4, 'ATLAS', 'TRT', 62, 3, 'Barrel')
    node_id_helper(5, 'ATLAS', 'TRT', 63, 1, 'Endcap')
    node_id_helper(6, 'ATLAS', 'TRT', 64, 1, 'CO2 envelope')
    node_id_helper(7, 'ATLAS', 'TRT', 64, 2, 'CO2 envelope')


    node_id_helper(1, 'CMS', 'TOTEM_GAS', 61, 1, 'Gas +')
    node_id_helper(2, 'CMS', 'TOTEM_GAS', 61, 2, 'Gas +')
    node_id_helper(3, 'CMS', 'TOTEM_GAS', 62, 1, 'Gas -')
    node_id_helper(4, 'CMS', 'TOTEM_GAS', 62, 2, 'Gas -')
    node_id_helper(1, 'CMS', 'TOTEM_T1', 61, 1, 'T1-')
    node_id_helper(2, 'CMS', 'TOTEM_T1', 62, 1, 'T1+')
    node_id_helper(1, 'CMS', 'TOTEM_T2', 61, 1, 'T2-')
    node_id_helper(2, 'CMS', 'TOTEM_T2', 62, 1, 'T2+')

    node_id_helper(2, 'CMS', 'CSC', 61, 1, 'ME-1')
    node_id_helper(1, 'CMS', 'CSC', 62, 1, 'ME-1')
    node_id_helper(3, 'CMS', 'CSC', 63, 1, 'ME-2/-3')
    node_id_helper(4, 'CMS', 'CSC', 64, 1, 'ME-2/-3')
    node_id_helper(5, 'CMS', 'CSC', 65, 1, 'ME-4')
    node_id_helper(7, 'CMS', 'CSC', 66, 1, 'ME+1')
    node_id_helper(6, 'CMS', 'CSC', 67, 1, 'ME+1')
    node_id_helper(8, 'CMS', 'CSC', 68, 1, 'ME+2/+3')
    node_id_helper(9, 'CMS', 'CSC', 69, 1, 'ME+2/+3')
    node_id_helper(10, 'CMS', 'CSC', 70, 1, 'ME+4')

    node_id_helper(1, 'CMS', 'DT', 61, 1, 'MB-2')
    node_id_helper(2, 'CMS', 'DT', 61, 2, 'MB-2')
    node_id_helper(3, 'CMS', 'DT', 61, 3, 'MB-2')
    node_id_helper(4, 'CMS', 'DT', 61, 4, 'MB-2')
    node_id_helper(5, 'CMS', 'DT', 62, 1, 'MB-1')
    node_id_helper(6, 'CMS', 'DT', 62, 2, 'MB-1')
    node_id_helper(7, 'CMS', 'DT', 62, 3, 'MB-1')
    node_id_helper(8, 'CMS', 'DT', 62, 4, 'MB-1')
    node_id_helper(9, 'CMS', 'DT', 63, 1, 'MB0')
    node_id_helper(10, 'CMS', 'DT', 63, 2, 'MB0')
    node_id_helper(11, 'CMS', 'DT', 63, 3, 'MB0')
    node_id_helper(12, 'CMS', 'DT', 63, 4, 'MB0')
    node_id_helper(13, 'CMS', 'DT', 64, 1, 'MB+1')
    node_id_helper(14, 'CMS', 'DT', 64, 2, 'MB+1')
    node_id_helper(15, 'CMS', 'DT', 64, 3, 'MB+1')
    node_id_helper(16, 'CMS', 'DT', 64, 4, 'MB+1')
    node_id_helper(17, 'CMS', 'DT', 65, 1, 'MB+2')
    node_id_helper(18, 'CMS', 'DT', 65, 2, 'MB+2')
    node_id_helper(19, 'CMS', 'DT', 65, 3, 'MB+2')
    node_id_helper(20, 'CMS', 'DT', 65, 4, 'MB+2')

    node_id_helper(1, 'CMS', 'GEM', 61, 1, 'RE-2')
    node_id_helper(2, 'CMS', 'GEM', 62, 1, 'RE-1')
    node_id_helper(3, 'CMS', 'GEM', 63, 1, 'RE0')
    node_id_helper(4, 'CMS', 'GEM', 64, 1, 'RE+1')
    node_id_helper(5, 'CMS', 'GEM', 65, 1, 'RE+2')
    node_id_helper(6, 'CMS', 'GEM', 66, 1, 'RE+3')

    node_id_helper(6, 'CMS', 'RPC', 61, 1, 'RE-1')
    node_id_helper(8, 'CMS', 'RPC', 62, 1, 'RE-2')
    node_id_helper(10, 'CMS', 'RPC', 63, 1, 'RE-3')
    node_id_helper(4, 'CMS', 'RPC', 64, 1, 'RE-4')
    node_id_helper(5, 'CMS', 'RPC', 65, 1, 'RE-1')
    node_id_helper(7, 'CMS', 'RPC', 66, 1, 'RE-2')
    node_id_helper(3, 'CMS', 'RPC', 67, 1, 'RE-3')
    node_id_helper(11, 'CMS', 'RPC', 68, 1, 'RE-4')
    node_id_helper(12, 'CMS', 'RPC', 68, 2, 'RE-4')
    node_id_helper(23, 'CMS', 'RPC', 69, 1, 'RB-2')
    node_id_helper(24, 'CMS', 'RPC', 69, 2, 'RB-2')
    node_id_helper(25, 'CMS', 'RPC', 70, 1, 'RB-1')
    node_id_helper(26, 'CMS', 'RPC', 70, 2, 'RB-1')
    node_id_helper(27, 'CMS', 'RPC', 71, 1, 'RB0')
    node_id_helper(28, 'CMS', 'RPC', 71, 2, 'RB0')
    node_id_helper(29, 'CMS', 'RPC', 72, 1, 'RB+1')
    node_id_helper(30, 'CMS', 'RPC', 72, 2, 'RB+1')
    node_id_helper(31, 'CMS', 'RPC', 73, 1, 'RB+2')
    node_id_helper(32, 'CMS', 'RPC', 73, 2, 'RB+2')
    node_id_helper(13, 'CMS', 'RPC', 74, 1, 'RB-2')
    node_id_helper(14, 'CMS', 'RPC', 74, 2, 'RB-2')
    node_id_helper(15, 'CMS', 'RPC', 75, 1, 'RB-1')
    node_id_helper(16, 'CMS', 'RPC', 75, 2, 'RB-1')
    node_id_helper(17, 'CMS', 'RPC', 76, 1, 'RB0')
    node_id_helper(18, 'CMS', 'RPC', 76, 2, 'RB0')
    node_id_helper(19, 'CMS', 'RPC', 77, 1, 'RB+1')
    node_id_helper(20, 'CMS', 'RPC', 77, 2, 'RB+1')
    node_id_helper(21, 'CMS', 'RPC', 78, 1, 'RB+2')
    node_id_helper(22, 'CMS', 'RPC', 78, 2, 'RB+2')
    node_id_helper(38, 'CMS', 'RPC', 79, 1, 'RE+1')
    node_id_helper(40, 'CMS', 'RPC', 80, 1, 'RE+2')
    node_id_helper(42, 'CMS', 'RPC', 81, 1, 'RE+3')
    node_id_helper(36, 'CMS', 'RPC', 82, 1, 'RE+4')
    node_id_helper(33, 'CMS', 'RPC', 83, 1, 'RE+1')
    node_id_helper(34, 'CMS', 'RPC', 84, 1, 'RE+2')
    node_id_helper(35, 'CMS', 'RPC', 85, 1, 'RE+3')
    node_id_helper(43, 'CMS', 'RPC', 86, 1, 'RE+4')
    node_id_helper(44, 'CMS', 'RPC', 86, 2, 'RE+4')
    node_id_helper(9, 'CMS', 'RPC', 87, 1, 'RE-3')
    node_id_helper(41, 'CMS', 'RPC', 88, 1, 'RE+3')


    node_id_helper(1, 'LHCb', 'GEM', 61, 1, 'side A')
    node_id_helper(2, 'LHCb', 'GEM', 62, 1, 'side C')

    node_id_helper(1, 'LHCb', 'MUON', 61, 1, 'M1 GEM side A')
    node_id_helper(11, 'LHCb', 'MUON', 61, 2, 'M1 GEM side A')
    node_id_helper(2, 'LHCb', 'MUON', 62, 1, 'M1 GEM side C')
    node_id_helper(12, 'LHCb', 'MUON', 62, 2, 'M1 GEM side C')

    node_id_helper(3, 'LHCb', 'MUON', 63, 1, 'M2M3 side A')
    node_id_helper(4, 'LHCb', 'MUON', 63, 2, 'M2M3 side A')
    node_id_helper(5, 'LHCb', 'MUON', 64, 1, 'M2M3 side C')
    node_id_helper(6, 'LHCb', 'MUON', 64, 2, 'M2M3 side C')
    node_id_helper(7, 'LHCb', 'MUON', 65, 1, 'M4M5 side A')
    node_id_helper(8, 'LHCb', 'MUON', 65, 2, 'M4M5 side A')
    node_id_helper(9, 'LHCb', 'MUON', 66, 1, 'M4M5 side C')
    node_id_helper(10, 'LHCb', 'MUON', 66, 2, 'M4M5 side C')

    node_id_helper(1, 'LHCb', 'OT', 61, 1, 'Side A')
    node_id_helper(2, 'LHCb', 'OT', 62, 1, 'Side C')
        
        
def add_flowcells():
    '''Add flowcells from Access database'''
    df = pd.read_excel('code/data_for_database_migration/CalibConstants.xlsx')
    df_extra = pd.read_excel('code/data_for_database_migration/CalibConstants_extra.xlsx')

    ids = list(df['CellID'])
    ids_extra = list(df_extra['CellID'])

    ids_filtered = []
    for id in ids:
        if id in ids_extra:
            r1 = df[df.CellID == id]
            r2 = df_extra[df_extra.CellID == id]
            
            if r1.DateTimeStamp.values[0] == r2.DateTimeStamp.values[0]:
                # print(f'id {id} skipped')
                continue
        ids_filtered.append(id)

    ids = ids_filtered

    for id in list(set(ids + ids_extra)):
        if id != id:
            continue # Skip nan values
        add_flowcell(id)


def p(x, params):
    return np.dot(params, [1, x, x**2, x**3, x**4])

def Dp(x, params):
    return np.dot(params, [0, 1, 2*x, 3*x**2, 4*x**3])

def calculate_slope(flow, params, slope_upper_bound, flowcell_id):
    '''Return the slope for the given flow (using Newton Raphson).
    An upper bound of the slope has to be given.'''
    params[0] = params[0] - flow
    slope = slope_upper_bound
    
    # Treat problematic cases independently
    # Easiest workaround for problematic cases is setting slope value to 0
    if flowcell_id in [203319, 203453, 203454, 103528, 103531, 441017]:
        # Wrong slopes (order wrong)
        return 0
    if flowcell_id in [301319, 202021, 202328, 202305, 103168, 173864, 105228, 105525, 301234, 203297, 203327, 301952, 203582, 103563, 103703, 103762, 113088, 113197, 113227, 203270]:
        # No convergence
        return 0
    if flowcell_id in [103406, 107037, 107038, 203266, 601049, 601235]:
        return 0
    
    counter = 0
    while np.abs(p(slope, params)) > 0.1:
        counter += 1
        if counter > 20:
            logging.error(f'Max iterations reached id={flowcell_id}, residual flow = {np.abs(p(slope, params))}')
            return slope
        slope = slope - p(slope, params)/Dp(slope, params)
    return max(0, np.round(slope))


def add_calibrations_from_df(df, ids):
    for id in ids:
        if id != id:
            continue # Skip nan values
        r = df[df.CellID == id]
        
        calib_gas = r.CalibrationGas.values[0]
        if not type(calib_gas) is str:
            if math.isnan(calib_gas):
                calib_gas = 'NULL'
            else:
                raise Exception(f'Calibration has unidentified calibration gas {calib_gas}, row =\n{r}')
        
        logging.debug(f'Add calibration for flowcell {r.CellID.values[0]}')
        params_low = [r.low_x0.values[0], r.low_x1.values[0], r.low_x2.values[0], r.low_x3.values[0], r.low_x4.values[0]]
        params_high = [r.high_x0.values[0], r.high_x1.values[0], r.high_x2.values[0], r.high_x3.values[0],r.high_x4.values[0]]
        if r.high_ZeroFlowSlope.values[0] > 0:
            slope_low_flow_min = r.low_ZeroFlowSlope.values[0]
            slope_low_flow_max = r.high_ZeroFlowSlope.values[0]
            slope_high_flow_max = calculate_slope(r.high_maxFlow.values[0], params_high, 0.9*r.high_ZeroFlowSlope.values[0], r.CellID.values[0])
            assert(slope_low_flow_min > slope_low_flow_max)
            assert(slope_low_flow_max > slope_high_flow_max)
            add_calibration(
                flowcell_id = r.CellID.values[0],
                timestamp = datetime.strptime(r.DateTimeStamp.values[0], '%d/%m/%Y %H:%M:%S'),
                calibration_gas = calib_gas,
                low_flow_min = r.low_minFlow.values[0],
                low_flow_max = r.low_maxFlow.values[0],
                slope_low_flow_min = slope_low_flow_min,
                slope_low_flow_max = slope_low_flow_max,
                low_x0 = r.low_x0.values[0],
                low_x1 = r.low_x1.values[0],
                low_x2 = r.low_x2.values[0],
                low_x3 = r.low_x3.values[0],
                low_x4 = r.low_x4.values[0],
                high_flow_enabled = True,
                high_flow_max = r.high_maxFlow.values[0],
                slope_high_flow_max = slope_high_flow_max,
                high_x0 = r.high_x0.values[0],
                high_x1 = r.high_x1.values[0],
                high_x2 = r.high_x2.values[0],
                high_x3 = r.high_x3.values[0],
                high_x4 = r.high_x4.values[0]
            )
        else:
            slope_low_flow_min = r.low_ZeroFlowSlope.values[0]
            slope_low_flow_max = calculate_slope(r.low_maxFlow.values[0], params_low, 0.9*r.low_ZeroFlowSlope.values[0], r.CellID.values[0])
            assert(slope_low_flow_min > slope_low_flow_max)
            add_calibration(
                flowcell_id = r.CellID.values[0],
                timestamp = datetime.strptime(r.DateTimeStamp.values[0], '%d/%m/%Y %H:%M:%S'),
                calibration_gas = calib_gas,
                low_flow_min = r.low_minFlow.values[0],
                low_flow_max = r.low_maxFlow.values[0],
                slope_low_flow_min = slope_low_flow_min,
                slope_low_flow_max = slope_low_flow_max,
                low_x0 = r.low_x0.values[0],
                low_x1 = r.low_x1.values[0],
                low_x2 = r.low_x2.values[0],
                low_x3 = r.low_x3.values[0],
                low_x4 = r.low_x4.values[0]
            )

def add_calibrations():
    '''Add calibrations from Access database, skip incomplete calibs from main excel file.'''
    df = pd.read_excel('code/data_for_database_migration/CalibConstants.xlsx')
    df_extra = pd.read_excel('code/data_for_database_migration/CalibConstants_extra.xlsx')

    ids = list(df['CellID'])
    ids_extra = list(df_extra['CellID'])

    ids_filtered = []
    for id in ids:
        if id in ids_extra:
            r1 = df[df.CellID == id]
            r2 = df_extra[df_extra.CellID == id]
            
            if r1.DateTimeStamp.values[0] == r2.DateTimeStamp.values[0]:
                # print(f'id {id} skipped')
                continue
        ids_filtered.append(id)

    add_calibrations_from_df(df, ids_filtered)
    add_calibrations_from_df(df_extra, ids_extra)
    


def set_flowcell_states():
    directory = 'code/data_for_database_migration/flowcell_locations'
    positions_ch = [
        'Pos 1',
        'Pos 2',
        'Pos 3',
        'Pos 4',
        'Pos 5',
        'Pos 6',
        'Pos 7',
        'Pos 8',
        'Pos 9',
        'Pos 10',
        'Pos 11',
        'Pos 12',
        'Pos 13',
        'Pos 14',
        'Pos 15',
        'Pos 16',
        'Pos 17',
        'Pos 18',
    ]
    filenames = sorted([filename for filename in os.listdir(directory)])

    dict_locs_inv = dict() # Map of flowcell_id to list of locations (there are duplicate ids...)

    for i in range(len(filenames)//2):
        f1 = filenames[2*i]
        f2 = filenames[2*i + 1]
        path1 = os.path.join(directory, f1)
        path2 = os.path.join(directory, f2)
        
        fileroot = f1[:-5]
        experiment, system = fileroot.split(' ')[:2]
        # Handle anomalous cases: set experiment and system to correct value
        if system == 'HMPID':
            system = 'HMP'
        if experiment == 'ALICE' and system == 'CPC':
            system = 'MCH'
        if experiment == 'ALICE' and system == 'RPC':
            system = 'MID'
        if experiment == 'ATLAS' and system == 'CSC':
            system = 'MMG'
        if experiment == 'TOTEM':
            experiment = 'CMS'
            if 'GAS' in system:
                system = 'TOTEM_GAS'
            if 'T1' in system:
                system = 'TOTEM_T1'
            if 'T2' in system:
                system = 'TOTEM_T2'
        if experiment == 'LHCb' and fileroot.split(' ')[2] == 'OT':
            system = 'OT'
        
        df_node = pd.read_excel(path1).sort_values('ID').reset_index()
        df_ids = pd.read_excel(path2).sort_values('ID').reset_index()

        for ind in df_node.index:
            
            rack = df_node['Rack'][ind]
            rack_pos = df_node['Position'][ind]
            
            for i in range(18):
                pos = positions_ch[i]
            
                id_in = df_ids[pos][2*ind]
                id_out = df_ids[pos][2*ind + 1]
                if (id_in == id_out):
                    # No more channels for this ELMB
                    break
                
                if id_in == 0:
                    pass
                elif id_in in dict_locs_inv:
                    dict_locs_inv[id_in].append((experiment, system, rack, rack_pos, i+1, True))
                else:
                    dict_locs_inv[id_in] = [(experiment, system, rack, rack_pos, i+1, True)]
                    
                if id_out == 0:
                    pass
                elif id_out in dict_locs_inv:
                    dict_locs_inv[id_out].append((experiment, system, rack, rack_pos, i+1, False))
                else:
                    dict_locs_inv[id_out] = [(experiment, system, rack, rack_pos, i+1, False)]
            
    for flowcell_id in dict_locs_inv:
        calibration_id = get_most_recent_calibration_id(flowcell_id)
        for location in dict_locs_inv[flowcell_id]:
            experiment, system, rack, rack_pos, channel, is_input = location
            add_state_flowcell(
                flowcell_id=flowcell_id,
                timestamp=datetime(2024, 4, 12),
                active_calibration_id=calibration_id,
                location_id=get_location_id(experiment, system, rack, rack_pos),
                channel=channel,
                is_input=is_input
            )



def find_missing_flowcells_set_flowcell_states():
    directory = 'code/data_for_database_migration/flowcell_locations'
    positions_ch = [
        'Pos 1',
        'Pos 2',
        'Pos 3',
        'Pos 4',
        'Pos 5',
        'Pos 6',
        'Pos 7',
        'Pos 8',
        'Pos 9',
        'Pos 10',
        'Pos 11',
        'Pos 12',
        'Pos 13',
        'Pos 14',
        'Pos 15',
        'Pos 16',
        'Pos 17',
        'Pos 18',
    ]
    filenames = sorted([filename for filename in os.listdir(directory)])

    for i in range(len(filenames)//2):
        f1 = filenames[2*i]
        f2 = filenames[2*i + 1]
        path1 = os.path.join(directory, f1)
        path2 = os.path.join(directory, f2)
        
        fileroot = f1[:-5]
        experiment, system = fileroot.split(' ')[:2]
        # Handle anomalous cases: set experiment and system to correct value
        if system == 'HMPID':
            system = 'HMP'
        if experiment == 'ALICE' and system == 'CPC':
            system = 'MCH'
        if experiment == 'ALICE' and system == 'RPC':
            system = 'MID'
        if experiment == 'ATLAS' and system == 'CSC':
            system = 'MMG'
        if experiment == 'TOTEM':
            experiment = 'CMS'
            if 'GAS' in system:
                system = 'TOTEM_GAS'
            if 'T1' in system:
                system = 'TOTEM_T1'
            if 'T2' in system:
                system = 'TOTEM_T2'
        if experiment == 'LHCb' and fileroot.split(' ')[2] == 'OT':
            system = 'OT'
        
        df_node = pd.read_excel(path1).sort_values('ID').reset_index()
        df_ids = pd.read_excel(path2).sort_values('ID').reset_index()

        for ind in df_node.index:
            
            rack = df_node['Rack'][ind]
            rack_pos = df_node['Position'][ind]
            
            for i in range(18):
                pos = positions_ch[i]
            
                id_in = df_ids[pos][2*ind]
                id_out = df_ids[pos][2*ind + 1]
                if (id_in == id_out):
                    # ('No more channels for this ELMB')
                    if id_in not in [0, 2]:
                        print(f'Problem with id {id_in}')
                        print(experiment, system, 'rack', rack, 'position', rack_pos, 'channel', i+1)
                        print()
                        break


def bugfix_set_flowcell_states():
    
    loc_id = get_location_id('ATLAS', 'RPC', 66, 1)
    
    flowcell_id = 703352
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=16,
        is_input=True
    )
    
    flowcell_id = 703353
    calibration_id = get_most_recent_calibration_id(flowcell_id)    
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=16,
        is_input=False
    )
    
    
    loc_id = get_location_id('ATLAS', 'RPC', 67, 2)
    
    flowcell_id = 703388
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=13,
        is_input=True
    )
    
    flowcell_id = 703389
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=13,
        is_input=False
    )
    
    
    
    loc_id = get_location_id('ATLAS', 'RPC', 68, 2)
    
    flowcell_id = 703424
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=13,
        is_input=True
    )
    
    flowcell_id = 703425
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=13,
        is_input=False
    )
    
    
    loc_id = get_location_id('CMS', 'GEM', 64, 1)
    flowcell_id = 109290
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=13,
        is_input=False
    )
    
    
    loc_id = get_location_id('CMS', 'GEM', 61, 1)
    flowcell_id = 109229
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=13,
        is_input=False
    )
    
    loc_id = get_location_id('CMS', 'RPC', 71, 1)
    flowcell_id = 203631
    calibration_id = get_most_recent_calibration_id(flowcell_id)
    add_state_flowcell(
        flowcell_id=flowcell_id,
        timestamp=datetime(2024, 10, 24),
        active_calibration_id=calibration_id,
        location_id=loc_id,
        channel=13,
        is_input=False
    )


if __name__ == '__main__':
    # add_locations()
    # add_node_ids()
    # add_flowcells()
    # add_calibrations()
    # set_flowcell_states()
    # find_missing_flowcells_set_flowcell_states()
    # bugfix_set_flowcell_states()
    pass
    

