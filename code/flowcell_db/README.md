## Overview

This folder contains code related to the flowcell database. Both the old Access database on DFS (G:\Departments\PH\Groups\TA1\Gas_group\Calibration_Station_Flow_cell\Database_backup) and the new Postgres database (dbod-flowcells.cern.ch) are used.

- [data_migration.py](data_migration.py): script to fill the Postgres database. Reads data files in the [data_for_database_migration folder](../data_for_database_migration/) that have been exported from Access.
- [data_export.py](data_export.py): script to generate the old database from the new database in order to use the [Webmas file generator](https://gitlab.cern.ch/fluidic-system/gas/industrial_flow_cells/-/tree/master/WEBMASS/Src/Webmass_file_creator/builds). The script generates files in the folder [database_source_files](./database_source_files/). This folder can be copied to the folder of the old Access database on DFS. The `db1` Access database uses these files as the data source. To run the script, execute the following steps:
    - Create a `.env` file in the folder `code\flowcell_db` and put the database password in it: `PW_FLOWCELLS_DB=<password here>`
    - Run `python -m pip install -r requirements.txt`
    - Run `python code\flowcell_db\data_export.py`

Due to mistakes from the past, there are flowcells in the database with duplicate barcodes (i.e. different flowcells installed in the gas systems have the same barcode). This is problematic as the new database only supports 1 location per barcode. For some gas systems (CMS GEM and CMS RPC in particular), there are duplicate barcodes within the gas system. This interferes with the generation of the database source files for the Webmas PLC.

You can easily check if there are duplicate flowcells within a gas system by inspecting the flowcell positions in the web interface: [https://flowcells-ui.app.cern.ch/](https://flowcells-ui.app.cern.ch/). There are duplicate flowcells if the flowcells in some positions are missing (shown as `<NA>`). There is only a problem when there are `<NA>` values in the position of calibrated flowcells, sometimes they are to be expected when there are inactive channels with dummy flowcells (e.g. ATLAS RPC Rack 67 position 2). In case of missing flowcells, you should manually correct the database source files by putting the correct flowcell barcodes in the txt files.

Below is an example of dummy flowcells (from [https://flowcells-ui.app.cern.ch/](https://flowcells-ui.app.cern.ch/)). Note: the barcode `777` is used in the txt files for dummy flowcells, i.e. physical flowcell positions that are irrelevant because they are unused. (The old Access database requires these dummy flowcells to work correctly.)

<img src="../img/dummy_flowcells.png" alt="dummy_flowcells" width="900"/>


Below is an example of a missing flowcell due to duplicate barcodes:

<img src="../img/missing_flowcell.png" alt="missing_flowcell" width="900"/>

After generating the database source files, the files should be copied to a Windows machine to generate the Webmas `conf_sys` file. The Webmas file generator application can be found [here](https://gitlab.cern.ch/fluidic-system/gas/industrial_flow_cells/-/tree/master/WEBMASS/Src/Webmass_file_creator). You should configure your machine to read the database on this path on DFS: `G:\Departments\PH\Groups\TA1\Gas_group\Calibration_Station_Flow_cell\Database_backup\db1` (a backup of the linked Access database is stored in this repository [here](db1_backup_from_DFS.mdb)). To configure the database, open the Windows program `ODBC Data Sources` and add a user data source:

<img src="../img/ODBC_screenshot.png" alt="ODBC_screenshot" width="900"/>

An entity-relationship diagram of the new Postgres database:

<img src="../img/db_diagram.png" alt="db_diagram" width="1200"/>
