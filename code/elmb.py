import pyads
import time
import logging
from utils import revert_dictionary


class Elmb:
    '''Generic ELMB'''
    
    def __init__(self, plc, id, delay=0.1):
        '''Initialize an ELMB with the given CAN-bus node ID'''
        self.logger = logging.getLogger(f'ELMB with ID={id}')
        self.plc = plc
        self.id = id
        self.delay = delay
        self.set_data_lengths()
        time.sleep(self.delay)
    
    codes_adc_rate = {
        '15.0 Hz':  0b000,
        '30.0 Hz':  0b001,
        '61.6 Hz':  0b010,
        '84.5 Hz':  0b011,
        '101.1 Hz': 0b100,
        '1.88 Hz':  0b101,
        '3.76 Hz':  0b110,
        '7.51 Hz':  0b111
    }
    codes_adc_range = {
        '100 mV': 0b000,
        '55 mV':  0b001,
        '25 mV':  0b010,
        '1 V':    0b011,
        '5 V':    0b100,
        '2.5 V':  0b101,
    }
    ranges_numeric_adc_for_code = {
        0b000: 0.1,
        0b001: 0.055,
        0b010: 0.025,
        0b011: 1.0,
        0b100: 5.0,
        0b101: 2.5,
    }
    
    codes_adc_mode = {
        'Bipolar': 0,
        'Unipolar': 1
    }
    
    rates_adc_for_code = revert_dictionary(codes_adc_rate)
    ranges_adc_for_code = revert_dictionary(codes_adc_range)
    modes_adc_for_code = revert_dictionary(codes_adc_mode)
    
    ## Data types: CANopen & TwinCAT notation:
    # U8  = USINT
    # U16 = UINT
    # U32 = UDINT
    # I32 = DINT
    # VisStr = STRING
    # Bool = BOOL
    # Asterisk for configuration parameters that can be stored in non-volatile EEPROM
    data_types = {
        (0x1009, 0): 'STRING', # Manufacturer hw version
        (0x100A, 0): 'STRING', # Manufacturer software version
        (0x100A, 1): 'STRING', # Minor version number
        (0x100D, 0): 'USINT',  # * Life time factor (s)
        (0x1010, 1): 'STRING', # Save all parameters
        (0x1017, 0): 'UINT',   # * Heartbeat time (s)
        (0x1802, 2): 'USINT',  # Transmission type
        (0x2100, 1): 'USINT',  # * #input channels ADC
        (0x2100, 2): 'USINT',  # * Conversion Word Rate
        (0x2100, 3): 'USINT',  # * Input Voltage Range
        (0x2100, 4): 'USINT',  # * Unipolar/Bipolar Measurement Mode
        (0x3100, 0): 'UDINT',  # ELMB Serial Number
        (0x3200, 2): 'USINT',  # * Enable auto-start 
        (0x3200, 3): 'USINT',  # * Bus-off max retry counter
        (0x6200, 1): 'USINT',  # Write outputs 1-8
        (0x6200, 2): 'USINT',  # Write outputs 9-16
    }
    # Analogue inputs
    for i in range(1, 65):
        data_types[(0x6404, i)] = 'UDINT'

    def set_data_lengths(self):
        data_lengths = dict()
        for can_indices, data_type in self.data_types.items():
            if data_type == 'USINT':
                data_lengths[can_indices] = 1
            elif data_type == 'UINT':
                data_lengths[can_indices] = 2
            elif data_type == 'UDINT':
                data_lengths[can_indices] = 4
            elif data_type == 'DINT':
                data_lengths[can_indices] = 4
            elif data_type == 'STRING':
                data_lengths[can_indices] = 4 # NOTE only strings used are 4 characters long
            elif data_type == 'BOOL':
                data_lengths[can_indices] = 1
            else:
                raise Exception(f'Invalid data_type: {data_type}')
        self.data_lengths = data_lengths
    
    def get_firmware_version(self):
        version_major = self.read_can(0x100A, 0)
        try:
            version_minor = self.read_can(0x100A, 1)
            return f'{version_major}.{version_minor}'
        except:
            return f'{version_major}'
    

    def read_can(self, can_index, can_subindex=0, timeout=5):
        data_type = self.data_types[(can_index, can_subindex)]
        data_length = self.data_lengths[(can_index, can_subindex)]
        self.plc.write_by_name(f'MAIN.elmb{self.id}_can_index', can_index)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_can_subindex', can_subindex)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_data_type', data_type)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_data_length', data_length)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_bRead', True)
        for _ in range(int(timeout/self.delay)):
            bRead = self.plc.read_by_name(f'MAIN.elmb{self.id}_bRead')
            if not bRead:
                bError = self.plc.read_by_name(f'MAIN.elmb{self.id}_bError')
                if bError:
                    nErrID = self.plc.read_by_name(f'MAIN.elmb{self.id}_nErrID')
                    raise Exception(f'read_can(can_index={hex(can_index)}, can_subindex={can_subindex}) failed with ADS error code: {hex(nErrID)}')
                res = self.plc.read_by_name(f'MAIN.elmb{self.id}_data_{data_type}')
                self.logger.debug(f'read_can(can_index={hex(can_index)}, can_subindex={can_subindex}) finished: {res}')
                return res
            time.sleep(self.delay)
        raise Exception('read_can: timeout exceeded')


    def write_can(self, data, can_index, can_subindex=0, timeout=5):
        data_type = self.data_types[(can_index, can_subindex)]
        data_length = self.data_lengths[(can_index, can_subindex)]
        self.plc.write_by_name(f'MAIN.elmb{self.id}_can_index', can_index)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_can_subindex', can_subindex)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_data_type', data_type)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_data_length', data_length)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_data_{data_type}', data)
        self.plc.write_by_name(f'MAIN.elmb{self.id}_bWrite', True)
        for _ in range(int(timeout/self.delay)):
            bWrite = self.plc.read_by_name(f'MAIN.elmb{self.id}_bWrite')
            if not bWrite:
                bError = self.plc.read_by_name(f'MAIN.elmb{self.id}_bError')
                if bError:
                    nErrID = self.plc.read_by_name(f'MAIN.elmb{self.id}_nErrID')
                    raise Exception(f'write_can(data={data}, can_index={hex(can_index)}, can_subindex={can_subindex}) failed with ADS error code: {hex(nErrID)}')
                self.logger.debug(f'write_can(data={data}, can_index={hex(can_index)}, can_subindex={can_subindex}) finished')
                return
            time.sleep(self.delay)
        raise Exception('write_can: timeout exceeded')

    def write_adc_rate(self, data: str):
        '''Wrapper function to write ADC Conversion Word Rate'''
        if data not in self.codes_adc_rate:
            raise Exception(f'Invalid ADC rate: {data}')
        self.write_can(self.codes_adc_rate[data], 0x2100, 2)

    def write_adc_range(self, data: str):
        '''Wrapper function to write ADC Input Voltage Range'''
        if data not in self.codes_adc_range:
            raise Exception(f'Invalid ADC range: {data}')
        self.write_can(self.codes_adc_range[data], 0x2100, 3)

    def write_adc_mode(self, data: str):
        '''Wrapper function to write ADC Measurement Mode'''
        if data not in self.codes_adc_mode:
            raise Exception(f'Invalid ADC mode: {data}')
        self.write_can(self.codes_adc_mode[data], 0x2100, 4)
        
    def read_analogue_input(self, num, adc_range=None):
        '''Read the voltage of the analog input = num'''
        raw_reading = self.read_can(0x6404, can_subindex=num+1)
        res_str =   format(raw_reading, '#034b')[:-8]
        res_flags = format(raw_reading, '#034b')[-8:]
        self.logger.debug(f'Finished reading analogue input {num}: {res_str} with flags {res_flags}\n(note: the flags for a successful reading = 11100000)')
        if not adc_range:
            adc_range = self.read_can(0x2100, 3)
        resolution = self.ranges_numeric_adc_for_code[adc_range]/(2**16-1)
        return int(res_str, base=2)*resolution

class ElmbElmbio(Elmb):
    '''ELMB with ELMBio firmware'''
    
    def __init__(self, plc, id, delay=0.1):
        super().__init__(plc, id, delay)
        
        ## Extra objects for ELMBio firmware
        # Asterisk for configuration parameters that can be stored in non-volatile EEPROM
        data_types_extra = {
            (0x1802, 5):   'UINT',  # Event timer [1 s]
            (0x6423, 0):   'BOOL',  # Global Analog Input Interrupt Enable
            (0x2140, 0):   'BOOL',  # Enable Analogue Input Interrupt Upper/Lower Limit Mode
            (0x6424, 255): 'DINT',  # All Inputs (Analogue Input Interrupt Upper Limit)
            (0x6424, 1): 'DINT',    # Input 1
            (0x6425, 255): 'DINT',  # All Inputs (Analogue Input Interrupt Lower Limit)
            (0x6425, 1): 'DINT',    # Input 1
            (0x2150, 0):   'USINT', # Upper/Lower Limit and Delta Exceed Counter
            (0x2300, 0):   'BOOL',  # Digital Output Init High
        }
        self.data_types.update(data_types_extra)
        self.set_data_lengths()
    
    # # TODO for type pressure sensor
    # - life time factor
    # - heartbeat
    # - Number of channels = #+2
    # - rest of ADC params: keep defaults (rate, range, mode)
    # - 1802sub2,5 set to special values from poster
    
    # - 3200sub2 set to 1 (has to be done with CAN interface)
    
    # # TODO for type analysis valve
    # Same fields as type pressure sensor, with #channels=35
    # In addition, settings from poster:
    #     6423, 2140 etc. ==> add to data_types_extra, check with firmware docs what to fill for 1.5V and 0V (in uV!)



class ElmbFlowscan(Elmb):
    '''ELMB with Flowscan firmware'''
    
    def __init__(self, plc, id, delay=0.1):
        super().__init__(plc, id, delay)
        
        ## Extra objects for Flowscan firmware
        # Asterisk for configuration parameters that can be stored in non-volatile EEPROM
        data_types_extra = {
            (0x1803, 2): 'USINT',  # * PDO transmission type (1=SYNC enabled, 255=SYNC disabled)
            (0x1803, 5): 'UINT',   # * Event timer (s)
            (0x2100, 19):'USINT',  # * SPI SCLK signal high period (optocoupler delay), 10<=value<=255
            (0x4000, 2): 'USINT',  # * #samples current & temperature (N)
            (0x4000, 3): 'USINT',  # * #samples slope (M)
            (0x4000, 4): 'USINT',  # * Delta-T (K)
            (0x4000, 5): 'USINT',  # * Delta-T Time-out (s)
            (0x4000, 6): 'USINT',  # * Settling time (in units of 10 ms)
            (0x4000, 10):'USINT',  # * Added delay (in units of 100 μs)
        }
        self.data_types.update(data_types_extra)
        self.set_data_lengths()
    
    def read_channel(self, ch_number_out, timeout=60):
        '''Read the given channel (0-15) with a timeout in seconds.'''
        if ch_number_out < 0 or ch_number_out > 15:
            raise Exception('Invalid argument ch_number')
        
        ch_elmb = self.plc.read_by_name(f'MAIN.elmb{self.id}_ch_number_out')
        if ch_elmb == ch_number_out:
            self.logger.debug(f'Adding bit flag to ch_number_out {ch_number_out}.')
            ch_number_out += 0b1000_0000
        
        self.plc.write_by_name(f'MAIN.elmb{self.id}_ch_number_out', ch_number_out)
        for count in range(timeout):
            ch_numbers = [self.plc.read_by_name(f'MAIN.elmb{self.id}_ch_number_in_tx{tx_num}') for tx_num in ['1', '2', '3', '4', '6']]
            if all(ch_num == ch_number_out for ch_num in ch_numbers):
                res = dict()
                for value_name in [
                        'T_amb1', 'T_amb2',
                        'I_low_in', 'I_high_in', 'I_low_out', 'I_high_out',
                        't_wait1', 't_wait2', 't_sample1', 't_sample2',
                        'slope1', 'slope2'
                    ]:
                    res[value_name] = self.plc.read_by_name(f'MAIN.elmb{self.id}_{value_name}')
                res['time_measured'] = count
                self.logger.debug(f'read_channel of channel {ch_number_out % 0b1000_0000}: finished in {count} seconds.')
                return res
            time.sleep(1)
        raise Exception('read_channel: timeout exceeded')


    def set_adc_mode(self, current, resistance=100):
        '''Switch on/off the high or low current and select the resistance.
        - current = high/low/off
        - resistance = 0-15 for channels or 100 for 100 Ohm resistor'''
        byte_A = 0
        byte_C = 0
        if resistance == 100:
            byte_C = 0b000_001_00
        elif 0 <= resistance <= 5:
            byte_C = (resistance+2)*2**2
        elif 6 <= resistance <= 12:
            byte_C = (resistance-5)*2**5
        elif 13 <= resistance <= 15:
            byte_A = resistance-12
        else:
            raise Exception(f'Invalid resistance: {resistance}')
        if current == 'high':
            byte_C += 1
        elif current == 'low':
            byte_C += 2
        elif current == 'off':
            byte_C = 0
            byte_A = 0
        else:
            raise Exception(f'Invalid current: {current}')
        self.write_can(byte_C, 0x6200, 1)
        self.write_can(byte_A, 0x6200, 2)
    
    def read_flowchannel_voltages(self, ch_num, adc_range=None):
        '''Return the inflow and outflow voltages for the given channel number (0-15).'''
        volt1 = self.read_analogue_input(2*ch_num+2, adc_range)
        volt2 = self.read_analogue_input(2*ch_num+3, adc_range)
        self.logger.debug(f'Finished reading flowchannel #{ch_num} voltages: ({volt1}, {volt2})')
        return (volt1, volt2)



def try_until_success(callable):
    try:
        callable()
    except Exception as e:
        print(e)
        time.sleep(.2)
        try_until_success(callable)


if __name__ == '__main__':

    LOGGING_LEVEL = 'DEBUG'

    try:
        logging.basicConfig(level=LOGGING_LEVEL, format='%(levelname)s:%(name)s:line=%(lineno)d:%(asctime)s:%(message)s', datefmt='%Y-%m-%d %H:%M:%S')

        plc = pyads.Connection('127.0.0.1.1.1', 851)
        plc.open()

    
        elmb = ElmbFlowscan(plc, 3)
        print('Initialized ELMB')
        
        elmb.write_can(11, 0x1017)
        # res = elmb.read_can(0x1017)
        # print(res)
        elmb.write_can('save', 0x1010, 1)
        
        # elmb.write_can(True, 0x2140)
        # res = elmb.read_can(0x2140)
        # print(res)
        # elmb.write_can(False, 0x2140)
        # res = elmb.read_can(0x2140)
        # print(res)


        ## Configuration
        
        # ## Generic parameters
        # try_until_success(lambda: elmb.write_can(0, 0x1017))
        # try_until_success(lambda: elmb.write_can(0,   0x100D))
        # try_until_success(lambda: elmb.write_can(255, 0x1803, 2))
        # try_until_success(lambda: elmb.write_can(255, 0x3200, 3))
        
        # ## Flowscan parameters
        # try_until_success(lambda: elmb.write_can(50,  0x4000, 2))
        # try_until_success(lambda: elmb.write_can(50,  0x4000, 3))
        # try_until_success(lambda: elmb.write_can(20,  0x4000, 4))
        # try_until_success(lambda: elmb.write_can(50,  0x4000, 5))
        # try_until_success(lambda: elmb.write_can(200, 0x4000, 6))
        # try_until_success(lambda: elmb.write_can(0,   0x4000, 10))
        
        # ## ADC parameters
        # try_until_success(lambda: elmb.write_can(0, 0x2100, 1))
        # try_until_success(lambda: elmb.write_adc_rate('30.0 Hz'))
        # try_until_success(lambda: elmb.write_adc_range('5 V'))
        # try_until_success(lambda: elmb.write_adc_mode('Unipolar'))

        # try_until_success(lambda: elmb.write_can('save', 0x1010, 1))
        
        
        
        # ## Read ADC inputs
        # elmb.set_adc_mode('low')
        # time.sleep(1)
        # print(elmb.read_analogue_input(0))
        # print(elmb.read_analogue_input(1))
        # elmb.set_adc_mode('off')
        
        
        # ## Read T_amb voltages
        # elmb.set_adc_mode(current='low', resistance=7)
        # for i in range(10):
        #     volt1, volt2 = elmb.read_flowchannel_voltages(7, elmb.read_can(0x2100, 3))
        #     print(f'{volt1}  {volt2}')
        
        # ## Read channel slopes
        # res = elmb.read_channel(3)
        # res = elmb.read_channel(5)

            

            
        
    finally:
        try:
            elmb.set_adc_mode('off')
        except:
            pass
        plc.close()
