
class pressure_transmitter:
    
    def __init__(self, plc, analog_channel, resistance = 500):
        '''Initialize VEGABAR 29 pressure transmitter connected to the given analog input channel of the EL3064 module.
        The 4-20 mA output is converted into the pressure reading using the given resistance.'''
        self.plc = plc
        self.analog_channel = analog_channel
        self.resistance = resistance
    
    def read(self):
        '''Return the pressure in barg.'''
        voltage_raw_int = self.plc.read_by_name(f'MAIN.analog_input_channel_{self.analog_channel}')
        voltage = 10.0 * voltage_raw_int / int('111111111111111', 2)
        current = voltage / self.resistance
        pressure = (current - 0.004)*5/0.016 # Map [0.004, 0.020] Ampere to [0, 5] barg
        return pressure
