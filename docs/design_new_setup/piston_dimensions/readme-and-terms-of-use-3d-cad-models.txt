
EN   Your CAD data on 08.04.2024 from Festo:

    Dear customer,
    
    attached please find the following file of our 2D/3D-CAD download portal powered by CADENAS:

	identification number: 2123072 DSBC-32-70-PPVA-N3 
    
    JPG2D, 2123072 DSBC-32-70-PPVA-N3---(0), 2123072_DSBC-32-70-PPVA-N3_front.jpg
    JPG2D, 2123072 DSBC-32-70-PPVA-N3---(0), 2123072_DSBC-32-70-PPVA-N3_back.jpg
    JPG2D, 2123072 DSBC-32-70-PPVA-N3---(0), 2123072_DSBC-32-70-PPVA-N3_right.jpg
    JPG2D, 2123072 DSBC-32-70-PPVA-N3---(0), 2123072_DSBC-32-70-PPVA-N3_left.jpg
    JPG2D, 2123072 DSBC-32-70-PPVA-N3---(0), 2123072_DSBC-32-70-PPVA-N3_top.jpg
    JPG2D, 2123072 DSBC-32-70-PPVA-N3---(0), 2123072_DSBC-32-70-PPVA-N3_bottom.jpg
    
    Please also check terms of use at:
    https://www.cadenas.de/terms-of-use-3d-cad-models
    
    Best regards

    Festo SE & Co. KG
    CAD Service
    design_tool@festo.com
    
