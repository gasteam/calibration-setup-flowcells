# Calibration setup flowcells

Repository for the new setup to calibrate Pt100 flowcells.

The code of the application and documentation can be found in the [code/](./code/) folder.

There's a Postgres database that stores the present and past locations of the flowcells and ELMBs, the calibration parameters etc. For everything related to the database, see the [code/flowcell_db/](code/flowcell_db/) folder.

On [this webpage](https://flowcells-ui.app.cern.ch/), the location and active calibrations of the flowcells can be consulted. This read-only webpage is hosted on [OpenShift](https://paas.cern.ch/topology/ns/flowcells-ui), the source code is [here](https://gitlab.cern.ch/gasteam/flowcells-ui).

Information on how to print flowcell barcodes can be found in the [flowcell_barcodes/](flowcell_barcodes) folder.

For documentation about the ELMB, see [this repository](https://gitlab.cern.ch/gasteam/elmb-documentation).


# ELMB firmware

The different firmwares and the firmware loader application are in the [ELMB_firmware/](./ELMB_firmware/) folder.

There are 2 types of firmware:
- Flowscan firmware for flowcell readout
- ELMBio firmware for readout of pressure sensors (ATLAS MDT) or control of rotary multiway analysis valves (CMS DT)

To load the compiled firmware (in .hex file format), you should use the [NI USB-8472](https://www.ni.com/fr-ch/support/model.usb-8472.html?srsltid=AfmBOop7NAmrQe4cZHDwXMj-S9ylI-46J7YkZ3fxs7Qf1OX0PpZ_4msz) CAN interface with the [ELMBloaderNI.exe](./ELMB_firmware/ELMBloaderNI/ELMBloaderNI.exe) application. This loader application should be run on Windows. To use the application, first install National Instruments' NI-CAN library.

For reference: [here](https://gitlab.cern.ch/atlas-dcs-common-software/ELMBfirmware/) is the GitLab repository with the source code for the ELMB firmware. [Here](https://www.nikhef.nl/pub/departments/ct/po/html/ELMB/ELMBresources.html) is a website from NIKHEF with various resources for the ELMB.


# Important note for ELMBio firmware

By default, an ELMB with the ELMBio firware does not automatically enter operational mode after power-up. 
To enable auto-start for an ELMB with ELMBio firmware v4.4.3+, execute the steps below.
A CAN interface is needed to communicate with the ELMB. Here, the inno-maker USB2CAN is used. The GitHub repository with documentation is [here](https://github.com/INNO-MAKER/usb2can). The following commands are for use with a Linux machine.

1. Load the ELMB with ELMBio firmware (v4.4.3 or v4.4.5) and set the node ID to 3 (the following commands are for node ID = 3)
2. Send the following commands with the CAN interface. These commands are based on the ELMBio firmware documention. Note: the first data byte has to be set to 2F, 2B and 23 for writing 1 byte, 2 bytes and 4 bytes respectively (e.g. steps 6, 7 and 8).
    1. `sudo ip link set can0 down`
    2. Set bitrate to 50 kbit/s:  
    `sudo ip link set can0 type can bitrate 50000`
    3. `sudo ip link set can0 up`
    4. Open a new terminal and run `candump can0`. This will allow you to monitor all of the messages on the bus, including the responses of the ELMB.
    5. Put ELMB in operational state = soft reset:  
    `cansend can0 000#0103`
    6. Write 1 to object with index 3200 subindex 2:  
    `cansend can0 603#2F00320201000000`
    7. Write 5 seconds to object with index 1017 subindex 0:  
    `cansend can0 603#2B17100005000000`
    8. Write "save" to object with index 1010:  
    `cansend can0 603#2310100173617665`
    9. Soft reset:  
    `cansend can0 000#0103`
3. Power cycle the ELMB and run the command "candump can0". You should now see a periodic heartbeat message every 5 seconds: `can0  703   [1]  05`. If so, the procedure was successful.
4. Send the following commands with the CAN interface.
    1. Set the life time factor to 0:  
    `cansend can0 603#2F0D100000000000`
    2. Set the bus-off max retry counter to 255:  
    `cansend can0 603#2F003203FF000000`
    3. Set the conversion word rate to 30 Hz:  
    `cansend can0 603#2F00210201000000`
    4. Set the input voltage range to 5 V:  
    `cansend can0 603#2F00210304000000`
    5. Set the measurement mode to unipolar:  
    `cansend can0 603#2F00210401000000`
    6. Save settings to non-volatile EEPROM:  
    `cansend can0 603#2310100173617665`


## Option 1: ELMBio for pressure sensors

At the moment, the GUI does not work well for ELMB configuration with ELMBio firmware. Instead of the GUI, use the commands below to configure the ELMB to read pressure sensors. If the ELMB is going to be used with the legacy Webmas PLC, use the first list of commands. Alternatively, use the second list of commands for use with the new Schneider M580 PLC. In any case, make sure to open a second terminal where you run `candump can0` to check the reply messages from the ELMB: the first byte of the reply should be `0x60`. When the first byte is `0x80`, it means the operation failed, in which case you can retry with a different ELMB.

### For use with Webmas PLC
1. `cansend can0 603#2F00210108000000` (Note: this is an example for 6 channels, for n channels, n+2 has to be written to object 2100sub1)
2. `cansend can0 603#2F021802FF000000`
3. `cansend can0 603#2B0218051E000000`
4. `cansend can0 603#2310100173617665`

These commands do the following:
1. Set the number of channels n to n+2 (the first two channels of the ELMB should be ignored, the voltages are contained in the third, fourth etc. channel)
2. Set the TxPDO3 transmission type to 255
3. Enable automatic measurements according to an event timer and set the time interval to 30 seconds
4. Save settings to non-volatile EEPROM


### For use with Schneider M580 PLC

The Schneider M580 PLC requires a different configuration:
- The number of channels should be set to zero (in contrast to the Webmas PLC, the SDO mechanism of the ELMBio firmware is used to read the analog inputs).
- The event timer is set to 0. This disables automatic measurements according to a time interval.

This is the full list of commands:
1. `cansend can0 603#2F00210100000000`
2. `cansend can0 603#2F021802FF000000`
3. `cansend can0 603#2B02180500000000`
4. `cansend can0 603#2310100173617665`


## Option 2: ELMBio for analysis valves

At the moment, the GUI does not work well for ELMB configuration with ELMBio firmware. Instead of the GUI, use the commands below to configure the ELMB to control analysis multi-way valves.

### For use with Webmas PLC

1. Set the number of channels to 35 (= 0x23):  
`cansend can0 603#2F00210123000000`
2. Set window mode:  
`cansend can0 603#2F40210001000000`
3. Set counter:  
`cansend can0 603#2F50210001000000`
3. Set all digital outputs to high after hard reset:  
`cansend can0 603#2F00230001000000`
4. Enable readout-on-change TPDO3 transmissions:  
`cansend can0 603#2F23640001000000`
5. Set Analogue Input Interrupt Upper Limit to 1.5 V:  
`cansend can0 603#232464FF60E31600`
6. Set Analogue Input Interrupt Lower Limit to 0 V:  
`cansend can0 603#232564FF00000000`
7. Save settings to non-volatile EEPROM:  
`cansend can0 603#2310100173617665`



# Configuration of Delta T for Flowscan firmware

Below are commands to read and write the Delta T configuration on the ELMB Flowscan firmware (object 0x4000 subindex 4 from the Flowscan object dictionary).

ATTENTION! the messages below are in hexadecimal number format. E.g. `NodeID=30` in decimal becomes `NodeID=1E` in hexadecimal.

Command to read Delta T:  
`cansend can0 600+NodeID#4000400400000000`

Reply:
`can0  580+NodeID   [8]  4F 00 40 04 <temperature-byte> 00 00 00`


Write 20°C:  
`cansend can0 600+NodeID#2F00400414000000`

Write 17°C:  
`cansend can0 600+NodeID#2F00400411000000`

**Save settings to non-volatile EEPROM:**  
`cansend can0 600+NodeID#2310100173617665`

The response for a successful write command starts with `0x60`, e.g. `can0  580+NodeID   [8]  60 00 40 04 00 00 00 00`  
The response for failure of a write command starts with `0x80`, e.g. `can0  580+NodeID   [8]  80 00 40 04 00 00 09 06`
